# EPITA Infrastructure

This project is the main repository for EPITA's teaching IT infrastructure. We
try to follow the Infrastructure as Code (IaC) methodology as much as possible
for all of our deployments.

EPITA's teaching IT infrastructure is made of a OpenStack private cloud cluster
made available to some entities of the school (majors or laboratories). We also
use it ourselves to deploy common services for the whole school. Most of our
services are deployed in Kubernetes clusters on top of our OpenStack private
cloud.

## Repository structure

### Undercloud

The `undercloud` folder is the Ansible deployment folder for our Undercloud. The
Undercloud is the underlying infrastructure behind our OpenStack private cloud
deployment.

The Undercloud is in charge of :
- backbone networking (routing, firewalling)
- DNS secondary of some zones, primary is OpenStack Designate
- DNS primary of some zones
- Vault
- Authentication inside the Undercloud
- E-mail relaying
- Backups
- Part of the monitoring
- Storage (Ceph)
- OpenStack infrastructure (control plane and computes)
- ArgoCD

We have a small k3s cluster for some of the mentioned above services inside the
Undercloud. The cluster runs in libvirt virtual machines on some physical
servers of the Undercloud.

Ceph is deployed using `cephadm` which is not a IaC tool, so our Ceph deployment
cannot be found in this repository.

### Kolla-Ansible

The `kolla` folder contains the Kolla-Ansible deployment files used to deploy
our OpenStack private cloud infrastructure. It is not part of the `undercloud`
folder as it is far more complicated than the other Ansible roles we use for the
rest of the Undercloud.

### Overcloud

The `overcloud` folder contains all of our Ansible and Terraform files to deploy
our services inside our OpenStack private cloud. Most of user facing services
are part of the Overcloud, and the vast majority is deployed in Kubernetes
clusters in the Overcloud.

Currently deployed services of the Overcloud are :
- Moulinette as a Service (MaaS) workers
- GitLab CI runners
- RT ticket system
- OpenAFS cluster for students file storage in computer rooms
- Moodle
- `ops` RKE1 cluster (mostly for monitoring and logging operations)
- `prod-1` RKE1 cluster for most of the user facing services

### Kubernetes

The `k8s` folder contains the Kubernetes manifests for all of our Kubernetes
clusters. Everything is automatically synced to our clusters using ArgoCD.

## Getting started

Before using Ansible in undercloud or overcloud and each time the
`requirements.yml` file changes, run the following from the root of the
repository:

```
ansible-galaxy install -r requirements.yml --force
```

You also need to login to Vault before running Ansible.

First, load the config.sh script to populate the VAULT_ADDR environment
variable.

```
source ./config.sh
```

Login to the Vault, replace MY_USERNAME with your login.
```
vault login -method=ldap username=MY_USERNAME
```

Load the config.sh script again to populate some useful secrets, especially if
working in the undercloud.

```
source ./config.sh
```
