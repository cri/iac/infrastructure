{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      #url = "github:nix-community/poetry2nix";
      url = "github:g00pix/poetry2nix/add-cryptography-43-hash";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgsKolla.url =
        "github:NixOS/nixpkgs/c082856b850ec60cda9f0a0db2bc7bd8900d708c";
    poetry2nixKolla = {
      url = "github:nix-community/poetry2nix/0302506b1598a2706dd47ed110e153e69b43e9ad";
      inputs.nixpkgs.follows = "nixpkgsKolla";
    };
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, nixpkgsKolla, poetry2nix, poetry2nixKolla, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsKollaFor = lib.genAttrs defaultSystems (system: import nixpkgsKolla {
        inherit system;
        overlays = [ poetry2nixKolla.overlays.default ];
      });
      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [ poetry2nix.overlays.default ];
      });
    in
    (eachDefaultSystem (system:
      let
        pkgsKolla = nixpkgsKollaFor.${system};
        pkgs = nixpkgsFor.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = (with pkgs; [
            poetry
            (pkgs.poetry2nix.mkPoetryEnv {
              projectDir = self;

              overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
                  os-client-config = super.os-client-config.overridePythonAttrs (old: {
                      buildInputs = old.buildInputs or [ ] ++ [ super.setuptools ];
                  });
              });
            })
        ]) ++ (with pkgs; [
            git
            go
            jsonnet
            jsonnet-bundler
            openssl
            pre-commit
            shellcheck
            opentofu
            vault
            velero
            yq
            packer
        ]);

          shellHook = ''
            source ./config.sh
          '';
        };

        devShells.kolla = pkgs.mkShell {
          buildInputs = (with pkgsKolla; [
            poetry
            (pkgsKolla.poetry2nix.mkPoetryEnv {
              projectDir = ./kolla;

              overrides = pkgsKolla.poetry2nix.overrides.withDefaults (self: super: {
                kolla-ansible = super.kolla-ansible.overridePythonAttrs (old: {
                  patches = [
                    (writeText "python-path.patch" ''
                      diff --git a/tools/kolla-ansible b/tools/kolla-ansible
                      index 2f80ff67a..631b25afe 100755
                      --- a/tools/kolla-ansible
                      +++ b/tools/kolla-ansible
                      @@ -3,7 +3,7 @@
                       # This script can be used to interact with kolla via ansible.

                       # do not use _PYTHON_BIN directly, use $(get_python_bin) instead
                      -_PYTHON_BIN=""
                      +_PYTHON_BIN="$(which python)"

                       function get_python_bin {
                           if [ -n "$_PYTHON_BIN" ]; then
                    '')
                    (writeText "basedir-path.patch" ''
                      diff --git a/tools/kolla-ansible b/tools/kolla-ansible
                      index 2f80ff67a..6d741a914 100755
                      --- a/tools/kolla-ansible
                      +++ b/tools/kolla-ansible
                      @@ -266,8 +266,7 @@ ARGS=$(getopt -o "''${SHORT_OPTS}" -l "''${LONG_OPTS}" --name "$0" -- "$@") || { usa

                       eval set -- "$ARGS"

                      -find_base_dir
                      -
                      +BASEDIR="@outDir@/share/kolla-ansible"
                       INVENTORY="''${BASEDIR}/ansible/inventory/all-in-one"
                       PLAYBOOK="''${BASEDIR}/ansible/site.yml"
                       VERBOSITY=
                    '')
                  ];

                  postInstall = ''
                    substituteInPlace $out/bin/kolla-ansible \
                      --subst-var-by outDir $out
                  '';

                  # Required because we use the git source of the package
                  # This is usually deduced from the git tag but Nix removes
                  # the .git folder when cloning so we have to specify it here.
                  PBR_VERSION = old.version;
                });
              });
            })
        ]) ++ (with pkgs; [
            git
            openssl
            vault
        ]);

          shellHook = ''
            source ./config.sh
          '';
        };
      }
    ));
}
