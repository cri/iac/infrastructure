resource "openstack_identity_project_v3" "CRI_Undercloud_domain" {
  name      = "CRI_Undercloud"
  is_domain = true
}

resource "openstack_identity_project_v3" "CRI_Undercloud" {
  name      = "CRI_Undercloud"
  domain_id = openstack_identity_project_v3.CRI_Undercloud_domain.id
}

data "openstack_identity_group_v3" "CRI_Undercloud_cri-roots" {
  name      = "cri-roots"
  domain_id = openstack_identity_project_v3.CRI_Undercloud_domain.id
}

data "openstack_identity_group_v3" "CRI_Undercloud_cri-service-accounts-openstack" {
  name      = "cri-service-accounts-openstack"
  domain_id = openstack_identity_project_v3.CRI_Undercloud_domain.id
}


resource "openstack_identity_role_assignment_v3" "CRI_Undercloud_domain-undercloud-cri-roots" {
  domain_id = openstack_identity_project_v3.CRI_Undercloud_domain.id
  group_id  = data.openstack_identity_group_v3.CRI_Undercloud_cri-roots.id
  for_each = toset([
    data.openstack_identity_role_v3.admin.id,
  ])
  role_id = each.value
}

resource "openstack_identity_role_assignment_v3" "CRI_Undercloud-undercloud-cri-roots" {
  project_id = openstack_identity_project_v3.CRI_Undercloud.id
  group_id   = data.openstack_identity_group_v3.CRI_Undercloud_cri-roots.id
  for_each = toset([
    data.openstack_identity_role_v3.admin.id,
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
  ])
  role_id = each.value
}

resource "openstack_identity_role_assignment_v3" "CRI_Undercloud-undercloud-cri-service-accounts-openstack" {
  project_id = openstack_identity_project_v3.CRI_Undercloud.id
  group_id   = data.openstack_identity_group_v3.CRI_Undercloud_cri-service-accounts-openstack.id
  for_each = toset([
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
  ])
  role_id = each.value
}

resource "openstack_identity_role_assignment_v3" "CRI_undercloud-cri-roots" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_Undercloud_cri-roots.id
  for_each = toset([
    data.openstack_identity_role_v3.admin.id,
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
  ])
  role_id = each.value
}

resource "openstack_identity_role_assignment_v3" "CRI_undercloud-cri-service-accounts-openstack" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_Undercloud_cri-service-accounts-openstack.id
  for_each = toset([
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
  ])
  role_id = each.value
}
