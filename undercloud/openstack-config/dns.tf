resource "openstack_dns_zone_v2" "openstack_epita_fr" {
  name        = "openstack.epita.fr."
  email       = "hostmaster@cri.epita.fr"
  description = <<EOT
    Default zone for newly created instances and network devices.
  EOT
  type        = "PRIMARY"
}
