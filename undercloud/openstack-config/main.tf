terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }

  backend "s3" {
    # this uses the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for
    # authentication
    bucket = "cri-terraform.s3.cri.epita.fr"
    key    = "prod/admin.tfstate"
    region = "default"
    endpoints = {
      s3 = "https://s3.cri.epita.fr"
    }
    force_path_style            = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_s3_checksum            = true
  }
}

provider "openstack" {
  # this uses the clouds.yaml file and the OS_PASSWORD env variable
  cloud = "openstack"
}
