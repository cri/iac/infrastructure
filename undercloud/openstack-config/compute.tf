resource "openstack_compute_aggregate_v2" "fast-io" {
  name = "fast-io"
  metadata = {
    ssd = "true"
  }

  hosts = [for i in concat([12, 13], range(20, 26)) : "compute-${i}.bm.cri.epita.fr"]
}

resource "openstack_compute_aggregate_v2" "x86-64-v2" {
  name = "x86-64-v2"
  metadata = {
    max-cpu-level = "2"
  }

  hosts = [for i in range(13, 16) : "compute-${i}.bm.cri.epita.fr"]
}

resource "openstack_compute_aggregate_v2" "x86-64-v3" {
  name = "x86-64-v3"
  metadata = {
    max-cpu-level = "3"
  }

  hosts = [for i in concat(range(4, 13), range(18, 26)) : "compute-${i}.bm.cri.epita.fr"]
}

# TODO: remove me when all computes kernels are updated
# these are the computes that have a recent enough kernel version that doesn't
# have the nested kvm cpu lock issue that I encountered.
# This is necessary for maas workers when running NET1 jobs...
resource "openstack_compute_aggregate_v2" "nested-works" {
  name = "nested-works"
  metadata = {
    nested-works = "true"
  }

  hosts = [for i in concat([5, 12, 13], range(20, 26)) : "compute-${i}.bm.cri.epita.fr"]
}
