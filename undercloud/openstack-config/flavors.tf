resource "openstack_compute_flavor_v2" "m1-tiny" {
  name      = "m1.tiny"
  ram       = 1024
  vcpus     = 1
  disk      = 10
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 96000000
    "quota:disk_read_iops_sec"   = 150
    "quota:disk_write_bytes_sec" = 96000000
    "quota:disk_write_iops_sec"  = 150
  }
}

resource "openstack_compute_flavor_v2" "m1-small" {
  name      = "m1.small"
  ram       = 2048
  vcpus     = 1
  disk      = 20
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 128000000
    "quota:disk_read_iops_sec"   = 200
    "quota:disk_write_bytes_sec" = 128000000
    "quota:disk_write_iops_sec"  = 200
  }
}
resource "openstack_compute_flavor_v2" "m2-small" {
  name      = "m2.small"
  ram       = 2048
  vcpus     = 2
  disk      = 20
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 128000000
    "quota:disk_read_iops_sec"   = 200
    "quota:disk_write_bytes_sec" = 128000000
    "quota:disk_write_iops_sec"  = 200
  }
}

resource "openstack_compute_flavor_v2" "m1-medium" {
  name      = "m1.medium"
  ram       = 4096
  vcpus     = 2
  disk      = 40
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 128000000
    "quota:disk_read_iops_sec"   = 200
    "quota:disk_write_bytes_sec" = 128000000
    "quota:disk_write_iops_sec"  = 200
  }
}
resource "openstack_compute_flavor_v2" "m2-medium" {
  name      = "m2.medium"
  ram       = 4096
  vcpus     = 4
  disk      = 40
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 128000000
    "quota:disk_read_iops_sec"   = 200
    "quota:disk_write_bytes_sec" = 128000000
    "quota:disk_write_iops_sec"  = 200
  }
}
resource "openstack_compute_flavor_v2" "m2-medium-ssd" {
  name      = "m2.medium.ssd"
  ram       = 4096
  vcpus     = 4
  disk      = 40
  is_public = true

  extra_specs = {
    "aggregate_instance_extra_specs:ssd" = "true"
  }
}

resource "openstack_compute_flavor_v2" "m1-large" {
  name      = "m1.large"
  ram       = 8192
  vcpus     = 4
  disk      = 80
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 256000000
    "quota:disk_read_iops_sec"   = 1000
    "quota:disk_write_bytes_sec" = 256000000
    "quota:disk_write_iops_sec"  = 1000
  }
}
resource "openstack_compute_flavor_v2" "m2-large" {
  name      = "m2.large"
  ram       = 8192
  vcpus     = 8
  disk      = 80
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 256000000
    "quota:disk_read_iops_sec"   = 1000
    "quota:disk_write_bytes_sec" = 256000000
    "quota:disk_write_iops_sec"  = 1000
  }
}
resource "openstack_compute_flavor_v2" "m2-large-ssd" {
  name      = "m2.large.ssd"
  ram       = 8192
  vcpus     = 8
  disk      = 80
  is_public = true

  extra_specs = {
    "aggregate_instance_extra_specs:ssd" = "true"
  }
}

resource "openstack_compute_flavor_v2" "m1-xlarge" {
  name      = "m1.xlarge"
  ram       = 16384
  vcpus     = 8
  disk      = 100
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 512000000
    "quota:disk_read_iops_sec"   = 2000
    "quota:disk_write_bytes_sec" = 512000000
    "quota:disk_write_iops_sec"  = 2000
  }
}
resource "openstack_compute_flavor_v2" "m1-xlarge2" {
  name      = "m1.xlarge2"
  ram       = 16384
  vcpus     = 4
  disk      = 100
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 512000000
    "quota:disk_read_iops_sec"   = 2000
    "quota:disk_write_bytes_sec" = 512000000
    "quota:disk_write_iops_sec"  = 2000
  }
}
resource "openstack_compute_flavor_v2" "m2-xlarge" {
  name      = "m2.xlarge"
  ram       = 16384
  vcpus     = 16
  disk      = 100
  is_public = true

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 512000000
    "quota:disk_read_iops_sec"   = 2000
    "quota:disk_write_bytes_sec" = 512000000
    "quota:disk_write_iops_sec"  = 2000
  }
}

// This is deprecated and must be removed once no more VMs use it
resource "openstack_compute_flavor_v2" "m1-2xlarge" {
  name      = "m1.2xlarge"
  ram       = 32768
  vcpus     = 16
  disk      = 200
  is_public = true
}

resource "openstack_compute_flavor_v2" "c1-medium" {
  name      = "c1.medium"
  ram       = 4096
  vcpus     = 8
  disk      = 20
  is_public = true
}
resource "openstack_compute_flavor_v2" "c1-large" {
  name      = "c1.large"
  ram       = 4096
  vcpus     = 16
  disk      = 20
  is_public = true
}

resource "openstack_compute_flavor_v2" "r1-small" {
  name      = "r1.small"
  ram       = 4096
  vcpus     = 1
  disk      = 20
  is_public = true
}
resource "openstack_compute_flavor_v2" "r1-medium" {
  name      = "r1.medium"
  ram       = 8192
  vcpus     = 2
  disk      = 20
  is_public = true
}
resource "openstack_compute_flavor_v2" "r1-large" {
  name      = "r1.large"
  ram       = 16384
  vcpus     = 2
  disk      = 20
  is_public = true
}
