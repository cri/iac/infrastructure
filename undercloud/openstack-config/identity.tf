resource "openstack_identity_project_v3" "CRI_domain" {
  name      = "CRI"
  is_domain = true
}

resource "openstack_identity_project_v3" "CRI" {
  name      = "CRI"
  domain_id = openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_cri-roots" {
  name      = "cri-roots"
  domain_id = openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_cri-service-accounts" {
  name      = "cri-service-accounts"
  domain_id = openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_cri" {
  name      = "cri"
  domain_id = openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_role_v3" "admin" {
  name = "admin"
}

data "openstack_identity_role_v3" "member" {
  name = "member"
}

data "openstack_identity_role_v3" "load-balancer_member" {
  name = "load-balancer_member"
}

resource "openstack_identity_role_assignment_v3" "CRI_domain_cri-roots_admin" {
  domain_id = openstack_identity_project_v3.CRI_domain.id
  group_id  = data.openstack_identity_group_v3.CRI_cri-roots.id
  role_id   = data.openstack_identity_role_v3.admin.id
}

resource "openstack_identity_role_assignment_v3" "CRI_cri-roots_admin" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_cri-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

resource "openstack_identity_role_assignment_v3" "CRI_cri-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_cri-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}

resource "openstack_identity_role_assignment_v3" "CRI_cri-service-accounts_member" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_cri-service-accounts.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "CRI_cri-service-accounts_load-balancer_member" {
  project_id = openstack_identity_project_v3.CRI.id
  group_id   = data.openstack_identity_group_v3.CRI_cri-service-accounts.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}

resource "openstack_compute_quotaset_v2" "CRI_quotaset_compute" {
  project_id = openstack_identity_project_v3.CRI.id

  instances                   = 100
  cores                       = 1000
  ram                         = 1100 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 100
  server_group_members        = 30
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "CRI_quotaset_volume" {
  project_id = openstack_identity_project_v3.CRI.id

  volumes              = 100
  snapshots            = 100
  gigabytes            = 15000
  per_volume_gigabytes = 5000
}
resource "openstack_networking_quota_v2" "CRI_quotaset_network" {
  project_id = openstack_identity_project_v3.CRI.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 40
  security_group      = 100
  security_group_rule = 1000
}
