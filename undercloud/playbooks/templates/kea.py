#!/usr/bin/env python3
import json
import requests
import subprocess

KEA_SHARED_SECRET = "{{ fleet_manager_kea_shared_secret }}"

{% raw %}

headers = {
    "Kea-Shared-Secret": KEA_SHARED_SECRET,
}

config = requests.get(
    "https://fleet.pie.cri.epita.fr/api/fleet/dhcp_config_discovery/", headers=headers
).json()

generated = ""

for subnet in config["Dhcp4"]["subnet4"]:
    sub = subnet["subnet"].split("/")[0]
    netmask = "255.255.255.0"
    router = list(filter(lambda x: x["name"] == "routers", subnet["option-data"]))[0][
        "data"
    ]
    domain_name = list(
        filter(lambda x: x["name"] == "domain-name", subnet["option-data"])
    )[0]["data"]
    pool = subnet["pools"][0]["pool"]
    start = pool.split("-")[0]
    end = pool.split("-")[1]

    generated += f"""
      subnet {sub} netmask {netmask} {{
        option domain-name "{domain_name}";
        option routers {router};
        range {start} {end};
      }}
    """

    generated += "group {"
    for resa in subnet["reservations"]:
        hostname = resa["hostname"]
        ip = resa["ip-address"]
        mac = resa["hw-address"]
        domain_name = ".".join(hostname.split(".")[1:])

        generated += f"""
          host {hostname} {{
            option host-name "{hostname}";
            option domain-name "{domain_name}";
            fixed-address {ip};
            hardware ethernet {mac};
          }}
        """
    generated += "}"

with open("/etc/dhcp/kea.conf", "w") as f:
    f.write(generated)

subprocess.run(
    [
        "/bin/sh",
        "-c",
        "/usr/sbin/dhcpd -t && /usr/bin/systemctl restart isc-dhcp-server",
    ]
)
{% endraw %}
