resource "random_password" "k8s-undercloud_netbox_django" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_netbox_django" {
  path = "k8s-undercloud/netbox/django"
  data_json = jsonencode({
    SECRET_KEY = random_password.k8s-undercloud_netbox_django[0].result
  })
}

resource "random_password" "k8s-undercloud_netbox_admin" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_netbox_admin" {
  path = "k8s-undercloud/netbox/admin"
  data_json = jsonencode({
    password  = random_password.k8s-undercloud_netbox_admin[0].result
    api_token = random_password.k8s-undercloud_netbox_admin[1].result
  })
}
