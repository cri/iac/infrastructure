# The seed is sort of a hack to we always generate the same keys and they don't
# change between terraform runs. Changing it will regenerate all keys.

resource "random_password" "rndc-seed" {
  length  = 64
  special = false
}

data "external" "rndc-undercloud_cri_epita_fr" {
  program = ["${path.module}/rndc-secret.sh"]

  query = {
    key_name = "undercloud.cri.epita.fr"
    key_seed = random_password.rndc-seed.result
  }
}
resource "vault_generic_secret" "undercloud_dns_undercloud-cri-epita-fr-tsig" {
  path = "undercloud/dns/undercloud-cri-epita-fr-tsig"
  data_json = jsonencode({
    key_name   = data.external.rndc-undercloud_cri_epita_fr.result.key_name
    key_secret = data.external.rndc-undercloud_cri_epita_fr.result.key_secret
  })
}

data "external" "rndc-ceph_cri_epita_fr" {
  program = ["${path.module}/rndc-secret.sh"]

  query = {
    key_name = "ceph.cri.epita.fr"
    key_seed = random_password.rndc-seed.result
  }
}
resource "vault_generic_secret" "undercloud_dns_ceph-cri-epita-fr-tsig" {
  path = "undercloud/dns/ceph-cri-epita-fr-tsig"
  data_json = jsonencode({
    key_name   = data.external.rndc-ceph_cri_epita_fr.result.key_name
    key_secret = data.external.rndc-ceph_cri_epita_fr.result.key_secret
  })
}

data "external" "rndc-openstack_cri_epita_fr" {
  program = ["${path.module}/rndc-secret.sh"]

  query = {
    key_name = "openstack.cri.epita.fr"
    key_seed = random_password.rndc-seed.result
  }
}
resource "vault_generic_secret" "undercloud_dns_openstack-cri-epita-fr-tsig" {
  path = "undercloud/dns/openstack-cri-epita-fr-tsig"
  data_json = jsonencode({
    key_name   = data.external.rndc-openstack_cri_epita_fr.result.key_name
    key_secret = data.external.rndc-openstack_cri_epita_fr.result.key_secret
  })
}
