terraform {
  required_providers {
    external = {
      source  = "hashicorp/external"
      version = "2.3.3"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.12.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }

  backend "s3" {
    # this uses the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for
    # authentication
    bucket                      = "cri-terraform.s3.cri.epita.fr"
    key                         = "prod/secrets.tfstate"
    region                      = "default"
    endpoint                    = "https://s3.cri.epita.fr"
    force_path_style            = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "under"
}
provider "random" {}
provider "vault" {}

resource "vault_mount" "k8s-undercloud" {
  path = "k8s-undercloud"
  type = "kv"
  options = {
    version = 2
  }
}

resource "vault_mount" "undercloud" {
  path = "undercloud"
  type = "kv"
  options = {
    version = 2
  }
}
