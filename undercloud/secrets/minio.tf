resource "random_password" "k8s-undercloud_minio_admin-creds" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_minio_admin-creds" {
  path = "k8s-undercloud/minio/admin-creds"
  data_json = jsonencode({
    accesskey = random_password.k8s-undercloud_minio_admin-creds[0].result
    secretkey = random_password.k8s-undercloud_minio_admin-creds[1].result
  })
}
