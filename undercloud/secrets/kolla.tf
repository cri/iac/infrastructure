resource "vault_mount" "kolla" {
  path        = "kolla"
  type        = "kv"
  description = "kolla passwords"
  options = {
    version = 2
  }
}
