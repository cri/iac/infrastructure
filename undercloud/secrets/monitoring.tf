data "vault_generic_secret" "ceph_s3_users_cri-k8s-undercloud-thanos" {
  path = "ceph/s3/users/cri-k8s-undercloud-thanos"
}

resource "vault_generic_secret" "monitoring_thanos-objectstorage" {
  path = "k8s-undercloud/monitoring/thanos-objectstorage"
  data_json = jsonencode({
    "objstore.yml" = <<-CONFIG
      type: S3
      config:
        bucket: "cri-k8s-undercloud-thanos"
        endpoint: "s3.cri.epita.fr"
        access_key: "${data.vault_generic_secret.ceph_s3_users_cri-k8s-undercloud-thanos.data["access_key"]}"
        secret_key: "${data.vault_generic_secret.ceph_s3_users_cri-k8s-undercloud-thanos.data["secret_key"]}"
    CONFIG
  })
}
