data "vault_generic_secret" "ops_logging_loki-auth-map-password" {
  path = "k8s-ops/logging/loki-auth-map-password"
}

resource "vault_generic_secret" "logging_loki-auth" {
  path = "k8s-undercloud/logging/loki-auth"
  data_json = jsonencode({
    username = "k8s-undercloud"
    password = data.vault_generic_secret.ops_logging_loki-auth-map-password.data["k8s-undercloud"]
  })
}
