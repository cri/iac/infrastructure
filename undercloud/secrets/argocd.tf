//
// ArgoCD secret
//
resource "vault_generic_secret" "k8s-undercloud_argocd_secret" {
  path         = "k8s-undercloud/argocd/secret"
  disable_read = true
  data_json = jsonencode({
    "oidc.cri_undercloud.clientID"     = "FIXME"
    "oidc.cri_undercloud.clientSecret" = "FIXME"
    "oidc.cri.clientID"                = "FIXME"
    "oidc.cri.clientSecret"            = "FIXME"
  })
}
