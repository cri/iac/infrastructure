resource "vault_generic_secret" "k8s-undercloud_intranet_intranet-keytab" {
  path         = "k8s-undercloud/intranet/intranet-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "k8s-undercloud_intranet_ldap-keytab" {
  path         = "k8s-undercloud/intranet/ldap-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "k8s-undercloud_intranet_spnego-keytab" {
  path         = "k8s-undercloud/intranet/spnego-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "k8s-undercloud_intranet_algolia" {
  path         = "k8s-undercloud/intranet/algolia"
  disable_read = true
  data_json = jsonencode({
    DJANGO_ALGOLIA_API_KEY        = "FIXME"
    DJANGO_ALGOLIA_APP_ID         = "FIXME"
    DJANGO_ALGOLIA_SEARCH_API_KEY = "FIXME"
  })
}
resource "random_password" "k8s-undercloud_intranet_django-secrets" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_intranet_django-secrets" {
  path = "k8s-undercloud/intranet/django-secrets"
  data_json = jsonencode({
    DJANGO_DEFAULT_ADMIN_PASSWORD = random_password.k8s-undercloud_intranet_django-secrets[0].result
    DJANGO_SECRET_KEY             = random_password.k8s-undercloud_intranet_django-secrets[1].result
  })
}
resource "vault_generic_secret" "k8s-undercloud_intranet_s3" {
  path = "k8s-undercloud/intranet/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = vault_generic_secret.k8s-undercloud_minio_admin-creds.data["accesskey"]
    S3_SECRET_KEY = vault_generic_secret.k8s-undercloud_minio_admin-creds.data["secretkey"]
  })
}
resource "vault_generic_secret" "k8s-undercloud_intranet_azuread" {
  path = "k8s-undercloud/intranet/azuread"
  data_json = jsonencode({
    AZUREAD_OAUTH2_KEY    = "FIXME"
    AZUREAD_OAUTH2_SECRET = "FIXME"
  })
}
resource "random_password" "k8s-undercloud_intranet_kerberos" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_intranet_kerberos" {
  path = "k8s-undercloud/intranet/kerberos"
  data_json = jsonencode({
    adm-service-password = random_password.k8s-undercloud_intranet_kerberos[0].result
    kdc-service-password = random_password.k8s-undercloud_intranet_kerberos[1].result
  })
}
resource "random_password" "k8s-undercloud_intranet_kerberos-admin-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_intranet_kerberos-admin-password" {
  path = "k8s-undercloud/intranet/kerberos-admin-password"
  data_json = jsonencode({
    KRB5_ADMIN_PASSWORD = random_password.k8s-undercloud_intranet_kerberos-admin-password[0].result
  })
}
resource "random_password" "k8s-undercloud_intranet_ldap-admin-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_intranet_ldap-admin-password" {
  path = "k8s-undercloud/intranet/ldap-admin-password"
  data_json = jsonencode({
    LDAP_ADMIN_PASSWORD = random_password.k8s-undercloud_intranet_ldap-admin-password[0].result
  })
}
resource "random_password" "k8s-undercloud_intranet_ldap-config-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_intranet_ldap-config-password" {
  path = "k8s-undercloud/intranet/ldap-config-password"
  data_json = jsonencode({
    LDAP_CONFIG_PASSWORD = random_password.k8s-undercloud_intranet_ldap-config-password[0].result
  })
}
