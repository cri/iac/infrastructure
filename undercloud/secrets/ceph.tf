resource "vault_mount" "ceph" {
  path = "ceph"
  type = "kv"
  options = {
    version = 2
  }
}

resource "random_password" "ceph_s3_users_admin" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "ceph_s3_users_admin" {
  path = "ceph/s3/users/admin"
  data_json = jsonencode({
    access_key = random_password.ceph_s3_users_admin[0].result
    secret_key = random_password.ceph_s3_users_admin[1].result
  })
}
