resource "vault_generic_secret" "k8s-undercloud_harbor-s3" {
  path         = "k8s-undercloud/harbor/s3"
  disable_read = true
  data_json = jsonencode({
    REGISTRY_STORAGE_S3_ACCESSKEY = "FIXME"
    REGISTRY_STORAGE_S3_SECRETKEY = "FIXME"
  })
}

resource "random_password" "k8s-undercloud_harbor-admin-password" {
  count   = 1
  length  = 32
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_harbor-admin-password" {
  path = "k8s-undercloud/harbor/admin-password"
  data_json = jsonencode({
    HARBOR_ADMIN_PASSWORD = random_password.k8s-undercloud_harbor-admin-password[0].result
  })
}

resource "random_password" "k8s-undercloud_harbor-secret-key" {
  count   = 1
  length  = 16
  special = false
}
resource "vault_generic_secret" "k8s-undercloud_harbor-secret-key" {
  path = "k8s-undercloud/harbor/secret-key"
  data_json = jsonencode({
    secretKey = random_password.k8s-undercloud_harbor-secret-key[0].result
  })
}
