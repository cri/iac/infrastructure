resource "vault_mount" "acme" {
  path = "acme"
  type = "kv"
  options = {
    version = 2
  }
}

resource "vault_auth_backend" "acme-approle" {
  path = "acme-approle"
  type = "approle"
}

resource "vault_policy" "acme-acme-1_undercloud_cri_epita_fr" {
  name = "acme-acme-1.undercloud.cri.epita.fr"

  policy = <<-EOP
    path "acme/data/certs/*" {
      capabilities = ["create", "read", "update", "list"]
    }
    path "acme/metadata/certs/*" {
      capabilities = ["create", "read", "update", "list"]
    }
  EOP
}
resource "vault_approle_auth_backend_role" "acme-1_undercloud_cri_epita_fr" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "acme-1.undercloud.cri.epita.fr"
  token_policies = [vault_policy.acme-acme-1_undercloud_cri_epita_fr.name]
}
resource "vault_approle_auth_backend_role_secret_id" "acme-1_undercloud_cri_epita_fr" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.acme-1_undercloud_cri_epita_fr.role_name
}
resource "vault_generic_secret" "acme_servers-creds_acme-1_undercloud_cri_epita_fr" {
  path = "acme/servers-creds/acme-1.undercloud.cri.epita.fr"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.acme-1_undercloud_cri_epita_fr.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.acme-1_undercloud_cri_epita_fr.secret_id
  })
}

resource "vault_policy" "acme-ceph_cri_epita_fr" {
  name = "acme-ceph.cri.epita.fr"

  policy = <<-EOP
    path "acme/data/certs/cluster.ceph.cri.epita.fr" {
      capabilities = ["read"]
    }

    path "acme/data/certs/openstack.cri.epita.fr" {
      capabilities = ["read"]
    }
  EOP
}
resource "vault_approle_auth_backend_role" "ceph_cri_epita_fr" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "ceph.cri.epita.fr"
  token_policies = [vault_policy.acme-ceph_cri_epita_fr.name]
}
resource "vault_approle_auth_backend_role_secret_id" "ceph_cri_epita_fr" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.ceph_cri_epita_fr.role_name
}
resource "vault_generic_secret" "acme_servers-creds_ceph_cri_epita_fr" {
  path = "acme/servers-creds/ceph.cri.epita.fr"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.ceph_cri_epita_fr.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.ceph_cri_epita_fr.secret_id
  })
}

resource "vault_policy" "acme-restic-backup_undercloud_cri_epita_fr" {
  name = "acme-restic-backup.undercloud.cri.epita.fr"

  policy = <<-EOP
    path "acme/data/certs/restic-backup.undercloud.cri.epita.fr" {
      capabilities = ["read"]
    }
  EOP
}
resource "vault_approle_auth_backend_role" "restic-backup_undercloud_cri_epita_fr" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "restic-backup.undercloud.cri.epita.fr"
  token_policies = [vault_policy.acme-restic-backup_undercloud_cri_epita_fr.name]
}
resource "vault_approle_auth_backend_role_secret_id" "restic-backup_undercloud_cri_epita_fr" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.restic-backup_undercloud_cri_epita_fr.role_name
}
resource "vault_generic_secret" "acme_servers-creds_restic-backup_undercloud_cri_epita_fr" {
  path = "acme/servers-creds/restic-backup.undercloud.cri.epita.fr"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.restic-backup_undercloud_cri_epita_fr.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.restic-backup_undercloud_cri_epita_fr.secret_id
  })
}

resource "vault_policy" "acme-mail_cri_epita_fr" {
  name = "acme-mail.cri.epita.fr"

  policy = <<-EOP
    path "acme/data/certs/mail.undercloud.cri.epita.fr" {
      capabilities = ["read"]
    }
  EOP
}
resource "vault_approle_auth_backend_role" "mail_cri_epita_fr" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "mail.cri.epita.fr"
  token_policies = [vault_policy.acme-mail_cri_epita_fr.name]
}
resource "vault_approle_auth_backend_role_secret_id" "mail_cri_epita_fr" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.mail_cri_epita_fr.role_name
}
resource "vault_generic_secret" "acme_servers-creds_mail_cri_epita_fr" {
  path = "acme/servers-creds/mail.cri.epita.fr"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.mail_cri_epita_fr.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.mail_cri_epita_fr.secret_id
  })
}
