//
// Postgres backup
//
resource "vault_generic_secret" "k8s-undercloud_postgres_pod-secrets" {
  path         = "k8s-undercloud/postgres/pod-secrets"
  disable_read = true
  data_json = jsonencode({
    AWS_ACCESS_KEY_ID     = "FIXME"
    AWS_SECRET_ACCESS_KEY = "FIXME"
  })
}
