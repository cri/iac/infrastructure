resource "vault_generic_secret" "k8s-undercloud_cert-manager_undercloud-cri-epita-fr-tsig" {
  path = "k8s-undercloud/cert-manager/undercloud-cri-epita-fr-tsig"
  data_json = jsonencode({
    tsig-name = vault_generic_secret.undercloud_dns_undercloud-cri-epita-fr-tsig.data.key_name
    tsig-key  = vault_generic_secret.undercloud_dns_undercloud-cri-epita-fr-tsig.data.key_secret
  })
}
