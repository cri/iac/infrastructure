#!/usr/bin/env bash

set -euo pipefail

eval "$(jq -r '@sh "KEY_NAME=\(.key_name) KEY_SEED=\(.key_seed)"')"

KEY_SECRET="$(echo -n "${KEY_SEED}" | openssl dgst -sha256 -mac HMAC -macopt key:"${KEY_NAME}" | sed 's/^.*= //')"

jq -n \
  --arg key_name "${KEY_NAME}" \
  --arg key_secret "${KEY_SECRET}" \
  '{"key_name": $key_name, "key_secret": $key_secret}'
