# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aminueza/minio" {
  version     = "1.5.2"
  constraints = "1.5.2"
  hashes = [
    "h1:rAngD3TtfUD9ZohzFo/QJe+2ae+kTmAzn4N2n6ayOOo=",
    "zh:02d90c08a5d8efddb7388eb74dae1722823a9bc6e54fc9e7e63a711108d20d58",
    "zh:03e541c770dc7785502ab6080e62328475a1af147957b3f2761750bf4c6cbf56",
    "zh:172b7538cef2ba2ccd79b5a84dcd27a88fbbe16f5fb1b9e3d2b6a05d63e17b29",
    "zh:3becdb228c2f78333265fec82abd16b1a14d86931cec96ff2726b5beafc290ac",
    "zh:4a0066767025a94b452fad7c0c58aa3103c1c521745b6c301ca64caa874501a2",
    "zh:4cd019edc99c6884d755f0f35122cb18bdd50ffab3a0690ecde139d2224d849f",
    "zh:5430f4d3cf757205d6b2ea82640475d0b164c61da9b1b944c3e66f829ed2eb82",
    "zh:882efeb742a3f1112034c05031b4ad686dda08ed7c6db6168339433b125288c4",
    "zh:99ad9e466ebabf83208a358230b871aa1c3576224a9112ee1038591a5e2154dc",
    "zh:9e06cefd61da232ca59c02e4a3d007c782c607af81f1640ef256ded367dd1730",
    "zh:c711cbd29a5d10411ee8510bf7ed49955af8d52c77bd3983f5f863595cf0c9c7",
    "zh:f7d7222bc796f362cf3b0211aca3ee256ac5a88cd52a1ddfbe918f01d8ed7970",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:2ve7G+YXMIUiNSH+J7daqU2Jg6WOvaOfsUfllXFwOOQ=",
    "zh:3dd0f4f12f5a479941422bc413ea147a76253c9d1bdb8dd2d098146c80f90aa5",
    "zh:4132382680ec77dda4713fd4701cbc7dcc08ef4742fb997961c3332c30b0ae12",
    "zh:56eb6b44bdbaf2f3f37a1df35c01405af5cf6eea988f3e6441e4d70391067918",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7e5f78948af1118a870d1caeabaff6dd72ad3f17c08c7ce986eab0aab9ba5694",
    "zh:8648ec617934ee880ce453011e9d8e2070a6db1b34ce11c007511f9399624d98",
    "zh:b2ded1dc6fd8c63dadd160a3360fb717f6808b5ad058be2af162be170aaefc3a",
    "zh:b88850e96c489dc8b5c66682bd166d6bb4a02cd6e943ba1d411cab911efd9487",
    "zh:c22c108bd60fd1af3b6fbea65018b069f731c95b1fb3900a052cef2b7fe2341f",
    "zh:c834ee80617c08f670826c8e566fcb01b30986ce996fbfc6fc3e9d838ced4d5f",
    "zh:cd4e6ecf2925915ad83cbdb962f8af65a70aebf5811fafe3f16cba69c1b39a6b",
    "zh:f6d39c4d3861ff682969a1fe4b960b7a27eda49c5a9747f20e24c56f3817a4cd",
  ]
}

provider "registry.terraform.io/rissson/rgw" {
  version     = "0.3.1"
  constraints = "0.3.1"
  hashes = [
    "h1:ahld3vA/h2ZlwnmyvL4R5N8n6kk0KR6x55QuoQc7pAg=",
    "zh:1a8c925b8232e99330a9c0dd4d4fc0c5988292d0ca66fe291eab27215d01aa69",
    "zh:2acd5cebdfe5b1a8adb3f099abc4410ca63c2068327131ef262a67225e4f338c",
    "zh:5d0fae43df1560ab5aebdbe2a811f11bc45b09dbfc34a5c4ab692a877449013d",
    "zh:76357b5ae13cf47fe633c113e8c0ce9e7da9f09290a63241861ebbd79009c91b",
    "zh:781e42e54f6c6381b29f202ea8a421df83cdb18f46bbfb3f2e4f09218ce04d0f",
    "zh:87988c4fe90a6f78dac79fa6d09923649fd5e115a9d1b341f8d78eb4f7d6a06a",
    "zh:88e67dd7f11c86a280989742492806233ca2a9d8dc0ede787ed2c16617b09b64",
    "zh:952399d3127dfa9b531c69dc382431ff831e2e7b90ba4afd9086dfbd4c03ee38",
    "zh:a562afde67e83fb94f05f55dd50c48969bf48b52de4e58d1b6a7c63567b6f9d5",
    "zh:ada317305c08ff5218435abfb65f59c38eaa7b32e62555d7f9e1bca046dfeec2",
    "zh:b681e9d6628a6aa32dd8115975053d69b6c3769087fcb5c76a13908e74bc5096",
    "zh:c6f6a75dd32186d3bdb0a98fac1aa92408811280277f438ac64ea27d9500c681",
    "zh:f5b42546ca075b67d12f18e230723b7640e09d04451bb167a360e8d157ba8f5e",
  ]
}
