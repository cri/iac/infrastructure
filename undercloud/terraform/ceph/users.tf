module "acu-intranet" {
  source  = "./user"
  user_id = "acu-intranet"
}

module "acu-scrooge" {
  source  = "./user"
  user_id = "acu-scrooge"
}

module "forge-intranet" {
  source       = "./user"
  user_id      = "forge-intranet"
  display_name = "Forge Intranet"

  buckets = {
    "documents.intranet.forge.epita.fr" = {
      acl = "private"
    }
    "traces.intranet.forge.epita.fr" = {
      acl = "private"
    }
  }
}

module "forge-intranet-git-lfs" {
  source       = "./user"
  user_id      = "forge-intranet-git-lfs"
  display_name = "Git LFS server"

  buckets = {
    "forge-intranet-git-lfs" = {
      acl = "private"
    }
  }
}

module "assistants-k8s-prod-1-thanos" {
  source  = "./user"
  user_id = "assistants-k8s-prod-1-thanos"
  buckets = {
    assistants-k8s-prod-1-thanos = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "ceph-dashboard" {
  source  = "./user"
  user_id = "ceph-dashboard"
}

module "cri-blog-dev" {
  source  = "./user"
  user_id = "cri-blog-dev"
  buckets = {
    "blog-dev.s3.cri.epita.fr" = {
      // Should be public-read
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-ci-pxe-images" {
  source  = "./user"
  user_id = "cri-ci-pxe-images"
  buckets = {
    "cri-nix-cache.s3.cri.epita.fr" = {
      // Should be public-read
      acl = "" // cannot be specified as bucket was imported
    }
    "cri-pxe-images.s3.cri.epita.fr" = {
      // Should be public-read
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-criscord" {
  source  = "./user"
  user_id = "cri-criscord"
}

module "cri-django-exam" {
  source       = "./user"
  user_id      = "cri-django-exam"
  display_name = "CRI Django Exam"
}

module "cri-fleet-manager" {
  source  = "./user"
  user_id = "cri-fleet-manager"
}

module "cri-hedgedoc" {
  source  = "./user"
  user_id = "cri-hedgedoc"
  buckets = {
    "hedgedoc-data.cri.epita.fr" = {
      // Should be ?
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-k8s-ops-loki" {
  source       = "./user"
  user_id      = "cri-k8s-ops-loki"
  display_name = "CRI k8s ops Loki"
  buckets = {
    cri-k8s-ops-loki = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-k8s-ops-thanos" {
  source  = "./user"
  user_id = "cri-k8s-ops-thanos"
  buckets = {
    cri-k8s-ops-thanos = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-k8s-prod-1-thanos" {
  source  = "./user"
  user_id = "cri-k8s-prod-1-thanos"
  buckets = {
    cri-k8s-prod-1-thanos = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-k8s-undercloud-thanos" {
  source  = "./user"
  user_id = "cri-k8s-undercloud-thanos"
  buckets = {
    cri-k8s-undercloud-thanos = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-terraform" {
  source  = "./user"
  user_id = "cri-terraform"
  buckets = {
    "cri-terraform.s3.cri.epita.fr" = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "gitlab" {
  source  = "./user"
  user_id = "gitlab"
  buckets = {
    gitlab-artifacts = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-backup = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-dependency-proxy = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-externaldiffs = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-lfs = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-packages = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-pages = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-pseudonymizer = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-registry = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-terraform = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-tmp = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
    gitlab-uploads = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "intranet" {
  source  = "./user"
  user_id = "intranet"
}

module "maas" {
  source  = "./user"
  user_id = "maas"
  buckets = {
    cri-maas = {
      // Should be private
      acl = "" // cannot be specified as bucket was imported
    }
  }
}

module "cri-main-k8s-prod-1-postgres" {
  source       = "./user"
  display_name = "CRI Main Postgres k8s prod-1"
  user_id      = "cri-main-k8s-prod-1-postgres"
  buckets = {
    cri-main-k8s-prod-1-postgres-wal = {
      acl = "private"
    }
  }
}

module "forge-dev-k8s-prod-1-postgres" {
  source       = "./user"
  display_name = "Forge::dev Postgres k8s prod-1"
  user_id      = "forge-dev-k8s-prod-1-postgres"
  buckets = {
    forge-dev-k8s-prod-1-postgres-wal = {
      acl = "private"
    }
  }
}
