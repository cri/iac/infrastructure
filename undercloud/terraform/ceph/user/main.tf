resource "rgw_user" "this" {
  user_id      = var.user_id
  display_name = var.display_name
}

resource "vault_generic_secret" "this" {
  path = "${var.secret_prefix}/${var.user_id}"
  data_json = jsonencode({
    access_key = rgw_user.this.keys[0].access_key
    secret_key = rgw_user.this.keys[0].secret_key
  })
}

provider "minio" {
  minio_server     = "s3.cri.epita.fr"
  minio_access_key = rgw_user.this.keys[0].access_key
  minio_secret_key = rgw_user.this.keys[0].secret_key
  minio_ssl        = true
}

resource "minio_s3_bucket" "this" {
  for_each = var.buckets
  bucket   = each.key
  acl      = each.value.acl
}
