terraform {
  required_providers {
    minio = {
      source  = "aminueza/minio"
      version = "1.5.2"
    }
    rgw = {
      source  = "rissson/rgw"
      version = "0.3.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}
