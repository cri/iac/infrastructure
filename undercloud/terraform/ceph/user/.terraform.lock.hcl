# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aminueza/minio" {
  version = "1.5.2"
  hashes = [
    "h1:3+6U/ubthvfc2kEE1j0fPoLKqf5RPbJT0jXweHFeGU0=",
    "h1:CihwrCuBLUK/dvUTN/jcGUTXHDY4goz6iXXD47OXORw=",
    "h1:LGoLvtst8TFHTMRvnwY5A2tBfX10daqvJRfE3l1NVYI=",
    "h1:M/7DGhiV8NZtAQqI8jVGIR1vEhHHaGfj3DiDmDX/cgc=",
    "h1:RbAqXX0jPtz7HTMhlfBp5yPHp0YAPRigjAPtMfCjccg=",
    "h1:Rr8h9Px9vrU3idjjWKWm86miDUYZdhRAoRjMxGFFL2o=",
    "h1:d4yL8OW9I9djVp2L29Pdnuv9WmPNps9wGaozLUX1ZV0=",
    "h1:fBoapXuY7wVHU+eNRaDe96/GwL2WCOQWuCG+/eCk+vY=",
    "h1:fh43+tD/79H68eACVi+RsAHm57uDTmiLa/mX7U21inY=",
    "h1:fhkAAC36egk1dH6JcEdeubIwOCZdKe/Ey+DLGsEMh5w=",
    "h1:rAngD3TtfUD9ZohzFo/QJe+2ae+kTmAzn4N2n6ayOOo=",
    "h1:st0zSv1hHW5jyPpKDAIo/uyS5P57X2mn6CqQaXRZdrI=",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:2ve7G+YXMIUiNSH+J7daqU2Jg6WOvaOfsUfllXFwOOQ=",
    "h1:5wenWB9fcfhGd/qB1G3TfXdPCiRpYJdcAughrV6qrB4=",
    "h1:86BIloLhu//b81kvK8YeK8nzeIXPBT0TGGNR/xLfYkw=",
    "h1:CAYmBNuqkUuBUI/9g/DxoZUA+FvlfeN91fYB48/li2k=",
    "h1:EzqgPMt20qzRlnX2DFTzyQuVeYCwZWB3Vfr9q5Pra0s=",
    "h1:FPcuQQlgeX47Zb6TFWak3pLNi9Hm842i5cH8iHlSgB0=",
    "h1:IG6KwVl/0XAhyObx0wbHV3zgrj1FL4nFp5RuI0dNGSs=",
    "h1:O92oyLi7wgyec/lbODY3oPUwahhns0FOBTaSMrJ9KRE=",
    "h1:Z38mV+mWZPqckGtwCQS6P6Rdp1gfSVYnQfoC5trWF/I=",
    "h1:dg/MF44OMJjwRL13YhwM3nFuZJPn0R4MsR3SjiReGNQ=",
    "h1:v49zBYXmqCit5CZ1tIExBka0lKTMD1paMX0FdmpTv1o=",
  ]
}

provider "registry.terraform.io/rissson/rgw" {
  version = "0.3.1"
  hashes = [
    "h1:0e2EVWNWjOw/sqx7Kg/0mwroZOtyOmqb6cMzlJi5XUc=",
    "h1:56B3TntBh3XeG4bhOMjC47cU6/wKs+ujoiv1A9zQV48=",
    "h1:6HO5f2gXvjZ7MrgbA769VVybSJWP1KwY1OAngOz+Gn8=",
    "h1:EUQvOla/jaON9gNAqM7GN559HT57Dk7RdeDrAoBSQEA=",
    "h1:JSwTtKIVJ3x+rPVEw1Z7aS25TPRo5NBtLLSnKNEMhS4=",
    "h1:LW1aYOQaYo1LSCr1zvvLXS7O9eTCA2sOU3ZCZeVbC14=",
    "h1:QklNzQJdVFiQeut/gGq2cbjeJ4qwl+KO5MWbgNLb+uA=",
    "h1:TTa9dPjkkOh2+xchSpC6QsfwsvvYETynRIPD611X9VE=",
    "h1:TXlBf2MscT8wlqNmE65PuNY7SMT5GmgAUh0DIkKkwk0=",
    "h1:Wim6CgmlzoMHYa0MmuNnka9toGRZtIh4srZRZ1ANnSQ=",
    "h1:ahld3vA/h2ZlwnmyvL4R5N8n6kk0KR6x55QuoQc7pAg=",
    "h1:o8FP8WmAgK3GSlqohkhOBi4ZAjaE6snkL56GrJpsKL8=",
    "h1:vKJvxvfvv/5m0RqKAuSIKCFnJDVoaF03tD+iWeJ5p/I=",
  ]
}
