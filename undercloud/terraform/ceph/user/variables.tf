variable "user_id" {
  type = string
}

variable "display_name" {
  type    = string
  default = ""
}

variable "secret_prefix" {
  type    = string
  default = "ceph/s3/users"
}

variable "buckets" {
  type = map(object({
    acl = string
  }))
  default = {}
}
