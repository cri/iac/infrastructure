terraform {
  required_providers {
    rgw = {
      source  = "rissson/rgw"
      version = "0.3.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }

  backend "s3" {
    # this uses the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for
    # authentication
    bucket                      = "cri-terraform.s3.cri.epita.fr"
    key                         = "prod/undercloud/ceph.tfstate"
    region                      = "default"
    endpoint                    = "https://s3.cri.epita.fr"
    force_path_style            = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }
}

provider "vault" {}

// This user must be created manually with
// radosgw-admin user create --uid admin --display-name "Admin User" --caps "buckets=*;users=*;usage=read;metadata=read;zone=read"
data "vault_generic_secret" "ceph_s3_users_admin" {
  path = "ceph/s3/users/admin"
}

provider "rgw" {
  endpoint   = "https://s3.cri.epita.fr"
  access_key = data.vault_generic_secret.ceph_s3_users_admin.data.access_key
  secret_key = data.vault_generic_secret.ceph_s3_users_admin.data.secret_key
}
