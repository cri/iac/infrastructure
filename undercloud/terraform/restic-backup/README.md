# Terraform for restic S3 buckets and secrets

Note that due to the way the minio provider handles policies, they might get
re-created and not reassigned to their respected users. You're better off with
deleting all of them first, and recreating them.
