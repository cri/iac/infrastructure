module "k8s-undercloud-velero" {
  source = "./modules/velero"

  name         = "k8s-undercloud-velero"
  cluster_name = "in-cluster"
}
