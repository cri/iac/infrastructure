module "rt-2_srv_cri_epita_fr-data" {
  source = "./modules/restic"

  name     = "rt-2.srv.cri.epita.fr-data"
  ro_users = local.ro_users
}
