module "reverse-pie-1_srv_cri_epita_fr-data" {
  source = "./modules/restic"

  name = "reverse-pie-1.srv.cri.epita.fr-data"
}

module "reverse-pie-1_srv_cri_epita_fr-logs" {
  source = "./modules/restic"

  name = "reverse-pie-1.srv.cri.epita.fr-logs"
}
