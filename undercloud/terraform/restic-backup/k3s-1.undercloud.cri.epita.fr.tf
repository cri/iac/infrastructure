module "k3s-1_undercloud_cri_epita_fr-data" {
  source = "./modules/restic"

  name     = "k3s-1.undercloud.cri.epita.fr-data"
  ro_users = local.ro_users
}
