module "moodle_cri_openstack_epita_fr-data" {
  source = "./modules/restic"

  name     = "moodle.cri.openstack.epita.fr-data"
  ro_users = local.ro_users
}
