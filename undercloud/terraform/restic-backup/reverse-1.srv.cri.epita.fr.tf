module "reverse-1_srv_cri_epita_fr-config" {
  source = "./modules/restic"

  name     = "reverse-1.srv.cri.epita.fr-config"
  ro_users = local.ro_users
}

module "reverse-1_srv_cri_epita_fr-data" {
  source = "./modules/restic"

  name     = "reverse-1.srv.cri.epita.fr-data"
  ro_users = local.ro_users
}

module "reverse-1_srv_cri_epita_fr-logs" {
  source = "./modules/restic"

  name     = "reverse-1.srv.cri.epita.fr-logs"
  ro_users = local.ro_users
}
