module "k8s-ops-velero" {
  source = "./modules/velero"

  name         = "k8s-ops-velero"
  cluster_name = "ops"
}
