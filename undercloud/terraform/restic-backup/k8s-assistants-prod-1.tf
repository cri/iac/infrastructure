module "k8s-assistants-prod-1-velero" {
  source = "./modules/velero"

  name         = "k8s-assistants-prod-1-velero"
  cluster_name = "assistants-prod-1"
}
