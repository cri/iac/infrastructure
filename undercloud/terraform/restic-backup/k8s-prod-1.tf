module "k8s-prod-1-velero" {
  source = "./modules/velero"

  name         = "k8s-prod-1-velero"
  cluster_name = "prod-1"
  ro_users     = local.ro_users
}
