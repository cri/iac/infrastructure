terraform {
  required_providers {
    rgw = {
      source  = "rissson/rgw"
      version = "0.3.1"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.23.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }

  backend "s3" {
    # this uses the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for
    # authentication
    bucket                      = "cri-terraform.s3.cri.epita.fr"
    key                         = "prod/undercloud/restic-backup.tfstate"
    region                      = "default"
    endpoint                    = "https://s3.cri.epita.fr"
    force_path_style            = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }
}

provider "random" {}
provider "vault" {}

resource "vault_mount" "restic" {
  path = "restic"
  type = "kv"
  options = {
    version = 2
  }
}

resource "random_password" "restic_s3-admin" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "restic_s3-admin" {
  path = "restic/s3-admin"
  data_json = jsonencode({
    accesskey = random_password.restic_s3-admin[0].result
    secretkey = random_password.restic_s3-admin[1].result
  })
}

provider "aws" {
  region                      = "us-east-1" # required to avoid an "Invalid AWS Region" error
  s3_use_path_style           = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  access_key = resource.vault_generic_secret.restic_s3-admin.data.accesskey
  secret_key = resource.vault_generic_secret.restic_s3-admin.data.secretkey

  endpoints {
    s3 = "https://restic-backup.undercloud.cri.epita.fr"
  }
}
provider "rgw" {
  endpoint   = "https://restic-backup.undercloud.cri.epita.fr"
  access_key = resource.vault_generic_secret.restic_s3-admin.data.accesskey
  secret_key = resource.vault_generic_secret.restic_s3-admin.data.secretkey
}
