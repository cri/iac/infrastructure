terraform {
  required_providers {
    rgw = {
      source  = "rissson/rgw"
      version = "0.3.1"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.23.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}
