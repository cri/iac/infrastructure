variable "ro_users" {
  type    = list(any)
  default = []
}

variable "disable_versionning" {
  type    = bool
  default = false
}

variable "name" {
  type = string
}

variable "vault_prefix" {
  type    = string
  default = "restic"
}
