resource "random_password" "restic" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "restic" {
  path = "${var.vault_prefix}/${var.name}/restic"
  data_json = jsonencode({
    password = random_password.restic[0].result
  })
}

resource "rgw_user" "backup" {
  user_id      = "backup_${var.name}"
  display_name = "backup_${var.name}"
  generate_key = false
}
resource "vault_generic_secret" "backup" {
  path = "${var.vault_prefix}/${var.name}/backup"
  data_json = jsonencode({
    accesskey = rgw_user.backup.keys[0].access_key
    secretkey = rgw_user.backup.keys[0].secret_key
  })
}

resource "rgw_user" "prune" {
  user_id      = "prune_${var.name}"
  display_name = "prune_${var.name}"
  generate_key = false
}
resource "vault_generic_secret" "prune" {
  path = "${var.vault_prefix}/${var.name}/prune"
  data_json = jsonencode({
    accesskey = rgw_user.prune.keys[0].access_key
    secretkey = rgw_user.prune.keys[0].secret_key
  })
}

resource "aws_s3_bucket" "this" {
  bucket = var.name
}
resource "aws_s3_bucket_versioning" "this" {
  bucket = aws_s3_bucket.this.id
  versioning_configuration {
    status = var.disable_versionning ? "Disabled" : "Enabled"
  }
}
resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.this.json
}
data "aws_iam_policy_document" "this" {
  statement {
    sid = "backup_policy_lock_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:DeleteObject",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/locks",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/locks/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam:::user/${rgw_user.backup.user_id}"]
    }
  }
  statement {
    sid = "backup_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam:::user/${rgw_user.backup.user_id}"]
    }
  }
  statement {
    sid = "prune_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:DeleteObject",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam:::user/${rgw_user.prune.user_id}"]
    }
  }
  statement {
    sid = "fetch_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = [for user in var.ro_users : "arn:aws:iam:::user/${user.user_id}"]
    }
  }
}
