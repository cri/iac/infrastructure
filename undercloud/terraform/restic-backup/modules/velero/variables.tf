variable "cluster_name" {
  type = string
}

variable "name" {
  type = string
}

variable "ro_users" {
  type    = list(any)
  default = []
}

variable "vault_prefix" {
  type    = string
  default = "restic"
}
