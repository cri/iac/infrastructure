resource "random_password" "restic" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "restic" {
  path = "${var.vault_prefix}/${var.name}/restic"
  data_json = jsonencode({
    password = random_password.restic[0].result
  })
}

resource "rgw_user" "velero" {
  user_id      = "velero_${var.name}"
  display_name = "velero_${var.name}"
  generate_key = false
}
resource "vault_generic_secret" "velero" {
  path = "${var.vault_prefix}/${var.name}/velero"
  data_json = jsonencode({
    accesskey = rgw_user.velero.keys[0].access_key
    secretkey = rgw_user.velero.keys[0].secret_key
  })
}

resource "aws_s3_bucket" "this" {
  bucket = var.name
}
resource "aws_s3_bucket_versioning" "this" {
  bucket = aws_s3_bucket.this.id
  versioning_configuration {
    status = "Disabled"
  }
}
resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.this.json
}
data "aws_iam_policy_document" "this" {
  statement {
    sid = "velero_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam:::user/${rgw_user.velero.user_id}"]
    }
  }
  statement {
    sid = "fetchlist_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = [for user in var.ro_users : "arn:aws:iam:::user/${user.user_id}"]
    }

    condition {
      test     = "StringLike"
      variable = "s3:prefix"

      values = [
        "${var.cluster_name}",
        "${var.cluster_name}/",
        "${var.cluster_name}/restic/*",
      ]
    }
  }
  statement {
    sid = "fetch_policy_${aws_s3_bucket.this.bucket}"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/${var.cluster_name}/restic",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/${var.cluster_name}/restic/*",
    ]

    principals {
      type        = "AWS"
      identifiers = [for user in var.ro_users : "arn:aws:iam:::user/${user.user_id}"]
    }
  }
}
