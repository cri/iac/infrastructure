terraform {
  required_providers {
    rgw = {
      source  = "rissson/rgw"
      version = "0.3.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}
