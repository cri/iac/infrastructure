resource "rgw_user" "this" {
  user_id      = "fetch_${var.name}"
  display_name = "fetch_${var.name}"
  generate_key = false
}
resource "vault_generic_secret" "this" {
  path = "${var.vault_prefix}/${var.name}/backup"
  data_json = jsonencode({
    accesskey = rgw_user.this.keys[0].access_key
    secretkey = rgw_user.this.keys[0].secret_key
  })
}
