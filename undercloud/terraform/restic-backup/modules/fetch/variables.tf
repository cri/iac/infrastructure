variable "name" {
  type = string
}

variable "vault_prefix" {
  type    = string
  default = "restic"
}
