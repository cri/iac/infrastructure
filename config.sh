#! /bin/sh
# shellcheck disable=SC2155
export AWS_ACCESS_KEY_ID="$(VAULT_ADDR='https://vault.cri.epita.fr:443' vault kv get -field=ACCESS_KEY infra/s3-terraform)"
export AWS_SECRET_ACCESS_KEY="$(VAULT_ADDR='https://vault.cri.epita.fr:443' vault kv get -field=SECRET_KEY infra/s3-terraform)"
export OS_PASSWORD="$(VAULT_ADDR='https://vault.cri.epita.fr:443' vault kv get -field=password infra/openstack/admin)"
export OS_CLOUD="openstack"
export VAULT_ADDR="https://vault.cri.epita.fr:443"
export SALTAPI_URL=https://salt-api.pie.cri.epita.fr:443/
export SALTAPI_EAUTH=ldap
