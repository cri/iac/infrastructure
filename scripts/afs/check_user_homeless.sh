#!/usr/bin/env bash

usage() {
  echo "$0"
  exit 1
}

[ $# -eq 0 ] || usage

ldapsearch -x uid -b 'ou=users,dc=cri,dc=epita,dc=fr' -ZZ -h ldap.pie.cri.epita.fr 2>/dev/null | grep '^uid:' | cut -d' ' -f2 | while read -r login; do
  if ! ./check_home.sh "$login" > /dev/null; then
    echo "$login"
  fi
done

