#!/usr/bin/env bash

usage() {
  echo "$0 login id"
  exit 1
}

[ $# -eq 2 ] || usage

login="$1"
id="$2"

vos remove -id "user_$id"

# shellcheck disable=SC3000-SC4000
l1="${login:0:1}"
# shellcheck disable=SC3000-SC4000
l2="${login:0:2}"
fs rmmount "/afs/cri.epita.fr/user/$l1/$l2/$login"

pts delete -nameorid "$id"
