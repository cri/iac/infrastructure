#!/usr/bin/env bash

total_volumes=0
nb_servers="$(vos listaddrs | wc -l)"

for server in $(vos listaddrs); do
  count="$(vos listvol -server "${server}" | grep -c '^user_')"
  total_volumes="$(( total_volumes + count ))"
  echo "${server}: ${count}"
done

echo "total: ${total_volumes}"
echo "average: $(( total_volumes / nb_servers ))"
