#!/usr/bin/env bash

usage() {
  echo "$0 login"
  exit 1
}

[ $# -eq 1 ] || usage

login="$1"

if ! id -u "$login" > /dev/null 2>&1; then
  echo "User $login does not exists"
  exit 2
fi
id=$(id -u "$1")

if vos examine "user_$id" > /dev/null 2>&1; then
  echo "Volume user_$id ($login) exists"
  exit 0
else
  echo "Volume user_$id ($login) does not exists"
  exit 1
fi
