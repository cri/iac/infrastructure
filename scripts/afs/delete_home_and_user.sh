#!/usr/bin/env bash

usage() {
  echo "$0 login"
  exit 1
}

[ $# -eq 1 ] || usage

login="$1"

# shellcheck disable=SC3000-SC4000
l1="${login:0:1}"
# shellcheck disable=SC3000-SC4000
l2="${login:0:2}"

vol="$(fs lsmount -dir "/afs/cri.epita.fr/user/$l1/$l2/$login" | cut -d' ' -f8 | sed "s/'%\(.*\)'$/\1/")"
id="$(echo "$vol" | sed 's/^user_\([0-9]*\)$/\1/')"

vos remove -id "$vol"
fs rmmount "/afs/cri.epita.fr/user/$l1/$l2/$login"
pts delete -nameorid "$id"
