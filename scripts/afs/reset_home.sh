#!/usr/bin/env bash

usage() {
  echo "$0 login"
  exit 1
}

[ $# -eq 1 ] || usage

id=$(id -u "$1")
group=$(id -g "$1")
login="$1"

if ! vos examine "user_$id" > /dev/null 2>&1; then
  echo "Volume user_$id ($login) does not exists!"
  exit 1
fi

# shellcheck disable=SC3000-SC4000
l1="${login:0:1}"
# shellcheck disable=SC3000-SC4000
l2="${login:0:2}"

find "/afs/.cri.epita.fr/user/$l1/$l2/$login/" -mindepth 1 -delete
mkdir "/afs/.cri.epita.fr/user/$l1/$l2/$login/u/"
cp -R /afs/cri.epita.net/resources/confs "/afs/.cri.epita.fr/user/$l1/$l2/$login/u/.confs"
chown -R "$id:$group" "/afs/.cri.epita.fr/user/$l1/$l2/$login"
