#!/usr/bin/env bash

export LC_ALL=C # For sort problems

TMP_DIR="$(mktemp -p /tmp -d 'afs-report.XXX')"

clean() {
  rm -rf "$TMP_DIR"
  exit 0
}

trap clean 1 2 3 15

users() {
  echo "### Users Reports ###"
  echo

  echo "Total LDAP: $(wc -l < "$TMP_DIR/ldap_users")"
  echo "Total AFS: $(wc -l < "$TMP_DIR/afs_users")"
  echo

  echo "-- Admins --"
  grep '\.admin$' "$TMP_DIR/afs_users"
  echo

  u=$(comm -2 -3 "$TMP_DIR/afs_users" "$TMP_DIR/ldap_users_sed" | grep -v '\.admin$' | grep -v '^anonymous$')
  if [ -n "$u" ]; then
    echo "-- Users existing only in afs --"
    echo "$u"
    echo
  fi

  u=$(comm -1 -3 "$TMP_DIR/afs_users" "$TMP_DIR/ldap_users_sed" | grep -v '\.admin$' | grep -v '^anonymous$')
  if [ -n "$u" ]; then
    echo "-- Users without afs account --"
    echo "$u"
    echo
  fi

  bad_uid="$(join "$TMP_DIR/ldap_users_ids" "$TMP_DIR/afs_users_ids" | awk '$2 != $3 {print $1}')"
  if [ -n "$bad_uid" ]; then
    echo "-- Users with bad UID --"
    echo "$bad_uid"
    echo
  fi
}

volumes() {
  echo "### Volumes Reports ###"
  echo


  echo "Number of volumes: $(wc -l "$TMP_DIR/volumes")"
  echo

  echo "-- Not user volumes --"
  grep -v '^user_' "$TMP_DIR/volumes"
  echo

  grep -v '.admin ' "$TMP_DIR/afs_users_ids" | grep -v anonymous | cut -d' ' -f2 | sort > "$TMP_DIR/afs_ids"

  v=$(comm -2 -3 "$TMP_DIR/afs_ids" "$TMP_DIR/volumes_ids")
  if [ -n "$v" ]; then
    echo "-- Users without volume --"
    echo "$v"
    echo
  fi

  v=$(comm -1 -3 "$TMP_DIR/afs_ids" "$TMP_DIR/volumes_ids")
  if [ -n "$v" ]; then
    echo "-- Volumes without users --"
    echo "$v"
    echo
  fi
}

ldapsearch -x uid -b 'ou=users,dc=cri,dc=epita,dc=fr'| sed -n 's/^uid: \(.*\)/\1/p' | sort > "$TMP_DIR/ldap_users"
 sed 's/\./_/g' "$TMP_DIR/ldap_users" | sort > "$TMP_DIR/ldap_users_sed"
pts listentries | sed '1d' | cut -f1 -d' ' | sort > "$TMP_DIR/afs_users"

ldapsearch -LLL -b 'ou=users,dc=cri,dc=epita,dc=fr' -x uid uidNumber | awk -F': ' '$1 == "uid" {uid=$2; getline; print uid " " $2; getline} $1 == "uidNumber" {uidNumber=$2; getline; print $2 " " uidNumber; getline}' | sed 's/\./_/g' | sort > "$TMP_DIR/ldap_users_ids"
pts listentries | sed '1d' | sed 's/[[:blank:]]\+/ /g' | cut -f1,2 -d ' ' | sort > "$TMP_DIR/afs_users_ids"

vos listvldb | sed '1d' | sed '$d' | grep -v '^[[:blank:]]' | grep -v '^$' | sed 's/[[:blank:]]*$//' | sort > "$TMP_DIR/volumes"
 sed -n 's/^user_//p' "$TMP_DIR/volumes" | sort > "$TMP_DIR/volumes_ids"


users

echo

volumes

clean
