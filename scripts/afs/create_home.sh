#!/usr/bin/env bash

set -x

usage() {
  echo "$0 login"
  exit 1
}

[ $# -eq 1 ] || usage

id=$(id -u "$1")
group=$(id -g "$1")
login="$1"

echo "Creating $login AFS directory"

if vos examine "user_$id" > /dev/null 2>&1; then
  echo "Volume user_$id ($login) already exists"
  exit 1
else
  pts examine "$login" > /dev/null 2>&1 || pts createuser -name "$login" -id "$id"
fi


dest="$((id % 8))"
vos create -server "afs-$dest.pie.cri.epita.fr" -partition /vicepa -name "user_$id" -maxquota 2000000

# shellcheck disable=SC3000-SC4000
l1="${login:0:1}"
# shellcheck disable=SC3000-SC4000
l2="${login:0:2}"
[ -d "/afs/.cri.epita.fr/user/$l1" ] || (mkdir "/afs/.cri.epita.fr/user/$l1"; chown root:root "/afs/.cri.epita.fr/user/$l1")
[ -d "/afs/.cri.epita.fr/user/$l1/$l2" ] || (mkdir "/afs/.cri.epita.fr/user/$l1/$l2"; chown root:root "/afs/.cri.epita.fr/user/$l1/$l2")
fs checkv
fs mkmount -dir "/afs/.cri.epita.fr/user/$l1/$l2/$login" -vol "user_$id" -rw
fs checkv
fs setacl -dir  "/afs/.cri.epita.fr/user/$l1/$l2/$login" -acl "$login" all
mkdir "/afs/.cri.epita.fr/user/$l1/$l2/$login/u/"
cp -R /afs/cri.epita.fr/resources/confs "/afs/.cri.epita.fr/user/$l1/$l2/$login/u/.confs"
chown -R "$id:$group" "/afs/.cri.epita.fr/user/$l1/$l2/$login"
