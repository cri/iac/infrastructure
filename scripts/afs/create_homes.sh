#!/usr/bin/env bash

set -xeuo pipefail

usage() {
  echo "$0 csv_file login_column"
  exit 1
}

[ $# -eq 2 ] || usage

csv_file="$1"
login_column="$2"

tail -n+2 < "${csv_file}" | cut -d , -f "${login_column}" | xargs -n1 ./create_home.sh

vos release user
