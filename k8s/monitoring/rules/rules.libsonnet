{
  local k = import '1.24/main.libsonnet',
  local configMap = k.core.v1.configMap,
  local utils = import '../utils.libsonnet',

  rulesConfigMaps: std.foldr(
    function(mixinName, acc)
      local mixin = $.mixins[mixinName];
      if mixin.prometheusRules == {} then acc else acc + [
        {
          apiVersion: 'monitoring.coreos.com/v1',
          kind: 'PrometheusRule',
          metadata: {
            name: utils.toConfigMapName(group.name),
            labels: $._config.grafana_rule_labels,
          },
          spec: {
            groups: [group]
          },
        }
        for group in mixin.prometheusRules.groups
      ],
    std.objectFields($.mixins),
    [],
  ),
}
