{
  toConfigMapName(name)::
    local lower = std.asciiLower(name);
    local underscore = std.strReplace(lower, '_', '-');
    local space = std.strReplace(underscore, ' ', '-');
    local extJson = std.strReplace(space, '.json', '');
    local extYaml = std.strReplace(extJson, '.yaml', '');
    local extYml = std.strReplace(extYaml, '.yml', '');

    extYml,
}
