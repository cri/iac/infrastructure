{
  local thanos = self,
  query+:: {
    selector: 'job=~".*thanos-.*-query.*"',
  },
  queryFrontend+:: {
    selector: 'job=~".*thanos-.*-query-frontend.*"',
  },
  store+:: {
    selector: 'job=~".*thanos-.*-store.*"',
  },
  receive+:: {
    selector: 'job=~".*thanos-.*-receive.*"',
  },
  rule+:: {
    selector: 'job=~".*thanos-.*-rule.*"',
  },
  compact+:: {
    selector: 'job=~".*thanos-.*-compact.*"',
  },
  sidecar+:: {
    selector: 'job="rancher-monitoring-thanos-discovery",container="thanos-sidecar"',
  },
  bucketReplicate+:: {
    selector: 'job=~".*thanos-.*-bucket-replicate.*"',
  },
}
