local data = (import 'base.libsonnet');

local k = import '1.24/main.libsonnet';
local list = k.core.v1.list;

list.new(data.dashboardsConfigMaps + data.rulesConfigMaps + data.alertsConfigMaps)
