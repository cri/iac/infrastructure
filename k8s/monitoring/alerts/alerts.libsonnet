{
  local k = import '1.24/main.libsonnet',
  local configMap = k.core.v1.configMap,
  local utils = import '../utils.libsonnet',

  alertsConfigMaps: std.foldr(
    function(mixinName, acc)
      local mixin = $.mixins[mixinName];
      if mixin.prometheusAlerts == {} then acc else acc + [
        {
          apiVersion: 'monitoring.coreos.com/v1',
          kind: 'PrometheusRule',
          metadata: {
            name: utils.toConfigMapName(group.name),
            labels: $._config.grafana_rule_labels,
          },
          spec: {
            groups: [group]
          },
        }
        for group in mixin.prometheusAlerts.groups
      ],
    std.objectFields($.mixins),
    [],
  ),
}
