{
  mixins+:: {
    certs+: (import 'cert-manager-mixin/mixin.libsonnet'),
    thanos+: (import 'mixin/mixin.libsonnet') + (import './thanos-config.libsonnet'),

    argo+: (import 'extra/argo.libsonnet'),
    'gitlab-ci'+: (import 'extra/gitlab-ci.libsonnet'),
    infra+: (import 'extra/infra.libsonnet'),
    kafka+: (import 'extra/kafka.libsonnet'),
    maas+: (import 'extra/maas.libsonnet'),
    minio+: (import 'extra/minio.libsonnet'),
    nginx+: (import 'extra/nginx.libsonnet'),
    pie+: (import 'extra/pie.libsonnet'),
    postgresql+: (import 'extra/postgresql.libsonnet'),
    quarkus+: (import 'extra/quarkus.libsonnet'),
  },
}
