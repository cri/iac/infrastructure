local default = (import 'default.libsonnet');

{
  local k = import '1.24/main.libsonnet',
  local configMap = k.core.v1.configMap,
  local utils = import '../utils.libsonnet',

  // Config map names can't contain special chars, this is a hack but will
  // do for now.
  toDashboardName(mixinName, name)::
    'dashboard-%s-%s' % [mixinName, utils.toConfigMapName(name)],

  dashboardsConfigMaps: std.foldr(
    function(mixinName, acc)
      local mixin = $.mixins[mixinName] + default;
      if mixin.grafanaDashboards == {} then acc else acc + [
        configMap.new($.toDashboardName(mixinName, name),
                      {
                        [name]: std.toString(mixin.grafanaDashboards[name]),
                      }) +
        configMap.mixin.metadata.withLabels($._config.grafana_dashboard_labels) +
        configMap.mixin.metadata.withAnnotations({
          'k8s-sidecar-target-directory': '/tmp/dashboards/%s' % mixinName,
        })
        for name in std.objectFields(mixin.grafanaDashboards)
      ],
    std.objectFields($.mixins),
    [],
  ),
}
