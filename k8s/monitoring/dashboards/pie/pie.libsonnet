{
  grafanaDashboards+: {
    'pie-overview.json': import './pie-overview.json',
    'pie-node-exporter-overview.json': import './pie-node-exporter-overview.json',
    'pie-node-exporter-details.json': import './pie-node-exporter-details.json',
  },
}
