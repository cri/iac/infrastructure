{
  // We also use this to add a little "opinion":
  // - Dashboard UIDs are set to the md5 hash of their filename.
  // - Timezone are set to be "default" (ie local).
  // - Tooltip only show a single value.
  // - Interval is set to 1m (4*15s)
  // - Editable is set to false
  local grafanaDashboards = super.grafanaDashboards,

  grafanaDashboards:: {
    [filename]:
      local dashboard = grafanaDashboards[filename];
      dashboard {
        uid: std.md5(filename),
        timezone: '',
        editable: false,

        [if std.objectHas(dashboard, 'rows') then 'rows']: [
          row {
            panels: [
              panel {
                interval: '1m',
                tooltip+: {
                  shared: false,
                },
              }
              for panel in super.panels
            ],
          }
          for row in super.rows
        ],
      }
    for filename in std.objectFields(grafanaDashboards)
  },
}
