{
  grafanaDashboards+: {
    'infra-node-exporter-overview.json': import './infra-node-exporter-overview.json',
    'infra-node-exporter-details.json': import './infra-node-exporter-details.json',
  },
}
