{
  grafanaDashboards+: {
    'kafka-overview.json': import './kafka-overview.json',
    'kafka-details.json': import './kafka-details.json',
    'strimzi-operators.json': import './strimzi-operators.json',
    'zookeeper.json': import './zookeeper.json',
  },
}
