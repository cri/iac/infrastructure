{
  grafanaDashboards+: {
    'postgresql-overview.json': import './postgresql-overview.json',
    'postgresql-insights.json': import './postgresql-insights.json',
    'postgresql-database.json': import './postgresql-database.json',
  },
}
