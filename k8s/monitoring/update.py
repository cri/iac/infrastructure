#!/usr/bin/env python3

from urllib.request import urlretrieve

DASHBOARD_FOLDER = "dashboards"
DASHBOARDS = {
    "argo": [
        {
            "name": "argocd.json",
            "url": "https://raw.githubusercontent.com/argoproj/argo-cd/master/examples/dashboard.json",
        },
    ],
}

for app_name, dashboards in DASHBOARDS.items():
    folder = f"{DASHBOARD_FOLDER}/{app_name}"
    for dashboard in dashboards:
        urlretrieve(dashboard["url"], f"{folder}/{dashboard['name']}")
