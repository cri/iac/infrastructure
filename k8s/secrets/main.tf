terraform {
  required_providers {
    gpg = {
      source  = "Olivr/gpg"
      version = "0.2.1"
    }
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = "1.0.4"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.12.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.6.3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.5"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }

  backend "s3" {
    region = "default"
    bucket = "terraform"
    key    = "k8s-secrets.tfstate.tf"
    endpoints = {
      s3 = "https://s3.cri.epita.fr"
    }
    force_path_style            = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_s3_checksum            = true
  }
}

provider "htpasswd" {}
provider "random" {}
provider "vault" {}

provider "kubernetes" {
  alias          = "prod-1"
  config_path    = "~/.kube/config"
  config_context = "prod-1"
}
module "cluster_prod-1" {
  source = "./clusters/prod-1"
  providers = {
    kubernetes = kubernetes.prod-1
  }
}

provider "kubernetes" {
  alias          = "ops"
  config_path    = "~/.kube/config"
  config_context = "ops"
}
module "cluster_ops" {
  source = "./clusters/ops"
  providers = {
    kubernetes = kubernetes.ops
  }
}

provider "kubernetes" {
  alias          = "staging-1"
  config_path    = "~/.kube/config"
  config_context = "staging-1"
}
module "cluster_staging-1" {
  source = "./clusters/staging-1"
  providers = {
    kubernetes = kubernetes.staging-1
  }
}
