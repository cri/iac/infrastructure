data "vault_generic_secret" "ceph_s3_users_cri-k8s-ops-thanos" {
  path = "ceph/s3/users/cri-k8s-ops-thanos"
}

resource "vault_generic_secret" "monitoring_thanos-objectstorage" {
  path = "k8s-${local.cluster_name}/monitoring/thanos-objectstorage"
  data_json = jsonencode({
    "objstore.yml" = <<-CONFIG
      type: S3
      config:
        bucket: "cri-k8s-ops-thanos"
        endpoint: "s3.cri.epita.fr"
        access_key: "${data.vault_generic_secret.ceph_s3_users_cri-k8s-ops-thanos.data["access_key"]}"
        secret_key: "${data.vault_generic_secret.ceph_s3_users_cri-k8s-ops-thanos.data["secret_key"]}"
    CONFIG
  })
}

data "vault_generic_secret" "observees_monitoring_thanos-objectstorage" {
  for_each = toset(["prod-1", "undercloud", "assistants-prod-1"])
  path     = "k8s-${each.value}/monitoring/thanos-objectstorage"
}

resource "vault_generic_secret" "monitoring_thanos-objectstorage-observees" {
  for_each  = data.vault_generic_secret.observees_monitoring_thanos-objectstorage
  path      = "k8s-${local.cluster_name}/monitoring/thanos-objectstorage-${each.key}"
  data_json = jsonencode(each.value.data)
}
