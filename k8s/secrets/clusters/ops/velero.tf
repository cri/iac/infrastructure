data "vault_generic_secret" "restic_k8s-ops-velero_restic" {
  path = "restic/k8s-ops-velero/restic"
}

data "vault_generic_secret" "restic_k8s-ops-velero_velero" {
  path = "restic/k8s-ops-velero/velero"
}

resource "vault_generic_secret" "k8s-ops_velero_velero-s3-secret" {
  path = "k8s-ops/velero/velero-s3-secret"
  data_json = jsonencode({
    cloud = <<-EOF
      [default]
      aws_access_key_id = ${data.vault_generic_secret.restic_k8s-ops-velero_velero.data["accesskey"]}
      aws_secret_access_key = ${data.vault_generic_secret.restic_k8s-ops-velero_velero.data["secretkey"]}
    EOF
  })
}

resource "vault_generic_secret" "k8s-ops_velero_velero-restic-credentials" {
  path = "k8s-ops/velero/velero-restic-credentials"
  data_json = jsonencode({
    repository-password = data.vault_generic_secret.restic_k8s-ops-velero_restic.data["password"]
  })
}
