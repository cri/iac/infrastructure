resource "vault_generic_secret" "renovate_cri-account" {
  path         = "k8s-${local.cluster_name}/renovate/cri-account"
  disable_read = true
  data_json = jsonencode({
    username = "FIXME"
    password = "FIXME"
  })
}

resource "vault_generic_secret" "renovate_gitlab-token" {
  path         = "k8s-${local.cluster_name}/renovate/gitlab-token"
  disable_read = true
  data_json = jsonencode({
    token = "FIXME"
  })
}

resource "vault_generic_secret" "renovate_github-token" {
  path         = "k8s-${local.cluster_name}/renovate/github-token"
  disable_read = true
  data_json = jsonencode({
    token = "FIXME"
  })
}

resource "gpg_private_key" "renovate" {
  name     = "Renovate Bot"
  email    = "renovate@cri.epita.fr"
  rsa_bits = 4096
}
resource "vault_generic_secret" "renovate_gpg-key" {
  path = "k8s-${local.cluster_name}/renovate/gpg-key"
  data_json = jsonencode({
    private_key             = gpg_private_key.renovate.private_key
    private_key_single_line = replace(gpg_private_key.renovate.private_key, "\n", "\\n")
    public_key              = gpg_private_key.renovate.public_key
    fingerprint             = gpg_private_key.renovate.fingerprint
  })
}
