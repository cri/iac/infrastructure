data "vault_generic_secret" "ceph_s3_users_cri-k8s-ops-loki" {
  path = "ceph/s3/users/cri-k8s-ops-loki"
}

resource "vault_generic_secret" "logging_loki-s3" {
  path = "k8s-${local.cluster_name}/logging/loki-s3"
  data_json = jsonencode({
    AWS_ACCESS_KEY_ID     = data.vault_generic_secret.ceph_s3_users_cri-k8s-ops-loki.data.access_key
    AWS_SECRET_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_cri-k8s-ops-loki.data.secret_key
  })
}

resource "random_password" "logging_loki-auth-map" {
  for_each = toset(["k8s-ops", "k8s-prod-1", "k8s-undercloud", "k8s-assistants-prod-1", "undercloud", "overcloud"])
  length   = 64
  special  = false
}

resource "htpasswd_password" "logging_loki-auth-map" {
  for_each = random_password.logging_loki-auth-map
  password = each.value.result
}

resource "vault_generic_secret" "logging_loki-auth-map" {
  path      = "k8s-${local.cluster_name}/logging/loki-auth-map"
  data_json = jsonencode({ for k, v in htpasswd_password.logging_loki-auth-map : "${k}" => v.bcrypt })
}

resource "vault_generic_secret" "logging_loki-auth-map-password" {
  path      = "k8s-${local.cluster_name}/logging/loki-auth-map-password"
  data_json = jsonencode({ for k, v in htpasswd_password.logging_loki-auth-map : "${k}" => v.password })
}

resource "vault_generic_secret" "logging_loki-auth" {
  path = "k8s-${local.cluster_name}/logging/loki-auth"
  data_json = jsonencode({
    username = "k8s-${local.cluster_name}"
    password = vault_generic_secret.logging_loki-auth-map-password.data["k8s-${local.cluster_name}"]
  })
}
