resource "vault_policy" "staging-admin" {
  name = "k8s-staging-1-admin"

  policy = <<-EOP
    path "k8s-staging-1/*" {
      capabilities = ["create", "read", "update", "delete", "list"]
    }
  EOP
}
