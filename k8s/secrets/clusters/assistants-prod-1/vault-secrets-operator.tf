module "vault-secrets-operator" {
  source = "../../modules/vault-secrets-operator"

  cluster_name = local.cluster_name
}
