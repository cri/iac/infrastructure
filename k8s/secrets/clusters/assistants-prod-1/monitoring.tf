data "vault_generic_secret" "ceph_s3_users_assistants-k8s-prod-1-thanos" {
  path = "ceph/s3/users/assistants-k8s-prod-1-thanos"
}

resource "vault_generic_secret" "monitoring_thanos-objectstorage" {
  path = "k8s-${local.cluster_name}/monitoring/thanos-objectstorage"
  data_json = jsonencode({
    "objstore.yml" = <<-CONFIG
      type: S3
      config:
        bucket: "assistants-k8s-prod-1-thanos"
        endpoint: "s3.cri.epita.fr"
        access_key: "${data.vault_generic_secret.ceph_s3_users_assistants-k8s-prod-1-thanos.data["access_key"]}"
        secret_key: "${data.vault_generic_secret.ceph_s3_users_assistants-k8s-prod-1-thanos.data["secret_key"]}"
    CONFIG
  })
}
