resource "vault_generic_secret" "intranet_intranet-keytab" {
  path         = "k8s-${local.cluster_name}/intranet/intranet-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "intranet_ldap-keytab" {
  path         = "k8s-${local.cluster_name}/intranet/ldap-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "intranet_spnego-keytab" {
  path         = "k8s-${local.cluster_name}/intranet/spnego-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "intranet_algolia" {
  path         = "k8s-${local.cluster_name}/intranet/algolia"
  disable_read = true
  data_json = jsonencode({
    DJANGO_ALGOLIA_API_KEY        = "FIXME"
    DJANGO_ALGOLIA_APP_ID         = "FIXME"
    DJANGO_ALGOLIA_SEARCH_API_KEY = "FIXME"
  })
}
resource "random_password" "intranet_django-secrets" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_django-secrets" {
  path = "k8s-${local.cluster_name}/intranet/django-secrets"
  data_json = jsonencode({
    DJANGO_DEFAULT_ADMIN_PASSWORD = random_password.intranet_django-secrets[0].result
    DJANGO_SECRET_KEY             = random_password.intranet_django-secrets[1].result
  })
}
data "vault_generic_secret" "ceph_s3_users_intranet" {
  path = "ceph/s3/users/intranet"
}
resource "vault_generic_secret" "intranet_s3" {
  path = "k8s-${local.cluster_name}/intranet/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_intranet.data["access_key"]
    S3_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_intranet.data["secret_key"]
  })
}
resource "vault_generic_secret" "intranet_azuread" {
  path         = "k8s-${local.cluster_name}/intranet/azuread"
  disable_read = true
  data_json = jsonencode({
    AZUREAD_OAUTH2_KEY    = "FIXME"
    AZUREAD_OAUTH2_SECRET = "FIXME"
  })
}
resource "random_password" "intranet_kerberos" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_kerberos" {
  path = "k8s-${local.cluster_name}/intranet/kerberos"
  data_json = jsonencode({
    adm-service-password = random_password.intranet_kerberos[0].result
    kdc-service-password = random_password.intranet_kerberos[1].result
  })
}
resource "random_password" "intranet_kerberos-admin-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_kerberos-admin-password" {
  path = "k8s-${local.cluster_name}/intranet/kerberos-admin-password"
  data_json = jsonencode({
    KRB5_ADMIN_PASSWORD = random_password.intranet_kerberos-admin-password[0].result
  })
}
resource "random_password" "intranet_ldap-admin-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_ldap-admin-password" {
  path = "k8s-${local.cluster_name}/intranet/ldap-admin-password"
  data_json = jsonencode({
    LDAP_ADMIN_PASSWORD = random_password.intranet_ldap-admin-password[0].result
  })
}
resource "random_password" "intranet_ldap-config-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_ldap-config-password" {
  path = "k8s-${local.cluster_name}/intranet/ldap-config-password"
  data_json = jsonencode({
    LDAP_CONFIG_PASSWORD = random_password.intranet_ldap-config-password[0].result
  })
}
resource "random_password" "intranet_meilisearch-master-key" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "intranet_meilisearch-master-key" {
  path = "k8s-${local.cluster_name}/intranet/meilisearch-master-key"
  data_json = jsonencode({
    MEILI_MASTER_KEY = random_password.intranet_django-secrets[0].result
  })
}
resource "vault_generic_secret" "intranet_meilisearch-auth" {
  path         = "k8s-${local.cluster_name}/intranet/meilisearch-auth"
  disable_read = true
  data_json = jsonencode({
    MEILI_ADMIN_API_KEY  = "FIXME"
    MEILI_PUBLIC_API_KEY = "FIXME"
  })
}
