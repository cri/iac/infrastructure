resource "vault_generic_secret" "gitolite_keytab" {
  path         = "k8s-${local.cluster_name}/gitolite/keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}

resource "vault_generic_secret" "gitolite_ssh-keys" {
  path         = "k8s-${local.cluster_name}/gitolite/ssh-keys"
  disable_read = true
  data_json = jsonencode({
    ssh_host_dsa_key           = "FIXME with ssh-keygen -t dsa"
    "ssh_host_dsa_key.pub"     = ""
    ssh_host_ecdsa_key         = "FIXME with ssh-keygen -t ecdsa"
    "ssh_host_ecdsa_key.pub"   = ""
    ssh_host_ed25519_key       = "FIXME with ssh-keygen -t ed25519"
    "ssh_host_ed25519_key.pub" = ""
    ssh_host_rsa_key           = "FIXME with ssh-keygen -t rsa"
    "ssh_host_rsa_key.pub"     = ""
  })
}

resource "vault_generic_secret" "gitolite_ldap-bind-password" {
  path         = "k8s-${local.cluster_name}/gitolite/ldap-bind-password"
  disable_read = true
  data_json = jsonencode({
    password = "FIXME"
  })
}
