resource "vault_generic_secret" "git-server_intranet-api-token" {
  path         = "k8s-${local.cluster_name}/git-server/intra-api-token"
  disable_read = true
  data_json = jsonencode({
    X_API_TOKEN = "FIXME"
  })
}

resource "vault_generic_secret" "git-server_ssh-keys" {
  path         = "k8s-${local.cluster_name}/git-server/ssh-keys"
  disable_read = true
  data_json = jsonencode({
    ssh_host_dsa_key           = "FIXME with ssh-keygen -t dsa"
    "ssh_host_dsa_key.pub"     = ""
    ssh_host_ecdsa_key         = "FIXME with ssh-keygen -t ecdsa"
    "ssh_host_ecdsa_key.pub"   = ""
    ssh_host_ed25519_key       = "FIXME with ssh-keygen -t ed25519"
    "ssh_host_ed25519_key.pub" = ""
    ssh_host_rsa_key           = "FIXME with ssh-keygen -t rsa"
    "ssh_host_rsa_key.pub"     = ""
  })
}

resource "vault_generic_secret" "git-server_keytab" {
  path         = "k8s-${local.cluster_name}/git-server/keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}

resource "vault_generic_secret" "git-server_registry-pull-secret" {
  path         = "k8s-${local.cluster_name}/git-server/registry-pull-secret"
  disable_read = true
  data_json = jsonencode({
    ".dockerconfigjson" = "FIXME"
  })
}
