resource "random_password" "gitlab_redis" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "gitlab_redis" {
  path = "k8s-${local.cluster_name}/gitlab/redis"
  data_json = jsonencode({
    password = random_password.gitlab_redis[0].result
  })
}

data "vault_generic_secret" "ceph_s3_users_gitlab" {
  path = "ceph/s3/users/gitlab"
}
resource "vault_generic_secret" "gitlab_s3" {
  path = "k8s-${local.cluster_name}/gitlab/s3"
  data_json = jsonencode({
    AWS_ACCESS_KEY_ID     = data.vault_generic_secret.ceph_s3_users_gitlab.data["access_key"]
    AWS_SECRET_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_gitlab.data["secret_key"]
  })
}

resource "vault_generic_secret" "gitlab_oidc-cri" {
  path         = "k8s-${local.cluster_name}/gitlab/oidc-cri"
  disable_read = true
  data_json = jsonencode({
    client_id     = "FIXME"
    client_secret = "FIXME"
  })
}

resource "vault_generic_secret" "gitlab_keytabs" {
  path         = "k8s-${local.cluster_name}/gitlab/keytabs"
  disable_read = true
  data_json = jsonencode({
    http = "FIXME"
    host = "FIXME"
  })
}
