resource "vault_generic_secret" "cert-manager_openstack-cloud-credentials" {
  path         = "k8s-${local.cluster_name}/cert-manager/openstack-cloud-credentials"
  disable_read = true
  data_json = jsonencode({
    OS_AUTH_URL     = "https://openstack.cri.epita.fr:5000/v3/"
    OS_DOMAIN_NAME  = "CRI"
    OS_REGION_NAME  = "RegionOne"
    OS_PROJECT_NAME = "CRI"
    OS_USERNAME     = "FIXME"
    OS_PASSWORD     = "FIXME"
  })
}
