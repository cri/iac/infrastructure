resource "vault_generic_secret" "django-exam_django-exam-keytab" {
  path         = "k8s-${local.cluster_name}/django-exam/django-exam-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "vault_generic_secret" "django-exam_keytab" {
  path         = "k8s-${local.cluster_name}/django-exam/keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}
resource "random_password" "django-exam_django-secrets" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "django-exam_django-secrets" {
  path = "k8s-${local.cluster_name}/django-exam/django-secrets"
  data_json = jsonencode({
    DJANGO_SECRET_KEY = random_password.django-exam_django-secrets[0].result
  })
}
data "vault_generic_secret" "ceph_s3_users_django-exam" {
  path = "ceph/s3/users/cri-django-exam"
}
resource "vault_generic_secret" "django-exam_s3" {
  path = "k8s-${local.cluster_name}/django-exam/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_django-exam.data["access_key"]
    S3_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_django-exam.data["secret_key"]
  })
}

resource "vault_generic_secret" "django-exam_epita-openid" {
  path         = "k8s-${local.cluster_name}/django-exam/epita-openid"
  disable_read = true
  data_json = jsonencode({
    EPITA_OPENID_KEY    = "FIXME"
    EPITA_OPENID_SECRET = "FIXME"
  })
}

resource "vault_generic_secret" "django-exam_gitolite-keytab" {
  path         = "k8s-${local.cluster_name}/django-exam/gitolite-keytab"
  disable_read = true
  data_json = jsonencode({
    keytab = "FIXME"
  })
}

resource "vault_generic_secret" "django-exam_gitolite-ssh-keys" {
  path         = "k8s-${local.cluster_name}/django-exam/gitolite-ssh-keys"
  disable_read = true
  data_json = jsonencode({
    ssh_host_dsa_key           = "FIXME with ssh-keygen -t dsa"
    "ssh_host_dsa_key.pub"     = ""
    ssh_host_ecdsa_key         = "FIXME with ssh-keygen -t ecdsa"
    "ssh_host_ecdsa_key.pub"   = ""
    ssh_host_ed25519_key       = "FIXME with ssh-keygen -t ed25519"
    "ssh_host_ed25519_key.pub" = ""
    ssh_host_rsa_key           = "FIXME with ssh-keygen -t rsa"
    "ssh_host_rsa_key.pub"     = ""
  })
}

resource "vault_generic_secret" "django-exam_gitolite-ldap-bind-password" {
  path         = "k8s-${local.cluster_name}/django-exam/gitolite-ldap-bind-password"
  disable_read = true
  data_json = jsonencode({
    password = "FIXME"
  })
}
