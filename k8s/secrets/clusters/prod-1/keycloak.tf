resource "random_password" "keycloak_admin-password" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "keycloak_admin-password" {
  path = "k8s-${local.cluster_name}/keycloak/admin-password"
  data_json = jsonencode({
    password = random_password.keycloak_admin-password[0].result
  })
}
