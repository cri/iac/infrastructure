data "vault_generic_secret" "ceph_s3_users_cri-main-postgres" {
  path = "ceph/s3/users/cri-main-k8s-prod-1-postgres"
}
resource "vault_generic_secret" "cri-main-postgres_wal_s3" {
  path = "k8s-${local.cluster_name}/postgres/s3-wal-secrets"
  data_json = jsonencode({
    AWS_ACCESS_KEY_ID     = data.vault_generic_secret.ceph_s3_users_cri-main-postgres.data["access_key"]
    AWS_SECRET_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_cri-main-postgres.data["secret_key"]
  })
}

data "vault_generic_secret" "ceph_s3_users_forge-dev-postgres" {
  path = "ceph/s3/users/forge-dev-k8s-prod-1-postgres"
}
resource "vault_generic_secret" "forge-dev-postgres_wal_s3" {
  path = "k8s-${local.cluster_name}/postgres-forge-dev/s3-wal-secrets"
  data_json = jsonencode({
    AWS_ACCESS_KEY_ID     = data.vault_generic_secret.ceph_s3_users_forge-dev-postgres.data["access_key"]
    AWS_SECRET_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_forge-dev-postgres.data["secret_key"]
  })
}
