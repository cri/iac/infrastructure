data "vault_generic_secret" "ceph_s3_users_maas" {
  path = "ceph/s3/users/maas"
}
resource "vault_generic_secret" "maas_s3" {
  path = "k8s-${local.cluster_name}/maas/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_maas.data["access_key"]
    S3_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_maas.data["secret_key"]
  })
}

resource "vault_generic_secret" "maas_oidc-epita" {
  path         = "k8s-${local.cluster_name}/maas/oidc-epita"
  disable_read = true
  data_json = jsonencode({
    OIDC_ACCESS_KEY = "FIXME"
    OIDC_SECRET_KEY = "FIXME"
  })
}

resource "random_password" "maas_worker-register-token" {
  count   = 1
  length  = 64
  special = false
}

resource "vault_generic_secret" "maas_worker-register-token" {
  path         = "k8s-${local.cluster_name}/maas/worker-register-token"
  disable_read = true
  data_json = jsonencode({
    WORKER_REGISTER_TOKEN = random_password.maas_worker-register-token[0].result
  })
}
