resource "vault_generic_secret" "forge_intranet" {
  path         = "k8s-${local.cluster_name}/forge-intranet/oidc"
  disable_read = true
  data_json = jsonencode({
    OIDC_CLIENT_ID     = "FIXME"
    OIDC_CLIENT_SECRET = "FIXME"
  })
}

data "vault_generic_secret" "ceph_s3_users_forge_intranet" {
  path = "ceph/s3/users/forge-intranet"
}

resource "vault_generic_secret" "forge_intranet_s3" {
  path = "k8s-${local.cluster_name}/forge-intranet/s3"
  data_json = jsonencode({
    MINIO_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_forge_intranet.data["access_key"]
    MINIO_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_forge_intranet.data["secret_key"]
  })
}

resource "vault_generic_secret" "forge_intranet_maas" {
  path         = "k8s-${local.cluster_name}/forge-intranet/maas"
  disable_read = true
  data_json = jsonencode({
    MAAS_API_TOKEN = "FIXME"
  })
}

resource "vault_generic_secret" "forge_intranet_dockerconfig" {
  path         = "k8s-${local.cluster_name}/registry/laboratoires-lab-si"
  disable_read = true
  data_json = jsonencode({
    ".dockerconfigjson" = "FIXME"
    username            = "FIXME"
    password            = "FIXME"
    hostname            = "FIXME"
  })
}


resource "random_password" "forge_intranet_grafana" {
  count   = 2
  length  = 64
  special = false
}
resource "vault_generic_secret" "forge_intranet_grafana" {
  path         = "k8s-${local.cluster_name}/forge-intranet/grafana"
  disable_read = true
  data_json = jsonencode({
    ldap_bind_password  = random_password.forge_intranet_grafana[0].result
    admin_user_password = random_password.forge_intranet_grafana[1].result
    OIDC_CLIENT_ID      = "FIXME"
    OIDC_CLIENT_SECRET  = "FIXME"
  })
}
