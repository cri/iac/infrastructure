resource "vault_generic_secret" "inn_ldap-bind-password" {
  path         = "k8s-${local.cluster_name}/inn/ldap-bind-password"
  disable_read = true
  data_json = jsonencode({
    ldap-bind-password = "FIXME"
  })
}
