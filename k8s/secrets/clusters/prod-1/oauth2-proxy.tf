resource "random_password" "forge-dev-oauth2-proxy-session" {
  length           = 32
  override_special = "-_"
}

resource "vault_generic_secret" "forge-dev-oauth2-proxy-session" {
  path = "k8s-${local.cluster_name}/forge-dev/oauth2-proxy-session"
  data_json = jsonencode({
    COOKIE_SECRET = random_password.forge-dev-oauth2-proxy-session.result
  })
}

resource "vault_generic_secret" "forge-dev-oauth2-proxy-oidc" {
  path         = "k8s-${local.cluster_name}/forge-dev/oauth2-proxy-oidc"
  disable_read = true
  data_json = jsonencode({
    OPENID_CLIENT_ID     = "FIXME"
    OPENID_CLIENT_SECRET = "FIXME"
  })
}

resource "random_password" "forge-rt-oauth2-proxy-session" {
  length           = 32
  override_special = "-_"
}

resource "vault_generic_secret" "forge-rt-oauth2-proxy-session" {
  path = "k8s-${local.cluster_name}/forge-rt/oauth2-proxy-session"
  data_json = jsonencode({
    COOKIE_SECRET = random_password.forge-rt-oauth2-proxy-session.result
  })
}

resource "vault_generic_secret" "forge-rt-oauth2-proxy-oidc" {
  path         = "k8s-${local.cluster_name}/forge-rt/oauth2-proxy-oidc"
  disable_read = true
  data_json = jsonencode({
    OPENID_CLIENT_ID     = "FIXME"
    OPENID_CLIENT_SECRET = "FIXME"
  })
}

resource "random_password" "affectmajeures-oauth2-proxy-session" {
  length           = 32
  override_special = "-_"
}

resource "vault_generic_secret" "affectmajeures-oauth2-proxy-session" {
  path = "k8s-${local.cluster_name}/affectmajeures/oauth2-proxy-session"
  data_json = jsonencode({
    COOKIE_SECRET = random_password.affectmajeures-oauth2-proxy-session.result
  })
}

resource "vault_generic_secret" "affectmajeures-oauth2-proxy-oidc" {
  path         = "k8s-${local.cluster_name}/affectmajeures/oauth2-proxy-oidc"
  disable_read = true
  data_json = jsonencode({
    OPENID_CLIENT_ID     = "FIXME"
    OPENID_CLIENT_SECRET = "FIXME"
  })
}
