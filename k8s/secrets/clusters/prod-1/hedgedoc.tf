resource "random_password" "hedgedoc_session-secret" {
  length  = 64
  special = false
}
resource "vault_generic_secret" "hedgedoc_session-secret" {
  path = "k8s-${local.cluster_name}/hedgedoc/session-secret"
  data_json = jsonencode({
    SESSION_SECRET = random_password.hedgedoc_session-secret.result
  })
}

data "vault_generic_secret" "ceph_s3_users_cri-hedgedoc" {
  path = "ceph/s3/users/cri-hedgedoc"
}
resource "vault_generic_secret" "hedgedoc_s3" {
  path = "k8s-${local.cluster_name}/hedgedoc/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_cri-hedgedoc.data["access_key"]
    S3_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_cri-hedgedoc.data["secret_key"]
  })
}

resource "vault_generic_secret" "hedgedoc_epita-openid" {
  path         = "k8s-${local.cluster_name}/hedgedoc/epita-openid"
  disable_read = true
  data_json = jsonencode({
    EPITA_OPENID_KEY    = "FIXME"
    EPITA_OPENID_SECRET = "FIXME"
  })
}
