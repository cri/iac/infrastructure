resource "vault_generic_secret" "jetbrains-lic_stats-token" {
  path         = "k8s-${local.cluster_name}/jetbrains-lic/stats-token"
  disable_read = true
  data_json = jsonencode({
    JLS_STATS_TOKEN = "FIXME"
  })
}
