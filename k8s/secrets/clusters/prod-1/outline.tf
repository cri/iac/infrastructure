resource "random_bytes" "outline_random-secrets" {
  count = 2

  length = 32
}

resource "vault_generic_secret" "outline_session-secrets" {
  path = "k8s-${local.cluster_name}/outline/secrets"
  data_json = jsonencode({
    secret-key   = random_bytes.outline_random-secrets[0].hex
    utils-secret = random_bytes.outline_random-secrets[1].hex
  })
}

resource "vault_generic_secret" "outline_s3" {
  path         = "k8s-${local.cluster_name}/outline/s3"
  disable_read = true
  data_json = jsonencode({
    S3_ACCESS_KEY = "FIXME"
    S3_SECRET_KEY = "FIXME"
  })
}

resource "vault_generic_secret" "outline_oidc" {
  path         = "k8s-${local.cluster_name}/outline/oidc"
  disable_read = true
  data_json = jsonencode({
    client-id     = "FIXME"
    client-secret = "FIXME"
  })
}
