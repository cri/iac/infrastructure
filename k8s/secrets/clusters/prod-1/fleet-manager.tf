resource "random_password" "fleet-manager_django-secrets" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "fleet-manager_django-secrets" {
  path = "k8s-${local.cluster_name}/fleet-manager/django-secrets"
  data_json = jsonencode({
    DJANGO_SECRET_KEY = random_password.fleet-manager_django-secrets[0].result
  })
}

data "vault_generic_secret" "ceph_s3_users_cri-fleet-manager" {
  path = "ceph/s3/users/cri-fleet-manager"
}
resource "vault_generic_secret" "fleet-manager_s3" {
  path = "k8s-${local.cluster_name}/fleet-manager/s3"
  data_json = jsonencode({
    S3_ACCESS_KEY = data.vault_generic_secret.ceph_s3_users_cri-fleet-manager.data["access_key"]
    S3_SECRET_KEY = data.vault_generic_secret.ceph_s3_users_cri-fleet-manager.data["secret_key"]
  })
}

resource "vault_generic_secret" "fleet-manager_sentry" {
  path         = "k8s-${local.cluster_name}/fleet-manager/sentry"
  disable_read = true
  data_json = jsonencode({
    dsn = "FIXME"
  })
}

resource "vault_generic_secret" "fleet-manager_epita-openid" {
  path         = "k8s-${local.cluster_name}/fleet-manager/epita-openid"
  disable_read = true
  data_json = jsonencode({
    EPITA_OPENID_KEY    = "FIXME"
    EPITA_OPENID_SECRET = "FIXME"
  })
}

resource "random_password" "fleet-manager_kea-shared-secret" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "fleet-manager_kea-shared-secret" {
  path = "k8s-${local.cluster_name}/fleet-manager/kea-shared-secret"
  data_json = jsonencode({
    secret = random_password.fleet-manager_kea-shared-secret[0].result
  })
}

resource "random_password" "fleet-manager_salt-api-shared-secret" {
  count   = 1
  length  = 64
  special = false
}
resource "vault_generic_secret" "fleet-manager_salt-api-shared-secret" {
  path = "k8s-${local.cluster_name}/fleet-manager/salt-api-shared-secret"
  data_json = jsonencode({
    secret = random_password.fleet-manager_kea-shared-secret[0].result
  })
}

resource "tls_private_key" "fleet-manager_salt-pki-master" {
  algorithm = "RSA"
  rsa_bits  = 2048
}
resource "vault_generic_secret" "fleet-manager_salt-pki-master" {
  path = "k8s-${local.cluster_name}/fleet-manager/salt-pki-master"
  data_json = jsonencode({
    private_key = tls_private_key.fleet-manager_salt-pki-master.private_key_pem
    public_key  = tls_private_key.fleet-manager_salt-pki-master.public_key_pem
  })
}
