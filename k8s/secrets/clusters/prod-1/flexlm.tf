resource "vault_generic_secret" "flexlm_license" {
  path         = "k8s-${local.cluster_name}/flexlm/license"
  disable_read = true
  data_json = jsonencode({
    "license.dat" = "FIXME"
  })
}

resource "vault_generic_secret" "flexlm_dockerconfig" {
  path         = "k8s-${local.cluster_name}/flexlm/dockerconfig"
  disable_read = true
  data_json = jsonencode({
    ".dockerconfigjson" = "FIXME"
  })
}
