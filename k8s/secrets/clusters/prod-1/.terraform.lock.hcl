# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.12.1"
  constraints = "2.12.1"
  hashes = [
    "h1:qOng2HJskkJxsj9u1COM9l3lHDZHgeJVnciL2noiqGw=",
    "zh:22f5e1ca3b984088b734b9da3c1dfe6d8d03173da5a529266b55a40ba1d912d2",
    "zh:3395d1c6b252433d26556291d33b65676203910d85b0fb56f79e8dbd56b1b442",
    "zh:4d91f43c29e30b69d35f0f742de487cb8cff6f4c06a7f98c47f173c6beeea0f9",
    "zh:60db6f6036e20c5a2853da3eeb4b3eeaf4fc9601169b3656077f5e5feb26e301",
    "zh:7d119b10a8e05dcaed15b0439d0e89abecf53b696cb04d6192e17abe11c55bd7",
    "zh:944e72782ab055da6830abdf3abfcaf06810baae2cde5fc93384cd7e9bb0bb7d",
    "zh:9b399d49eecac95f52a92556230d749bc657663420561d7470b33b1440df7c0a",
    "zh:9f646eb4efa5c616aa12d7f3d4fdd922488a223846ed56a6c98dca1a149f9ddd",
    "zh:ccd0709c3b72586ea6358cc6ca671bf31e81faf75fb6e8826bc678c55edddfb0",
    "zh:dea3a23b923c191c6ea54624bd9f24b54e4ea4034e8265560e104fe1ff983c1a",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.3.2"
  constraints = "3.3.2"
  hashes = [
    "h1:3cVQP2pZt7WXSmZVYRj75U8PTP17Tbo+LubWAJJvt6s=",
    "zh:06940c8bc66b49e27c4a7030242df2211be2635bc061b656c9110b521f0d6f71",
    "zh:0aa9d7f1d7971b662485ca2474fc13ab2dba7951ad56f37d08ccf92c2e918bec",
    "zh:0e265e4154792c79865c27c55661f63c3df56e9ab961f47c4014f255e4aa3c33",
    "zh:2d8305ed9ddd1907b81a208170c7599ffb99eacf2639cf40b5c7fe384585ee87",
    "zh:43c7dd999908ead0e98a053c294b7d53546c45d6317e9124df39f6ed31e5fce8",
    "zh:5ddce4cb91ddda675071166d975cc5af2ccb5efabbf327e9e5d21f0b93c9ab6c",
    "zh:777ef4bba1f1875c4bcda9c2207bb00a24758a2a6c9097446c84d7cc20356673",
    "zh:81542311f3d1fac213c9c25d3650de2a0d54cc480ec9c5abc16d88f9802b82b0",
    "zh:9031150598307c66e61c13f7ca7b750ff8c4e373dc912f869d8ca81f9d1b4a2e",
    "zh:9c8caf2248dadd21480bd2705680d76e9939f2b5956b6863789bbe0ec5457892",
  ]
}

provider "registry.opentofu.org/hashicorp/tls" {
  version     = "4.0.5"
  constraints = "4.0.5"
  hashes = [
    "h1:zEH0OgSkeXDqNWzmOUWDczrUwyyujAHvnbW79qdxVMI=",
    "zh:05a7dc3ac92005485714f87541ad6d0d478988b478c5774227a7d39b01660050",
    "zh:547e0def44080456169bf77c21037aa6dc9e7f3e644a8f6a2c5fc3e6c15cf560",
    "zh:6842b03d050ae1a4f1aaed2a2b1ca707eae84ae45ae492e4bb57c3d48c26e1f1",
    "zh:6ced0a9eaaba12377f3a9b08df2fd9b83ae3cb357f859eb6aecf24852f718d9a",
    "zh:766bcdf71a7501da73d4805d05764dcb7c848619fa7c04b3b9bd514e5ce9e4aa",
    "zh:84cc8617ce0b9a3071472863f43152812e5e8544802653f636c866ef96f1ed34",
    "zh:b1939e0d44c89315173b78228c1cf8660a6924604e75ced7b89e45196ce4f45e",
    "zh:ced317916e13326766427790b1d8946c4151c4f3b0efd8f720a3bc24abe065fa",
    "zh:ec9ff3412cf84ba81ca88328b62c17842b803ef406ae19152c13860b356b259c",
    "zh:ff064f0071e98702e542e1ce00c0465b7cd186782fe9ccab8b8830cac0f10dd4",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:10HWtvEnr5Cp82lRiTMgKUIN+NofVMYvsNwC3kCFvgI=",
    "zh:5153281d676027ab940c19350b0e1e85e9f00733b5c5bafd6a393e108ac21f5b",
    "zh:614c40b1ef42d8914b2994cf9ed9af1843da946909dd3a06d9d22fe8aae209e6",
    "zh:627d6203ebea43a8175a2c0b4caee20bbe34ed98b71f7b3ffb27ca726ea54cde",
    "zh:b4f09f4aa0ca18f47390cf41b583e44dea35e6346f0d20cc78f6dc65c22d0243",
    "zh:c792a3ae198e36197481902940e893593b6a7f1b1da163ed8d1cc8332a4d7435",
    "zh:c85fb763e0cb706e019b2a36fcc9c7b7083f868f1ff76e8f9bc0acfa235813bf",
    "zh:d1b57d5970515326c3a4ae1e9ecdfc13b12ae3ce5b47444f90c3fa04f85d30a8",
    "zh:d85ccc64d08a7d654b212f90d4bd85b1a5671410148a3640c2ed603ce8fa481a",
    "zh:de0122ec4ffc070b70a618e9061c3cf2ac70c8f67be763c91a55898108b0181b",
    "zh:fa1860aea222a8c529fcece5b51974ec02650b808b592f7ba3fdc106e206cc31",
  ]
}
