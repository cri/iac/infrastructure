# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.12.1"
  constraints = "2.12.1"
  hashes = [
    "h1:qOng2HJskkJxsj9u1COM9l3lHDZHgeJVnciL2noiqGw=",
    "zh:22f5e1ca3b984088b734b9da3c1dfe6d8d03173da5a529266b55a40ba1d912d2",
    "zh:3395d1c6b252433d26556291d33b65676203910d85b0fb56f79e8dbd56b1b442",
    "zh:4d91f43c29e30b69d35f0f742de487cb8cff6f4c06a7f98c47f173c6beeea0f9",
    "zh:60db6f6036e20c5a2853da3eeb4b3eeaf4fc9601169b3656077f5e5feb26e301",
    "zh:7d119b10a8e05dcaed15b0439d0e89abecf53b696cb04d6192e17abe11c55bd7",
    "zh:944e72782ab055da6830abdf3abfcaf06810baae2cde5fc93384cd7e9bb0bb7d",
    "zh:9b399d49eecac95f52a92556230d749bc657663420561d7470b33b1440df7c0a",
    "zh:9f646eb4efa5c616aa12d7f3d4fdd922488a223846ed56a6c98dca1a149f9ddd",
    "zh:ccd0709c3b72586ea6358cc6ca671bf31e81faf75fb6e8826bc678c55edddfb0",
    "zh:dea3a23b923c191c6ea54624bd9f24b54e4ea4034e8265560e104fe1ff983c1a",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.6.3"
  constraints = "3.6.3"
  hashes = [
    "h1:Ry0Lr0zaoicslZlcUR4rAySPpl/a7QupfMfuAxhW3fw=",
    "zh:1bfd2e54b4eee8c761a40b6d99d45880b3a71abc18a9a7a5319204da9c8363b2",
    "zh:21a15ac74adb8ba499aab989a4248321b51946e5431219b56fc827e565776714",
    "zh:221acfac3f7a5bcd6cb49f79a1fca99da7679bde01017334bad1f951a12d85ba",
    "zh:3026fcdc0c1258e32ab519df878579160b1050b141d6f7883b39438244e08954",
    "zh:50d07a7066ea46873b289548000229556908c3be746059969ab0d694e053ee4c",
    "zh:54280cdac041f2c2986a585f62e102bc59ef412cad5f4ebf7387c2b3a357f6c0",
    "zh:632adf40f1f63b0c5707182853c10ae23124c00869ffff05f310aef2ed26fcf3",
    "zh:b8c2876cce9a38501d14880a47e59a5182ee98732ad7e576e9a9ce686a46d8f5",
    "zh:f27e6995e1e9fe3914a2654791fc8d67cdce44f17bf06e614ead7dfd2b13d3ae",
    "zh:f423f2b7e5c814799ad7580b5c8ae23359d8d342264902f821c357ff2b3c6d3d",
  ]
}

provider "registry.opentofu.org/hashicorp/tls" {
  version     = "4.0.5"
  constraints = "4.0.5"
  hashes = [
    "h1:zEH0OgSkeXDqNWzmOUWDczrUwyyujAHvnbW79qdxVMI=",
    "zh:05a7dc3ac92005485714f87541ad6d0d478988b478c5774227a7d39b01660050",
    "zh:547e0def44080456169bf77c21037aa6dc9e7f3e644a8f6a2c5fc3e6c15cf560",
    "zh:6842b03d050ae1a4f1aaed2a2b1ca707eae84ae45ae492e4bb57c3d48c26e1f1",
    "zh:6ced0a9eaaba12377f3a9b08df2fd9b83ae3cb357f859eb6aecf24852f718d9a",
    "zh:766bcdf71a7501da73d4805d05764dcb7c848619fa7c04b3b9bd514e5ce9e4aa",
    "zh:84cc8617ce0b9a3071472863f43152812e5e8544802653f636c866ef96f1ed34",
    "zh:b1939e0d44c89315173b78228c1cf8660a6924604e75ced7b89e45196ce4f45e",
    "zh:ced317916e13326766427790b1d8946c4151c4f3b0efd8f720a3bc24abe065fa",
    "zh:ec9ff3412cf84ba81ca88328b62c17842b803ef406ae19152c13860b356b259c",
    "zh:ff064f0071e98702e542e1ce00c0465b7cd186782fe9ccab8b8830cac0f10dd4",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:10HWtvEnr5Cp82lRiTMgKUIN+NofVMYvsNwC3kCFvgI=",
    "zh:5153281d676027ab940c19350b0e1e85e9f00733b5c5bafd6a393e108ac21f5b",
    "zh:614c40b1ef42d8914b2994cf9ed9af1843da946909dd3a06d9d22fe8aae209e6",
    "zh:627d6203ebea43a8175a2c0b4caee20bbe34ed98b71f7b3ffb27ca726ea54cde",
    "zh:b4f09f4aa0ca18f47390cf41b583e44dea35e6346f0d20cc78f6dc65c22d0243",
    "zh:c792a3ae198e36197481902940e893593b6a7f1b1da163ed8d1cc8332a4d7435",
    "zh:c85fb763e0cb706e019b2a36fcc9c7b7083f868f1ff76e8f9bc0acfa235813bf",
    "zh:d1b57d5970515326c3a4ae1e9ecdfc13b12ae3ce5b47444f90c3fa04f85d30a8",
    "zh:d85ccc64d08a7d654b212f90d4bd85b1a5671410148a3640c2ed603ce8fa481a",
    "zh:de0122ec4ffc070b70a618e9061c3cf2ac70c8f67be763c91a55898108b0181b",
    "zh:fa1860aea222a8c529fcece5b51974ec02650b808b592f7ba3fdc106e206cc31",
  ]
}

provider "registry.opentofu.org/loafoe/htpasswd" {
  version     = "1.0.4"
  constraints = "1.0.4"
  hashes = [
    "h1:v/EZlkxlFBRlLIK2rmgbksuhbxOwenP3TQvreUhCAtE=",
    "zh:1f17ffcb8ab2f19de1242a6980f78334fc81efeaddfa85545435048f54045e4a",
    "zh:6265fd9bbb718d55655120044b4969c80aa938ecfb17a0fd7541ff7de8c54e1e",
    "zh:79b7a6e3260b084530f6bdaba13536843fa55fc28569965a69cbdcb5d5c208a5",
    "zh:827991bd4481b9c0f33a922f5168146d0e68f627c8c71f1c18da27df05386502",
    "zh:898a54254123718828d07ca54fba9626f6c706e4849c1d5bfd93d16df4463a6f",
    "zh:b42f93565c8e5ab902d12a44dc34efa7207f5a568c7588f957732be3d9cd3997",
    "zh:d43a78148ae10aac214c7abef7c131d78b7173d28ab679354ac67c11ff979f8e",
    "zh:ddb702db1b27df028dab0364fbd90a1f5d97244e41765a7e66a8afc1a85d8371",
    "zh:df22dd80e4639c14ec428d345cdf21851e807890cfe72908759d037cfaed68b7",
    "zh:f6c7dfbc72ad83727c1fcfc064adb0362d947b66a2f5ba185742d5668c598c96",
    "zh:f7e1feafd63a1987f5e39f9c75ac05dc153ffde2c9dd669847c19ad318bbebe7",
  ]
}

provider "registry.opentofu.org/olivr/gpg" {
  version     = "0.2.1"
  constraints = "0.2.1"
  hashes = [
    "h1:+IxSHXkyhHgszKVym4qfp9rF8U0a1a2But15WdA1Xlg=",
    "zh:014bb7c1140dd2bb96b13a729d450e0aaa743b7f233884ab471a1e029853e66b",
    "zh:262fafa02f79402373b34c7a3bcb1b7b611a6e1fb6713b38480359d8582670e8",
    "zh:5202f998398f8b0d0a9cce7c1dd891ecce493b7aeb23d970ba5319cfbb024086",
    "zh:63609d1e453442453834bf7196e79d29d9340c76f172aec21c321aa13880011a",
    "zh:7fda5a60fe84c4e592087befdbba0b28f0b7f2218d81474720a79bfc587745e8",
    "zh:7fe14800b15f3a79efe9222047c34d0347c0a904356205c9a38abb5a668bc1cc",
    "zh:85f80b1a074b1e616c15c78d6d97c802ee0b5bd2bd878febc8f8430467de9819",
    "zh:a38ba53bd08827ef648f082efb216f65174ab2b18b5d6e14daf9b15ff8a9dfe8",
    "zh:a7c5c35cac6466d60feabb5e2e27a8d582f104152b7e5b87adb0071108b413a6",
    "zh:b6983ea1502698a74215fe2a52b3b81b48dc539daf5d9abe681ef9bcd9b67453",
    "zh:da2a64d029a496d20cfd23bc63a6943e8b80ceead96f5967a2e040eb5e1aeac4",
    "zh:e12b1fb462ab0df7cea32da5f3263d7c10305419dc86118e41687bb863737324",
    "zh:e15335ccd1589cccfa539f0b17b7736b57175ce0ac2e0aad87800b492ee066bf",
    "zh:ebc1e1032e6c996cb3295977196ee87f8babbf38c427bcbd2d8eca41d65f9ffd",
  ]
}
