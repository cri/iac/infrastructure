terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.12.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}
