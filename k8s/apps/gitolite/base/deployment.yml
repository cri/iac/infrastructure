# yamllint disable rule:line-length
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: gitolite
  labels:
    app: gitolite
    component: gitolite
spec:
  strategy:
    rollingUpdate:
      maxSurge: 75%
      maxUnavailable: 50%
  selector:
    matchLabels:
      app: gitolite
      component: gitolite
  template:
    metadata:
      annotations:
        backup.velero.io/backup-volumes: gitolite-data
      labels:
        app: gitolite
        component: gitolite
    spec:
      initContainers:
        - name: init-sssd-dir
          image: busybox
          command:
            - /bin/sh
            - -c
          args:
            - >-
              mkdir -p /var/lib/sss/pipes/private;
              chmod 700 /var/lib/sss/pipes/private;
          volumeMounts:
            - name: gitolite-sssd
              mountPath: /var/lib/sss/pipes
      containers:
        - name: gitolite
          image: registry.cri.epita.fr/cri/docker/gitolite/master
          envFrom:
            - configMapRef:
                name: gitolite-env
          ports:
            - name: ssh
              containerPort: 22
          readinessProbe:
            tcpSocket:
              port: ssh
            periodSeconds: 2
          lifecycle:
            preStop:
              exec:
                command:
                  - /bin/sh
                  - -c
                  - |-
                    for i in $(seq 20); do
                      if [ -z "$(ss -Hno state established '( dport = :ssh or sport = :ssh )')" ]; then
                        break;
                      else
                        sleep 3;
                        echo "This server is being stopped." > /etc/nologin;
                      fi
                    done
          volumeMounts:
            - name: gitolite-data
              mountPath: /var/lib/git
            - name: gitolite-ssh
              mountPath: /etc/ssh/keys
            - name: gitolite-keytab
              mountPath: /etc/openssh-keytab
            - name: gitolite-ldap-bind-password
              mountPath: /secrets/ldap-bind-password
            - name: gitolite-sssd
              mountPath: /var/lib/sss/pipes
              readOnly: true
          resources:
            requests:
              cpu: 100m
              memory: 1Gi
              ephemeral-storage: 500Mi
            limits:
              memory: 1Gi
        - name: sssd
          image: registry.cri.epita.fr/cri/docker/gitolite/master
          args: ["sssd"]
          envFrom:
            - configMapRef:
                name: gitolite-env
          volumeMounts:
            - name: gitolite-sssd
              mountPath: /var/lib/sss/pipes
            - name: gitolite-ldap-bind-password
              mountPath: /secrets/ldap-bind-password
          resources:
            requests:
              cpu: 20m
              memory: 100Mi
              ephemeral-storage: 500Mi
            limits:
              memory: 100Mi
      volumes:
        - name: gitolite-data
          persistentVolumeClaim:
            claimName: gitolite-data

        - name: gitolite-ssh
          secret:
            secretName: gitolite-ssh-key
            defaultMode: 0600
        - name: gitolite-keytab
          secret:
            secretName: gitolite-keytab
        - name: gitolite-ldap-bind-password
          secret:
            secretName: gitolite-ldap-bind-password

        - name: gitolite-sssd
          emptyDir: {}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: component
                      operator: In
                      values:
                        - gitolite
                topologyKey: kubernetes.io/hostname
