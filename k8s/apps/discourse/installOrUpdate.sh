#!/bin/bash
set -euxo pipefail


bundle exec rake maxminddb:get db:migrate assets:precompile s3:upload_assets

ASSET_PATH="./public/assets"
SERVICE_WORKER_FILE=$(find $ASSET_PATH -type f -name 'service-worker-*.js' -printf "%f\n");
SPROCKET_FILE=$(find $ASSET_PATH -type f -name '.sprockets-manifest-*' -printf "%f\n");

# shellcheck disable=SC2086
CONFIGMAP=$(
    jq -n --arg sprocket "$(cat $ASSET_PATH/$SPROCKET_FILE)" \
    --arg serviceWorker "$(cat $ASSET_PATH/$SERVICE_WORKER_FILE)" \
    '{
        "apiVersion": "v1",
        "kind": "ConfigMap",
        "metadata": {
            "name": "'$ASSET_NAME'", 
            "namespace": "'$NAMESPACE'",
        },
        "data": {
            ".sprockets-manifest-00000000000000000000000000000000.json": $sprocket, 
            "'$SERVICE_WORKER_FILE'": $serviceWorker,
        }
    }'
    )

curl --cacert "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt" \
     --header "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
     -X PUT -H 'Accept: application/json' -H 'Content-Type: application/json' \
     "https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT/api/v1/namespaces/${NAMESPACE}/configmaps/${ASSET_NAME}" \
     -d "$CONFIGMAP"
