---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: inn
  labels:
    app: inn
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: inn
  template:
    metadata:
      labels:
        app: inn
    spec:
      initContainers:
        - name: init
          args:
            - init
          image: registry.cri.epita.fr/cri/docker/inn
          envFrom:
            - configMapRef:
                name: inn-env
          volumeMounts:
            - name: news-run
              mountPath: /run/news/

            - name: inn-data
              mountPath: /var/lib/news/
              subPath: db/
            - name: inn-data
              mountPath: /var/spool/news
              subPath: spool/

            - name: inn-files
              mountPath: /etc/news/initial
              subPath: initial
            - name: inn-files
              mountPath: /etc/news/inn.conf
              subPath: inn.conf
            - name: inn-files
              mountPath: /etc/news/ldap_group_map.yml
              subPath: ldap_group_map.yml
            - name: inn-files
              mountPath: /etc/news/readers.conf
              subPath: readers.conf
          resources:
            requests:
              cpu: 10m
              memory: 200Mi
              ephemeral-storage: 500Mi
            limits:
              memory: 200Mi
      containers:
        - name: innd
          image: registry.cri.epita.fr/cri/docker/inn
          args:
            - innd
            - -C
          envFrom:
            - configMapRef:
                name: inn-env
          volumeMounts:
            - name: news-run
              mountPath: /run/news/

            - name: inn-data
              mountPath: /var/lib/news/
              subPath: db/
            - name: inn-data
              mountPath: /var/spool/news
              subPath: spool/

            - name: inn-files
              mountPath: /etc/news/initial
              subPath: initial
            - name: inn-files
              mountPath: /etc/news/inn.conf
              subPath: inn.conf
            - name: inn-files
              mountPath: /etc/news/ldap_group_map.yml
              subPath: ldap_group_map.yml
            - name: inn-files
              mountPath: /etc/news/readers.conf
              subPath: readers.conf

            - name: inn-tls
              mountPath: /etc/news/ca.pem
              subPath: tls.crt
            - name: inn-tls
              mountPath: /etc/news/cert.pem
              subPath: tls.crt
            - name: inn-tls
              mountPath: /secrets/key.pem
              subPath: tls.key

            - name: inn-ldap-bind-password
              mountPath: /secrets/ldap_bind_password
              subPath: ldap-bind-password
          ports:
            - name: nntp
              containerPort: 119
          readinessProbe:
            tcpSocket:
              port: nntp
            periodSeconds: 10
          lifecycle:
            preStop:
              exec:
                command:
                  - /bin/sh
                  - -c
                  - ctlinnd shutdown 'terminated'
          resources:
            requests:
              cpu: 10m
              memory: 200Mi
              ephemeral-storage: 500Mi
            limits:
              memory: 200Mi
        - name: nnrpd
          image: registry.cri.epita.fr/cri/docker/inn
          args:
            - nnrpd
            - -S -p 563
          envFrom:
            - configMapRef:
                name: inn-env
          volumeMounts:
            - name: news-run
              mountPath: /run/news/

            - name: inn-data
              mountPath: /var/lib/news/
              subPath: db/
            - name: inn-data
              mountPath: /var/spool/news
              subPath: spool/

            - name: inn-files
              mountPath: /etc/news/initial
              subPath: initial
            - name: inn-files
              mountPath: /etc/news/inn.conf
              subPath: inn.conf
            - name: inn-files
              mountPath: /etc/news/ldap_group_map.yaml
              subPath: ldap_group_map.yaml
            - name: inn-files
              mountPath: /etc/news/readers.conf
              subPath: readers.conf

            - name: inn-tls
              mountPath: /etc/news/ca.pem
              subPath: tls.crt
            - name: inn-tls
              mountPath: /etc/news/cert.pem
              subPath: tls.crt
            - name: inn-tls
              mountPath: /secrets/key.pem
              subPath: tls.key

            - name: inn-ldap-bind-password
              mountPath: /secrets/ldap_bind_password
              subPath: ldap-bind-password
          ports:
            - name: nntps
              containerPort: 563
          readinessProbe:
            tcpSocket:
              port: nntps
            periodSeconds: 10
          resources:
            requests:
              cpu: 1m
              memory: 200Mi
              ephemeral-storage: 500Mi
            limits:
              memory: 200Mi
      volumes:
        - name: news-run
          emptyDir: {}

        - name: inn-data
          persistentVolumeClaim:
            claimName: inn-data

        - name: inn-files
          configMap:
            name: inn-files
            items:
              - key: initial
                path: initial
              - key: inn.conf
                path: inn.conf
              - key: ldap_group_map.yaml
                path: ldap_group_map.yaml
              - key: readers.conf
                path: readers.conf

        - name: inn-tls
          secret:
            secretName: inn-tls
            defaultMode: 0444

        - name: inn-ldap-bind-password
          secret:
            secretName: inn-ldap-bind-password
            defaultMode: 0444
