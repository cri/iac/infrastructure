# Kolla Ansible deployment of EPITA's OpenStack

## Preparing your environment

### Dependencies

If you are not using the `kolla` devShell provided by the Nix Flake, you have to
run `poetry install`. You then have to prefix all your kolla-ansible commands
with `poetry run`.

Install the Ansible requiremnts of Kolla Ansible:

```sh
kolla-ansible install-deps
```

### Passwords

If your `passwords.yml` file does not exists, `cp passwords.yml{.sample,}`

You then need to populate your `passwords.yml` file from Vault:

```sh
kolla-readpwd \
    --passwords passwords.yml \
    --vault-addr "${VAULT_ADDR}" \
    --vault-mount-point kolla \
    --vault-kv-path "" \
    --vault-token "$(cat ~/.vault-token)"
```

To upload your `passwords.yml` file to Vault, use `kolla-writepwd` with the same
arguments as above. BE VERY CAREFUL WITH THIS COMMAND.

## Running Kolla Ansible

```sh
ANSIBLE_CONFIG=./ansible.cfg kolla-ansible \
    -i hosts \
    --configdir "$(pwd)" \
    --passwords "$(pwd)/passwords.yml" \
    YOUR_ACTION_HERE \
    -e "node_custom_config=$(pwd)/"
```

`YOUR_ACTION_HERE` must be replaced by one of kolla-ansible's action. See
`kolla-ansible --help` or kolla's documentation for more information.

## Testing

See `./testing/README.md`.
