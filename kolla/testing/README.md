# Testing kolla

```sh
vagrant up
vagrant ssh-config > ssh-config
ANSIBLE_CONFIG=./ansible.cfg poetry run kolla-ansible \
    -i hosts \
    --configdir "$(pwd)" \
    --passwords "$(pwd)/passwords.yml" \
    YOUR_ACTION_HERE
```
