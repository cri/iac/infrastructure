# syntax = docker/dockerfile:experimental
#checkov:skip=CKV_DOCKER_2:We don't need healthchecks
#checkov:skip=CKV_DOCKER_3:We don't need a special user

FROM registry.cri.epita.fr/cri/docker/mirror/python:3.10-slim as base

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_CREATE=false \
    PATH="/opt/venv/bin:$PATH"

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            curl ca-certificates gnupg apt-transport-https git software-properties-common

# Jsonnet
RUN apt-get update && \
    apt-get install -y --no-install-recommends jsonnet

# Kubectl
RUN curl -1sLf \
        -o /usr/share/keyrings/kubernetes-archive-keyring.gpg \
        https://packages.cloud.google.com/apt/doc/apt-key.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends kubectl

# Kubeval
RUN curl -1sLf https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz -O && \
    tar xf kubeval-linux-amd64.tar.gz && rm kubeval-linux-amd64.tar.gz && \
    mv kubeval /usr/local/bin/kubeval

# Kustomize
RUN curl -1sLf https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh | bash && \
    mv kustomize /usr/local/bin/kustomize

# Reviewdog
RUN curl -sfL https://raw.githubusercontent.com/reviewdog/nightly/master/install.sh | sh -s -- -b /usr/local/bin/

# Shellcheck
RUN apt-get install -y --no-install-recommends shellcheck

# Terraform
RUN curl -1sLf https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com bullseye main" && \
    apt-get update && \
    apt-get install -y --no-install-recommends terraform

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python-dev git

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pip \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install --upgrade poetry && \
    # --dev to get development dependencies
    # ignore kolla-ansible
    # --without-hashes as it fails with checkov
    # see https://github.com/python-poetry/poetry/issues/3472
    poetry export --dev --without-hashes --format requirements.txt | sed '/^kolla-ansible/d' > ./requirements.txt && \
    pip3 install --requirement ./requirements.txt

FROM base

COPY --from=builder /opt/venv/ /opt/venv/
