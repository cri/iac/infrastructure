# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/random" {
  version = "3.6.3"
  hashes = [
    "h1:Ry0Lr0zaoicslZlcUR4rAySPpl/a7QupfMfuAxhW3fw=",
    "zh:1bfd2e54b4eee8c761a40b6d99d45880b3a71abc18a9a7a5319204da9c8363b2",
    "zh:21a15ac74adb8ba499aab989a4248321b51946e5431219b56fc827e565776714",
    "zh:221acfac3f7a5bcd6cb49f79a1fca99da7679bde01017334bad1f951a12d85ba",
    "zh:3026fcdc0c1258e32ab519df878579160b1050b141d6f7883b39438244e08954",
    "zh:50d07a7066ea46873b289548000229556908c3be746059969ab0d694e053ee4c",
    "zh:54280cdac041f2c2986a585f62e102bc59ef412cad5f4ebf7387c2b3a357f6c0",
    "zh:632adf40f1f63b0c5707182853c10ae23124c00869ffff05f310aef2ed26fcf3",
    "zh:b8c2876cce9a38501d14880a47e59a5182ee98732ad7e576e9a9ce686a46d8f5",
    "zh:f27e6995e1e9fe3914a2654791fc8d67cdce44f17bf06e614ead7dfd2b13d3ae",
    "zh:f423f2b7e5c814799ad7580b5c8ae23359d8d342264902f821c357ff2b3c6d3d",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:10HWtvEnr5Cp82lRiTMgKUIN+NofVMYvsNwC3kCFvgI=",
    "zh:5153281d676027ab940c19350b0e1e85e9f00733b5c5bafd6a393e108ac21f5b",
    "zh:614c40b1ef42d8914b2994cf9ed9af1843da946909dd3a06d9d22fe8aae209e6",
    "zh:627d6203ebea43a8175a2c0b4caee20bbe34ed98b71f7b3ffb27ca726ea54cde",
    "zh:b4f09f4aa0ca18f47390cf41b583e44dea35e6346f0d20cc78f6dc65c22d0243",
    "zh:c792a3ae198e36197481902940e893593b6a7f1b1da163ed8d1cc8332a4d7435",
    "zh:c85fb763e0cb706e019b2a36fcc9c7b7083f868f1ff76e8f9bc0acfa235813bf",
    "zh:d1b57d5970515326c3a4ae1e9ecdfc13b12ae3ce5b47444f90c3fa04f85d30a8",
    "zh:d85ccc64d08a7d654b212f90d4bd85b1a5671410148a3640c2ed603ce8fa481a",
    "zh:de0122ec4ffc070b70a618e9061c3cf2ac70c8f67be763c91a55898108b0181b",
    "zh:fa1860aea222a8c529fcece5b51974ec02650b808b592f7ba3fdc106e206cc31",
  ]
}

provider "registry.opentofu.org/rancher/rancher2" {
  version     = "1.24.1"
  constraints = "1.24.1"
  hashes = [
    "h1:OfKUpb1hChdO+CluyNKjmrQimFJAJAA+s96Pp6vK6FQ=",
    "zh:019607a142db3cb24c40837dc8a65e93a86b56bf4f8f8ebedf6433a8108b8a31",
    "zh:0bad17fb2bdc730102a056b3ca63fb9d890d2f85b47316620fc4f750fdbef078",
    "zh:46e73b838be94c24a46dba08461a7b955b03d32f050b7ba819e8f2bb5af1fe5f",
    "zh:574217a569ae4de201d43c95e09cdf5e22ea259c45b38a3b9d6247eac1ad3abf",
    "zh:591488b3bd1016ef9ce51b03fe08c4a2954d16be15157a4c075e833dc1b6e248",
    "zh:6657904ec02e17518bc712147fa618d84a1a9b3b3a275c3e112072f2dec12a16",
    "zh:6caf485186f466a485c58b36216f8cc39e3d4f08945b8ea9835158cfb0a73398",
    "zh:6e1cacae4ca25e2ad442a1ca12180a8226314c46c4d1c23871ff54e484392048",
    "zh:7b5dc8f2268dab0ad8ee38513462835b1269f5848fdd96d8e8f20a4afeb5509b",
    "zh:9ed3c38771c6a7232c378553fbb2b2d66dece982f8898fc91e2d6c0ddac00a88",
    "zh:bb06e06be0de781b134f565986be04e8581cdd68c58af520c3304c84b29c2f35",
    "zh:fb7f8d24b8b1681e50d3ee4baf7080ff1bbf8c30891ef27afcb5d95fb610d9fe",
  ]
}

provider "registry.opentofu.org/terraform-provider-openstack/openstack" {
  version     = "1.48.0"
  constraints = ">= 1.43.0, 1.48.0"
  hashes = [
    "h1:qjf/qyH9oKOMujQk59bNxV8yLRbUhmihxMRrKOeA8qI=",
    "zh:1fe237fa1153e05879fd26857416a1d029a3f108e32e83c4931dd874c777aa6a",
    "zh:2c4587b4c810d569aafd69e287ecc2ee910e6c16cfc784e49861e0a8066b8655",
    "zh:3f1a42fce3c925afeeaa96efae0bc9be95acfc80ba147a8123d03038d429df6b",
    "zh:430511b62dc2fdafa070e9bd88e5e1fc39b3d667151aa9bf8e21b2c2c5421281",
    "zh:4452279f6f23d3f2c5969deebf24ae2c38af8e02d52ee589b658c52b321835e5",
    "zh:5525d1ca817f28ec9f0f648ea38b94fd0741130eaed2260bbd734efd03aecfb8",
    "zh:675001e8cec8d0d4f006ce01b0608b7c5a378b4e56c6a27fbf5562f04371de70",
    "zh:6c0f4da6da81da562e16af6fbb36035c0797de2a0384d0ef7c9a8b4676f8eca9",
    "zh:79db708664ecbcf9d1a6d20e6a294716bff21a2641a8f58bfce60f3d11b944ef",
    "zh:7bfc5ee6765694779fbfc00954fe04795035e85dfefd916dc6601717116b7005",
    "zh:899a17c1547aa1bf732a55c903f3df25c8a0c107c16e0753677aecb8ed32130c",
    "zh:9e02fb5267dc415a763ef55a24f3890f7e63de8d61e05e220d90a5a4a4b891ed",
    "zh:a224e6e677e92cd31d0806a2d11c9bb17d032eaa0086e2aa8136ae0e9ce2fa83",
    "zh:b3905869f6fea27ffd144eb8221ea67aeca63e23c06af43a221e55634faef3e2",
  ]
}
