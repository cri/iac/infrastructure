module "dns_synergie_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "synergie.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {}
}
