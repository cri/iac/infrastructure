module "dns_scia_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "scia.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["163.5.226.20"] }
    }
    "*" = {
      CNAME = { records = ["@"] }
    }
  }
}
