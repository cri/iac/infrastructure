module "dns_gistre_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "gistre.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["163.5.226.70"] }
    }
    "blog" = {
      CNAME = { records = ["pages.epita.fr."] }
    }
    "*" = {
      CNAME = { records = ["@"] }
    }
  }
}
