module "dns_infoprepa_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "infoprepa.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["91.243.117.184"] }
    }
    "algo" = {
      CNAME = { records = ["prepa.pages.epita.fr."] }
    }
    "algo-td" = {
      CNAME = { records = ["prepa.pages.epita.fr."] }
    }
    "wiki-prog" = {
      CNAME = { records = ["prepa.pages.epita.fr."] }
    }
  }
}
