module "dns_forge_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "forge.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      MX  = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = ["\"v=spf1 redirect=cri.epita.fr\"", "google-site-verification=j8l5iyKrfGnT2Ov2QDgiMd0U7UA8K2E7mr1ajU-urfE"] }
    }
    "cri._domainkey" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
    git = {
      A = { records = ["91.243.117.189"] }
    }
    "*.api" = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    discord = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    discourse = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    intra = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    operator = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    storybook = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    syllabus = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    forms = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    league = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    docs = {
      CNAME = { records = ["forge.pages.epita.fr."] }
    }
    formation = {
      CNAME = { records = ["forge.pages.epita.fr."] }
    }
    pie = {
      NS = { records = ["designate.openstack.cri.epita.fr."] }
    }
    calendar = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    l = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    rtc = {
      CNAME = { records = ["forge.pages.epita.fr."] }
    }
    tickets-next = {
      A  = { records = ["91.243.117.180"] }
      MX = { records = ["10 mail.cri.epita.fr."] }
    }
    welcome = {
      CNAME = { records = ["cri.pages.epita.fr."] }
    }
    exam = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    "git.exam" = {
      A = { records = ["91.243.117.188"] }
    }
    zeus = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    "admin.zeus" = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    intracom = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    rdt = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    "admin.rdt" = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    absences = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    sso = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    affect-majeures = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    wiki = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
  }
}

module "dns_pie_forge_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "pie.forge.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    dhcp = {
      A = { records = ["10.201.5.67"] }
    }
    tftp = {
      A = { records = ["10.201.5.67"] }
    }
    repo-sm-inventory = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
  }
}
