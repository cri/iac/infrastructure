module "dns_prepa_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "prepa.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "afit" = {
      CNAME = { records = ["prepa.pages.epita.fr."] }
    }
    "ml" = {
      MX  = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = ["\"v=spf1 redirect=cri.epita.fr\""] }
    }
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.ml" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
  }
}
