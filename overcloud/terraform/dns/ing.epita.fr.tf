module "dns_ing_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "ing.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc.ml" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "pfee" = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    ml = {
      MX  = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
  }
}
