module "dns_pie_cri_epita_net" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  project_id = data.openstack_identity_project_v3._3IE.id
  zone_name  = "pie.cri.epita.net."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["10.201.5.2"] }
    }
    gate = {
      A = { records = ["10.201.5.2"] }
    }
    jetbrains-lic = {
      CNAME = { records = ["jetbrains-lic.pie.cri.epita.fr."] }
    }
    kms = {
      A = { records = ["163.5.11.114"] }
    }
    ntp = {
      CNAME = { records = ["gate"] }
    }
    reverse = {
      A = { records = ["10.224.4.2"] }
    }
    static = {
      CNAME = { records = ["reverse"] }
    }
    tickets = {
      CNAME = { records = ["reverse"] }
    }
    torrent = {
      CNAME = { records = ["torrent.pie.cri.epita.fr."] }
    }
  }
}
