module "dns_lab_cri_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "lab.cri.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    gate = {
      A = { records = ["192.168.240.254"] }
    }
    restic-prune-1 = {
      A = { records = ["192.168.240.8"] }
    }
    nas = {
      A = { records = ["192.168.240.9"] }
    }
    nas-1 = {
      A = { records = ["192.168.240.10"] }
    }
    leo = {
      A = { records = ["192.168.240.42"] }
    }
    "leo.underdesk" = {
      A = { records = ["192.168.240.62"] }
    }
    nico = {
      A = { records = ["192.168.240.26"] }
    }
    printer = {
      A = { records = ["192.168.240.12"] }
    }
    sw = {
      A = { records = ["192.168.240.240"] }
    }
    sw-roots = {
      A = { records = ["192.168.240.241"] }
    }
    tel-1 = {
      A = { records = ["192.168.240.51"] }
    }
    tel-2 = {
      A = { records = ["192.168.240.52"] }
    }

    "_vlmcs._tcp" = {
      SRV = { records = ["0 100 1688 kms.pie.cri.epita.fr."] }
    }
    "_jetbrains-license-server" = {
      TXT = { records = ["url=https://jetbrains-lic.pie.cri.epita.fr"] }
    }
  }
}
