module "dns_cyber_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "cyber.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
  }
}
