module "dns_ssie_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "ssie.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "blog" = {
      CNAME = { records = ["pages.epita.fr."] }
    }
  }
}
