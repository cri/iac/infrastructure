module "dns_dfp_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "dfp.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      MX = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = [
        "\"v=spf1 redirect=cri.epita.fr\"",
        "google-site-verification=mNuOzlDRyJ_FCI-OFjgke2-MFBUkXjsuqsxhQiPLEHA"
      ] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
  }
}
