module "dns_assos_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "assos.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["91.243.117.196"] }
    }
  }
}
