module "dns_mti_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "mti.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A   = { records = ["163.5.224.114"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 mx -all\""] }
    }
    "*" = {
      CNAME = { records = ["@"] }
    }
    lists = {
      A   = { records = ["163.5.224.114"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 mx -all\""] }
    }
    mail = {
      A = { records = ["163.5.224.114"] }
    }
    mti-gate1 = {
      A = { records = ["163.5.224.114"] }
    }
    "*.mti-gate1" = {
      CNAME = { records = ["mti-gate1"] }
    }
    mti-gate2 = {
      A = { records = ["163.5.224.112"] }
    }
    "*.mti-gate2" = {
      CNAME = { records = ["mti-gate2"] }
    }
  }
}
