module "dns_acdc_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "acdc.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      MX  = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = ["\"v=spf1 redirect=cri.epita.fr\""] }
    }
    "cri._domainkey" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    cats = {
      CNAME = { records = ["cats.acdc.risson.space."] }
    }
  }
}
