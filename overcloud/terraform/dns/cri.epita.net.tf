data "openstack_identity_project_v3" "_3IE" {
  name = "3IE"
}
module "dns_cri_epita_net" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  project_id = data.openstack_identity_project_v3._3IE.id
  zone_name  = "cri.epita.net."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A   = { records = ["91.243.117.196"] }
      MX  = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    "cri._domainkey" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "_dmarc" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.ml" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }

    pie = {
      NS = { records = ["designate.openstack.cri.epita.fr."] }
    }

    doc = {
      CNAME = { records = ["reverse.cri.epita.fr."] }
    }
    mail = {
      CNAME = { records = ["mail.cri.epita.fr."] }
    }
    "_acme-challenge.mail" = {
      CNAME = { records = ["_acme-challenge.mail.undercloud.cri.epita.fr."] }
    }
    ml = {
      CNAME = { records = ["ml.cri.epita.fr."] }
    }
    news = {
      CNAME = { records = ["news.cri.epita.fr."] }
    }
    nntp = {
      CNAME = { records = ["news.cri.epita.fr."] }
    }
    photos = {
      CNAME = { records = ["photos.cri.epita.fr."] }
    }
    sand = {
      CNAME = { records = ["sand.cri.epita.fr."] }
    }
    static = {
      CNAME = { records = ["static.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
  }
}
