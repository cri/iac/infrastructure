module "dns_assistants_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "assistants.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A  = { records = ["91.243.117.184"] } # ing.pages.epita.fr
      MX = { records = ["10 mail.cri.epita.fr."] }
      TXT = { records = [
        "\"v=spf1 redirect=cri.epita.fr\""
      ] }
    }
    "cri._domainkey" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey.ml.cri.epita.fr."] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.tickets.cri.epita.fr."] }
    }
    "_dmarc" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.ml" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    "admin-svc.iaas" = {
      A = { records = ["91.243.117.150"] }
    }
    assignments = {
      CNAME = { records = ["ingress.assistants.ing.iaas.epita.fr."] }
    }
    "bastion.iaas" = {
      CNAME = { records = ["admin-svc.iaas.assistants.epita.fr."] }
    }
    scrooge = {
      CNAME = { records = ["ingress.assistants.ing.iaas.epita.fr."] }
    }
    doc = {
      CNAME = { records = ["ing.pages.epita.fr."] }
    }
    ml = {
      CNAME = { records = ["ml.cri.epita.fr."] }
    }
    static = {
      CNAME = { records = ["ing.pages.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
    trove = {
      CNAME = { records = ["ing.pages.epita.fr."] }
    }
    creeps = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    "game.creeps" = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    "git-workshop" = {
      A = { records = ["91.243.117.157"] }
    }
    eplace = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    tty = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    t4t = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    nerds = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    libzork = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
  }
}
