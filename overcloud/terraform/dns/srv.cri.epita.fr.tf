module "dns_srv_cri_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "srv.cri.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    kvm-1 = {
      A = { records = ["192.168.204.65"] }
    }
    kvm-2 = {
      A = { records = ["192.168.204.14"] }
    }
    mail = {
      CNAME = { records = ["mail-2"] }
    }
    "_acme-challenge.mail" = {
      CNAME = { records = ["_acme-challenge.mail.undercloud.cri.epita.fr."] }
    }
    mail-2 = {
      A = { records = ["192.168.204.78"] }
    }
    ml-1 = {
      A = { records = ["192.168.204.157"] }
    }
    moodle-exam-1 = {
      A = { records = ["192.168.204.80"] }
    }
    reverse-1 = {
      A = { records = ["192.168.204.84"] }
    }
    reverse-pie-1 = {
      A = { records = ["192.168.204.67"] }
    }
  }
}
