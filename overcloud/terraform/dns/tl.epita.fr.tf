module "dns_tl_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "tl.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      MX = { records = [
        "1 aspmx.l.google.com.",
        "5 alt1.aspmx.l.google.com.",
        "5 alt2.aspmx.l.google.com.",
        "10 alt3.aspmx.l.google.com.",
        "10 alt4.aspmx.l.google.com."
      ] }
      TXT = { records = [
        "\"v=spf1 include:_spf.google.com include:mail.cri.epita.fr -all\"",
        "google-site-verification=_H5BoQD6QH2_inQLeNnOD4slUxxKA9EMh8pAKf7e82U"
      ] }
    }
    "google._domainkey" = {
      TXT = {
        records = ["\"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiQo/dz6HxU3lOJ99os3NRVbJ22GNCOnSIZ6U1rJkhaYqprFFBsiOKRaMw+SvSE1Y1XAbA5eFw9H6WOZhP9QgvF5FcD31Jw0p1H3n2qi2ocfrkoUXLHNm7wdqKc2lp8MBcbG3wAQRLA13WvVIcRuo2jDmk+zg0+7oTxOPqcp4ZPoGsApHGnCXbffVPgxp+fU\" \"2ppdcjSj8xz53TxSyvoBWpUdmy5OEvsxANU2pjwj3eGn4jtwyVIJyigUzrkq4wacdxCa26aR0dOcbM3D2oj5mgmuqkVq0rftMsesftXpr9+Pdv6DLMKN0TTPBISJGvPuNU+IfeSSqw3R0OBz0wmQNGwIDAQAB\""]
      }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey.cri.epita.fr."] }
    }
    tickets = {
      CNAME = { records = ["tickets.cri.epita.fr."] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc.cri.epita.fr."] }
    }
    drive = {
      CNAME = { records = ["ghs.googlehosted.com."] }
    }
  }
}
