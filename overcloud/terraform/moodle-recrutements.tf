# Volumes

resource "openstack_blockstorage_volume_v3" "moodle-recrut_rootfs" {
  name     = "moodle-recrut"
  size     = 50
  image_id = data.openstack_images_image_v2.ubuntu-20-04-20220815.id
}

resource "openstack_blockstorage_volume_v3" "moodle-recrut_data" {
  name = "moodle-recrut_data"
  size = 20
}

resource "openstack_blockstorage_volume_v3" "moodle-recrut_db" {
  name = "moodle-recrut_db"
  size = 100
}

# Instances

resource "openstack_compute_instance_v2" "moodle-recrut" {
  name      = "moodle-recrut"
  flavor_id = data.openstack_compute_flavor_v2.m2-xlarge.id
  key_pair  = openstack_compute_keypair_v2.dummy.name
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-http-https.security_group_name,
  ]

  block_device {
    uuid             = openstack_blockstorage_volume_v3.moodle-recrut_rootfs.id
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = 0
  }

  network {
    uuid = module.CRI_moodle-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "moodle-recrut_data" {
  instance_id = openstack_compute_instance_v2.moodle-recrut.id
  volume_id   = openstack_blockstorage_volume_v3.moodle-recrut_data.id
}

resource "openstack_compute_volume_attach_v2" "moodle-recrut_db" {
  instance_id = openstack_compute_instance_v2.moodle-recrut.id
  volume_id   = openstack_blockstorage_volume_v3.moodle-recrut_db.id
}
