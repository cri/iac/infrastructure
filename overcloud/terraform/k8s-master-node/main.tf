resource "openstack_networking_port_v2" "k8s-master" {
  name           = "${var.cluster_name}-k8s-master-${var.node_id}"
  network_id     = var.network_id
  admin_state_up = true
  security_group_ids = [
    var.secgroup_id,
    var.secgroup_master_id,
  ]
  fixed_ip {
    subnet_id = var.subnet_id
  }
  # This is needed because calico uses other IPs not bound to this port
  dynamic "allowed_address_pairs" {
    for_each = [var.cluster_cidr, var.service_cluster_ip_range]
    content {
      ip_address = allowed_address_pairs.value
    }
  }
}
resource "openstack_compute_instance_v2" "k8s-master" {
  name      = "${var.cluster_name}-k8s-master-${var.node_id}"
  image_id  = var.image_id
  flavor_id = var.flavor_id

  user_data = templatefile("${path.module}/cloud-init/master.yml", {
    user_data_ssh_keys = var.user_data_ssh_keys
    register_cmd       = "${var.node_registration_command} --etcd --controlplane%{for k, v in var.node_labels} --label ${k}=${v}%{endfor}"
  })

  network {
    uuid = var.network_id
    port = openstack_networking_port_v2.k8s-master.id
  }

  security_groups = [
    var.secgroup_name,
    var.secgroup_master_name,
  ]

  scheduler_hints {
    group = var.server_group_id
  }

  metadata = {
    cluster_name = var.cluster_name
    type         = "master"
  }

  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_lb_member_v2" "this" {
  count   = var.loadbalancer_pool_id != "" ? 1 : 0
  pool_id = var.loadbalancer_pool_id

  address       = openstack_compute_instance_v2.k8s-master.network[0].fixed_ip_v4
  protocol_port = 6443
}
