variable "node_registration_command" {
  description = "Rancher registration command to run on node bootstrap"
  type        = string
}

variable "cluster_name" {
  description = "Name of the cluster"
  type        = string
}

variable "node_id" {
  description = "ID of the node"
  type        = number
}

variable "node_labels" {
  description = "Labels to add to Kubernes node"
  type        = map(string)
  default     = {}
}

variable "network_id" {
  description = "ID of the network to connect the node to"
  type        = string
}

variable "subnet_id" {
  description = "ID of the subnet to connect the node to"
  type        = string
}

variable "secgroup_id" {
  description = "ID of the security group to apply to all nodes"
  type        = string
}

variable "secgroup_master_id" {
  description = "ID of the security group to apply to all master nodes"
  type        = string
}
variable "secgroup_name" {
  description = "Name of the security group to apply to all nodes"
  type        = string
}

variable "secgroup_master_name" {
  description = "Name of the security group to apply to all master nodes"
  type        = string
}

variable "cluster_cidr" {
  description = "Kubernetes cluster internal CIDR"
  type        = string
}

variable "service_cluster_ip_range" {
  description = "Kubernetes cluster internal service CIDR"
  type        = string
}

variable "image_id" {
  description = "Instance image ID"
  type        = string
}

variable "flavor_id" {
  description = "Instance flavor ID"
  type        = string
}

variable "server_group_id" {
  description = "Server group ID"
  type        = string
}

variable "user_data_ssh_keys" {
  description = "SSH keys"
  type        = string
}

variable "loadbalancer_pool_id" {
  description = "Load balancer pool ID to add the node to"
  type        = string
}
