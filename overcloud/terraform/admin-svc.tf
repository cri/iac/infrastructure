module "CRI_admin-svc-net" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "admin-svc-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.99.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.99.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}

resource "openstack_compute_instance_v2" "CRI_bastion-1" {
  name      = "bastion-1"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m1-small.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-wg.security_group_name,
  ]

  network {
    uuid = module.CRI_admin-svc-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_networking_floatingip_v2" "CRI_admin-svc" {
  pool    = data.openstack_networking_network_v2.ext-net.name
  address = "91.243.117.141"
}
resource "openstack_dns_recordset_v2" "admin-svc_iaas_cri_epita_fr" {
  zone_id = openstack_dns_zone_v2.iaas_cri_epita_fr.id
  name    = "admin-svc.iaas.cri.epita.fr."
  type    = "A"
  records = [openstack_networking_floatingip_v2.CRI_admin-svc.address]
}
resource "openstack_lb_loadbalancer_v2" "CRI_admin-svc" {
  name           = "admin-svc"
  vip_subnet_id  = module.CRI_admin-svc-net.subnets[0].id
  admin_state_up = true
}
resource "openstack_networking_floatingip_associate_v2" "CRI_admin-svc" {
  floating_ip = openstack_networking_floatingip_v2.CRI_admin-svc.address
  port_id     = openstack_lb_loadbalancer_v2.CRI_admin-svc.vip_port_id
}

resource "openstack_dns_recordset_v2" "bastion_iaas_cri_epita_fr" {
  zone_id = openstack_dns_zone_v2.iaas_cri_epita_fr.id
  name    = "bastion.iaas.cri.epita.fr."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas_cri_epita_fr.name]
}
resource "openstack_lb_listener_v2" "CRI_admin-svc-ssh-bastion" {
  loadbalancer_id        = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol               = "TCP"
  protocol_port          = 2222
  timeout_client_data    = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
  timeout_member_connect = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
  timeout_member_data    = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
}
resource "openstack_lb_pool_v2" "CRI_admin-svc-ssh-bastion" {
  listener_id = openstack_lb_listener_v2.CRI_admin-svc-ssh-bastion.id
  protocol    = "TCP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "CRI_admin-svc-ssh-bastion" {
  pool_id = openstack_lb_pool_v2.CRI_admin-svc-ssh-bastion.id

  member {
    address       = openstack_compute_instance_v2.CRI_bastion-1.network[0].fixed_ip_v4
    protocol_port = 22
  }
}
resource "openstack_lb_listener_v2" "CRI_admin-svc-wg-bastion" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol        = "UDP"
  protocol_port   = 51820
}
resource "openstack_lb_pool_v2" "CRI_admin-svc-wg-bastion" {
  listener_id = openstack_lb_listener_v2.CRI_admin-svc-wg-bastion.id
  protocol    = "UDP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "CRI_admin-svc-wg-bastion" {
  pool_id = openstack_lb_pool_v2.CRI_admin-svc-wg-bastion.id

  member {
    address       = openstack_compute_instance_v2.CRI_bastion-1.network[0].fixed_ip_v4
    protocol_port = 51820
  }
}
