module "CRI_gitlab-ci-net" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "gitlab-ci-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.101.0/26"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.101.62"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}

resource "openstack_compute_instance_v2" "CRI_gitlab-ci" {
  count     = 2
  name      = "gitlab-ci-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.gitlab-runner.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
  ]

  network {
    uuid = module.CRI_gitlab-ci-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}
