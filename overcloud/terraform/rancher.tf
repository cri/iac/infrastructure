resource "rancher2_auth_config_openldap" "undercloud" {
  servers = ["ldap.pie.cri.epita.fr."]
  port    = 636
  tls     = true

  service_account_distinguished_name = "uid=rancher,ou=users,dc=cri,dc=epita,dc=fr"
  service_account_password           = data.vault_generic_secret.k8s-undercloud_rancher_admin.data.password
  test_username                      = "rancher"
  test_password                      = data.vault_generic_secret.k8s-undercloud_rancher_admin.data.password

  user_search_base    = "ou=users,dc=cri,dc=epita,dc=fr"
  user_name_attribute = "cn"
  group_search_base   = "ou=groups,dc=cri,dc=epita,dc=fr"
  group_object_class  = "groupOfMembers"

  access_mode = "restricted"
  allowed_principal_ids = [
    "openldap_group://cn=cri,ou=groups,dc=cri,dc=epita,dc=fr",
    "openldap_group://cn=forge-staff,ou=groups,dc=cri,dc=epita,dc=fr",
  ]
}

resource "rancher2_global_role_binding" "cri-root-admin" {
  name               = "cri-root-admin"
  global_role_id     = "admin"
  group_principal_id = "openldap_group://cn=cri-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_global_role_binding" "forge-root-admin" {
  name               = "forge-root-admin"
  global_role_id     = "admin"
  group_principal_id = "openldap_group://cn=forge-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_global_role_binding" "forge-staff-user-base" {
  name               = "forge-staff-user-base"
  global_role_id     = "user-base"
  group_principal_id = "openldap_group://cn=forge-staff,ou=groups,dc=cri,dc=epita,dc=fr"
}
