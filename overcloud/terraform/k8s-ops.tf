module "net_k8s-ops" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name   = "k8s-ops-net"

  subnets = [
    {
      cidr            = "192.168.120.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.120.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.210"]
    },
  ]
}

// PIE access
resource "openstack_networking_port_v2" "pie-router-0_k8s-ops" {
  network_id            = module.net_k8s-ops.network_id
  port_security_enabled = false
  fixed_ip {
    subnet_id  = module.net_k8s-ops.subnets[0].id
    ip_address = "192.168.120.253"
  }
}
resource "openstack_networking_router_interface_v2" "pie-router-0_k8s-ops" {
  router_id = data.openstack_networking_router_v2.pie[0].id
  port_id   = openstack_networking_port_v2.pie-router-0_k8s-ops.id
}
resource "openstack_networking_subnet_route_v2" "pie-router-0_k8s-ops" {
  for_each         = toset(local.pie-subnets)
  subnet_id        = module.net_k8s-ops.subnets[0].id
  destination_cidr = each.value
  next_hop         = openstack_networking_port_v2.pie-router-0_k8s-ops.fixed_ip[0].ip_address
}

resource "openstack_dns_recordset_v2" "api_ops_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "${module.k8s_ops.cluster_fqdn_api}."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas_cri_epita_fr.name]
}
resource "openstack_identity_application_credential_v3" "k8s-ops" {
  provider = openstack.rancher

  name  = "k8s-ops OpenStack Cloud Controller Manager"
  roles = ["member", "load-balancer_member"]
}

module "k8s_ops" {
  source = "./k8s"

  cluster_name       = "ops"
  cluster_fqdn_api   = "api.ops.k8s.cri.epita.fr"
  kubernetes_version = "v1.25.16-rancher2-3"

  masters_affinity = "anti-affinity"
  nodes_affinity   = "anti-affinity"

  subnet_id = module.net_k8s-ops.subnets[0].id

  loadbalancer_id   = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  loadbalancer_port = 6440

  openstack_application_credential_id     = openstack_identity_application_credential_v3.k8s-ops.id
  openstack_application_credential_secret = openstack_identity_application_credential_v3.k8s-ops.secret

  argocd_rancher_cluster_id = data.rancher2_cluster.rancher_argocd.id
  argocd_cluster_extra_labels = {
    tenant = "CRI"
    type   = "openstack-rancher"
  }
}

module "k8s_ops-master-nodes" {
  count  = 3
  source = "./k8s-master-node"

  cluster_name              = "ops"
  node_id                   = count.index
  node_registration_command = module.k8s_ops.node_registration_command

  network_id = module.net_k8s-ops.network_id
  subnet_id  = module.net_k8s-ops.subnets[0].id

  secgroup_id              = module.k8s_ops.secgroup_id
  secgroup_master_id       = module.k8s_ops.secgroup_master_id
  secgroup_name            = module.k8s_ops.secgroup_name
  secgroup_master_name     = module.k8s_ops.secgroup_master_name
  cluster_cidr             = module.k8s_ops.cluster_cidr
  service_cluster_ip_range = module.k8s_ops.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-24-04-20240505.id
  flavor_id = data.openstack_compute_flavor_v2.m2-medium-ssd.id

  server_group_id    = module.k8s_ops.master_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys

  loadbalancer_pool_id = module.k8s_ops.loadbalancer_pool_id
}

module "k8s_ops-worker-nodes" {
  count  = 3
  source = "./k8s-worker-node"

  cluster_name              = "ops"
  node_id                   = count.index
  node_registration_command = module.k8s_ops.node_registration_command

  network_id = module.net_k8s-ops.network_id
  subnet_id  = module.net_k8s-ops.subnets[0].id

  secgroup_id              = module.k8s_ops.secgroup_id
  secgroup_name            = module.k8s_ops.secgroup_name
  cluster_cidr             = module.k8s_ops.cluster_cidr
  service_cluster_ip_range = module.k8s_ops.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m1-xlarge.id

  server_group_id    = module.k8s_ops.worker_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys
}

module "k8s_ops-worker-node-3" {
  source = "./k8s-worker-node"

  cluster_name              = "ops"
  node_id                   = 3
  node_registration_command = module.k8s_ops.node_registration_command

  network_id = module.net_k8s-ops.network_id
  subnet_id  = module.net_k8s-ops.subnets[0].id

  secgroup_id              = module.k8s_ops.secgroup_id
  secgroup_name            = module.k8s_ops.secgroup_name
  cluster_cidr             = module.k8s_ops.cluster_cidr
  service_cluster_ip_range = module.k8s_ops.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.m1-xlarge.id

  server_group_id    = module.k8s_ops.worker_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys
}

resource "openstack_networking_floatingip_v2" "ops-svc" {
  # This is some nasty IP squatting so we can reuse those for this or other
  # clusters.
  for_each = toset([
    "91.243.117.179",
  ])
  pool    = data.openstack_networking_network_v2.ext-net.name
  address = each.value
}
resource "openstack_dns_recordset_v2" "ingress_ops_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "ingress.ops.k8s.cri.epita.fr."
  type    = "A"
  records = ["91.243.117.179"]
}
resource "openstack_dns_recordset_v2" "wild_ops_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "*.ops.k8s.cri.epita.fr."
  type    = "CNAME"
  records = ["ingress.ops.k8s.cri.epita.fr."]
}

resource "openstack_identity_application_credential_v3" "prometheus_sd" {
  name  = "k8s-ops Prometheus Service Discovery"
  roles = ["reader"]
}
resource "rancher2_secret_v2" "prometheus_additional_scrape" {
  cluster_id = module.k8s_ops.cluster_id
  name       = "prometheus-additional-scrape-configs"
  namespace  = "cattle-monitoring-system"
  data = {
    "prometheus-config.yml" = <<-EOC
      - job_name: pie_node
        scrape_interval: 60s
        scrape_timeout: 3s
        http_sd_configs:
          - url: https://fleet.pie.cri.epita.fr/api/fleet/prometheus_sd/
        metric_relabel_configs:
          - source_labels: [__name__]
            regex: 'node_(.*)'
            target_label: __name__
            replacement: 'pie_node_$1'

      - job_name: infra_node
        scrape_interval: 30s
        openstack_sd_configs:
          - role: instance
            region: RegionOne
            identity_endpoint: https://openstack.cri.epita.fr:5000/v3/
            application_credential_id: '${openstack_identity_application_credential_v3.prometheus_sd.id}'
            application_credential_secret: '${openstack_identity_application_credential_v3.prometheus_sd.secret}'
        relabel_configs:
          - source_labels: [__meta_openstack_instance_name]
            target_label: __address__
            replacement: '$1.cri.openstack.epita.fr:9100'
          - source_labels: [__meta_openstack_instance_name]
            regex: '.*k8s.*'
            action: drop
    EOC
  }
}
