# Terraform

## Initial setup

```sh
source config.sh
```

### Create a `clouds.yaml` file

```
clouds:
  openstack:
    auth:
      auth_url: https://openstack.cri.epita.fr:5000
      username: "FIXME"
      password: "FIXME"
      project_id: e3b3356ef0384e839a6d650b658f7e90
      project_name: "CRI"
      user_domain_name: "CRI"
    region_name: "RegionOne"
    interface: "public"
    identity_api_version: 3
```

It is essential that you do not modify `project_id` as it determines where
terraform resources are created.

For now, only username/password authentication works, you won't be able to use
application credentials.

### openstack CLI usage

```sh
poetry install
poetry run openstack --help
```
