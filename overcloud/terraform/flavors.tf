data "openstack_compute_flavor_v2" "m1-tiny" {
  name = "m1.tiny"
}

data "openstack_compute_flavor_v2" "m1-small" {
  name = "m1.small"
}
data "openstack_compute_flavor_v2" "m2-small" {
  name = "m2.small"
}

data "openstack_compute_flavor_v2" "m1-medium" {
  name = "m1.medium"
}
data "openstack_compute_flavor_v2" "m2-medium" {
  name = "m2.medium"
}
data "openstack_compute_flavor_v2" "m2-medium-ssd" {
  name = "m2.medium.ssd"
}

data "openstack_compute_flavor_v2" "m1-large" {
  name = "m1.large"
}
data "openstack_compute_flavor_v2" "m2-large" {
  name = "m2.large"
}
data "openstack_compute_flavor_v2" "m2-large-ssd" {
  name = "m2.large.ssd"
}

data "openstack_compute_flavor_v2" "m1-xlarge" {
  name = "m1.xlarge"
}
data "openstack_compute_flavor_v2" "m2-xlarge" {
  name = "m2.xlarge"
}

data "openstack_compute_flavor_v2" "clone-store" {
  name = "CRI.clone-store"
}
data "openstack_compute_flavor_v2" "gitlab-runner" {
  name = "CRI.gitlab-runner"
}
data "openstack_compute_flavor_v2" "m1-huge-gpu" {
  name = "CRI.m1.huge-gpu"
}
data "openstack_compute_flavor_v2" "m1-2xlarge-ssd" {
  name = "CRI.m1.2xlarge-ssd"
}
data "openstack_compute_flavor_v2" "huge-ssd" {
  name = "CRI.huge-ssd"
}
data "openstack_compute_flavor_v2" "cri-maas-m2-large" {
  name = "CRI.maas-m2.large"
}
