module "dns_k8s_cri_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "k8s.cri.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {}
}

data "rancher2_cluster" "rancher_argocd" {
  name = "local"
}
