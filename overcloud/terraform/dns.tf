module "dns" {
  source = "./dns"
}

resource "openstack_dns_zone_v2" "iaas_cri_epita_fr" {
  name  = "iaas.cri.epita.fr."
  email = "hostmaster@cri.epita.fr"
  type  = "PRIMARY"
}

resource "openstack_dns_zone_v2" "cri_iaas_epita_fr" {
  name  = "cri.iaas.epita.fr."
  email = "hostmaster@cri.epita.fr"
  type  = "PRIMARY"
}

module "dns_cri_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "cri.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A   = { records = ["91.243.117.180"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\"", "google-site-verification=Mo2E7ziGW8yuD9PZlJ3EkE6clYLE4wFVlfJ5y_gLkAU"] }
    }

    bm = {
      NS = { records = ["ns"] }
    }
    ceph = {
      NS = { records = ["ns"] }
    }
    gate = {
      NS = { records = ["ns"] }
    }
    iaas = {
      NS = { records = ["designate.openstack"] }
    }
    k8s = {
      NS = { records = ["designate.openstack"] }
    }
    lab = {
      NS = { records = ["designate.openstack"] }
    }
    mgmt = {
      NS = { records = ["ns"] }
    }
    openstack = {
      NS = { records = ["ns"] }
    }
    pie = {
      NS = { records = ["designate.openstack"] }
    }
    sm = {
      NS = { records = ["fleet-ns.pie"] }
    }
    srv = {
      NS = { records = ["designate.openstack"] }
    }
    undercloud = {
      NS = { records = ["ns"] }
    }

    _kerberos = {
      TXT = { records = ["CRI.EPITA.FR"] }
    }
    "_kerberos._tcp" = {
      SRV = { records = ["0 100 88 kerberos"] }
    }
    "_kerberos-adm._tcp" = {
      SRV = { records = ["0 100 749 kerberos"] }
    }
    "_kerberos-master._tcp" = {
      SRV = { records = ["0 100 88 kerberos"] }
    }
    "_kpasswd._tcp" = {
      SRV = { records = ["0 100 464 kerberos"] }
    }

    "cri._domainkey" = {
      TXT = { records = ["\"v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC2tMgXo/6RTaojLSCa+p2BSjcUM1FAd6gYNG8BeYE/BytPfuMSyuPd/NpmC0I7+uHL/AZsyRo4AJYFuBKNB0hG5S5xSE2e/HeiGCYxHceu+xx96tV906a0lBij6i5oNuApkUm5snDDpDM5DlZGH24bqa6NotzD48H/wHRhhhvgZQIDAQAB\""] }
    }
    "cri._domainkey.groups" = {
      CNAME = { records = ["cri._domainkey"] }
    }
    "cri._domainkey.nntp-gate" = {
      CNAME = { records = ["cri._domainkey"] }
    }
    "cri._domainkey.ml" = {
      CNAME = { records = ["cri._domainkey"] }
    }
    "cri._domainkey.tickets" = {
      CNAME = { records = ["cri._domainkey"] }
    }
    "_dmarc.groups" = {
      CNAME = { records = ["_dmarc"] }
    }
    "_dmarc.ml" = {
      CNAME = { records = ["_dmarc"] }
    }
    "_dmarc.tickets" = {
      CNAME = { records = ["_dmarc"] }
    }

    blog = {
      CNAME = { records = ["cri.pages.epita.fr."] }
    }
    "blog.dev" = {
      CNAME = { records = ["reverse"] }
    }
    bookstack = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    discord = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    django-admin = {
      CNAME = { records = ["reverse"] }
    }
    doc = {
      CNAME = { records = ["cri.pages.epita.fr."] }
    }
    exam = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    feedback = {
      CNAME = { records = ["reverse"] }
    }
    git = {
      A = { records = ["91.243.117.182"] }
    }
    gitlab = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    groups = {
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    hedgedoc = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    kerberos = {
      A = { records = ["91.243.117.186"] }
    }
    inventree = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    maas = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    "maths.prepa" = {
      CNAME = { records = ["prepa.pages.epita.fr."] }
    }
    moodle = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    moodle-1819 = {
      CNAME = { records = ["reverse"] }
    }
    moodle-exam = {
      CNAME = { records = ["reverse"] }
    }
    mail = {
      A   = { records = ["91.243.117.197"] }
      TXT = { records = ["\"v=spf1 ip4:91.243.117.192/29 ip4:91.243.117.208/29 mx a -all\""] }
    }
    "_acme-challenge.mail" = {
      CNAME = { records = ["_acme-challenge.mail.undercloud"] }
    }
    ml = {
      A   = { records = ["91.243.117.196"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    netbox = {
      CNAME = { records = ["netbox.undercloud"] }
    }
    news = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    nntp = {
      CNAME = { records = ["news"] }
    }
    nntp-gate = {
      A   = { records = ["91.243.117.196"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    ns = {
      A = { records = ["91.243.117.210"] }
    }
    photos = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    recrutements = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    registry = {
      CNAME = { records = ["gitlab"] }
    }
    reverse = {
      A = { records = ["91.243.117.196"] }
    }
    reverse-k8s = {
      A = { records = ["91.243.117.193"] }
    }
    rocketchat = {
      CNAME = { records = ["ingress.prod-1.k8s"] }
    }
    rtfm = {
      CNAME = { records = ["doc"] }
    }
    s3 = {
      CNAME = { records = ["rgw.ceph"] }
    }
    "*.s3" = {
      CNAME = { records = ["rgw.ceph"] }
    }
    "_acme-challenge.s3" = {
      CNAME = { records = ["_acme-challenge.rgw.ceph"] }
    }
    sand = {
      CNAME = { records = ["reverse"] }
    }
    static = {
      CNAME = { records = ["reverse"] }
    }
    tickets = {
      A   = { records = ["91.243.117.180"] }
      MX  = { records = ["10 mail"] }
      TXT = { records = ["\"v=spf1 redirect=mail.cri.epita.fr\""] }
    }
    vault = {
      CNAME = { records = ["vault.undercloud"] }
    }
    "_dmarc" = {
      TXT = { records = ["\"v=DMARC1; p=none; rua=mailto:dmarc@cri.epita.fr; ruf=mailto:dmarc@cri.epita.fr; fo=1;\""] }
    }
  }
}

module "dns_pie_cri_epita_fr" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-zone.git?ref=2.0.0"

  zone_name  = "pie.cri.epita.fr."
  zone_email = "hostmaster@cri.epita.fr"

  recordsets = {
    "@" = {
      A = { records = ["10.201.5.2"] }
    }
    clone-store = {
      A = { records = ["10.201.5.111"] }
    }
    django-pxe = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    fleet = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    fleet-ns = {
      A = { records = ["91.243.117.187"] }
    }
    gate = {
      A = { records = ["10.201.5.2"] }
    }
    ipxe = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    jetbrains-lic = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    kerberos = {
      A = { records = ["91.243.117.186"] }
    }
    kms = {
      A = { records = ["10.81.0.20"] }
    }
    ldap = {
      A = { records = ["91.243.117.185"] }
    }
    machine-state = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    ns = {
      A = { records = ["10.201.5.53"] }
    }
    ntp = {
      CNAME = { records = ["gate"] }
    }
    reverse = {
      CNAME = { records = ["reverse.cri.epita.fr."] }
    }
    salt = {
      A = { records = ["10.201.5.45"] }
    }
    salt-api = {
      CNAME = { records = ["ingress.prod-1.k8s.cri.epita.fr."] }
    }
    static = {
      CNAME = { records = ["reverse"] }
    }
    tickets = {
      CNAME = { records = ["reverse"] }
    }
    torrent = {
      A = { records = ["91.243.117.181"] }
    }
  }
}
