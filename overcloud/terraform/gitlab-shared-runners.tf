module "net_gitlab-shared-runners" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "gitlab-shared-runners-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.101.64/26"
      router_id       = data.openstack_networking_router_v2.external-router[1].id
      gateway_ip      = "192.168.101.126"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}
resource "openstack_networking_port_v2" "admin-svc_gitlab-shared-runners" {
  network_id = module.net_gitlab-shared-runners.network_id
  fixed_ip {
    subnet_id = module.net_gitlab-shared-runners.subnets[0].id
  }
}
resource "openstack_networking_router_interface_v2" "admin-svc_gitlab-shared-runners" {
  router_id = data.openstack_networking_router_v2.external-router[0].id
  port_id   = openstack_networking_port_v2.admin-svc_gitlab-shared-runners.id
}
resource "openstack_networking_subnet_route_v2" "admin-svc_gitlab-shared-runners" {
  subnet_id        = module.net_gitlab-shared-runners.subnets[0].id
  destination_cidr = module.CRI_admin-svc-net.subnets[0].cidr
  next_hop         = openstack_networking_port_v2.admin-svc_gitlab-shared-runners.all_fixed_ips[0]
}
resource "openstack_networking_subnet_route_v2" "k8s-ops_gitlab-shared-runners" {
  subnet_id        = module.net_gitlab-shared-runners.subnets[0].id
  destination_cidr = module.net_k8s-ops.subnets[0].cidr
  next_hop         = openstack_networking_port_v2.admin-svc_gitlab-shared-runners.all_fixed_ips[0]
}

module "sg_gitlab-shared-runners-proxy-ingress" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "GitLab Shared Runners Proxy 3128 Ingress only"
  description          = <<EOT
    Only allow Proxy 3128 Ingress. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "tcp"
      port_range_min = 3128
      port_range_max = 3128
    }
  ]
}

resource "openstack_compute_instance_v2" "gitlab-shared-runners-proxy" {
  name      = "gitlab-shared-runners-proxy"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m2-small.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_gitlab-shared-runners-proxy-ingress.security_group_name,
  ]

  network {
    uuid = module.net_gitlab-shared-runners.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_compute_instance_v2" "gitlab-shared-runners" {
  count     = 5
  name      = "gitlab-shared-runner-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m2-xlarge.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
  ]

  network {
    uuid = module.net_gitlab-shared-runners.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}
