locals {
  srvc_maas_worker_nb_instances = 10
}

module "net_maas" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "maas-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.106.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.106.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.210"]
    },
  ]
}

data "openstack_images_image_v2" "srvc_maas-worker" {
  name        = "srvc-maas-worker-v1.6.1"
  most_recent = true
}

data "vault_kv_secret_v2" "srvc-maas_registry" {
  mount = "k8s-prod-1"
  name  = "maas/registries"
}

data "vault_kv_secret_v2" "srvc-maas_gitlab-token" {
  mount = "k8s-prod-1"
  name  = "forge-intranet/srvc-maas"
}

data "vault_kv_secret_v2" "srvc-maas_bootstrap-token" {
  mount = "k8s-prod-1"
  name  = "forge-intranet/repo-maas"
}

resource "openstack_compute_servergroup_v2" "srvc-maas-worker" {
  name     = "srvc-maas-worker"
  policies = ["soft-anti-affinity"]
}
data "openstack_compute_flavor_v2" "cri-maas-m2-large-nested-tmp" {
  name = "CRI.maas-m2.large.nested-tmp"
}

resource "openstack_compute_instance_v2" "srvc-maas-worker" {
  count = local.srvc_maas_worker_nb_instances

  name = "srvc-maas-worker-${count.index + 1}"

  image_id  = data.openstack_images_image_v2.srvc_maas-worker.id
  flavor_id = data.openstack_compute_flavor_v2.cri-maas-m2-large-nested-tmp.id

  user_data = local.user_data_ssh_keys

  metadata = {
    name = "srvc-maas-worker-${count.index + 1}.${lower(data.openstack_identity_project_v3.CRI.name)}.openstack.epita.fr"

    gitlab_token = data.vault_kv_secret_v2.srvc-maas_gitlab-token.data["ANSIBLE_GITLAB_TOKEN"]
    version      = "1.4.15-SNAPSHOT"
    snapshot     = "1.4.15-20250227.190453-1"

    scheduler_url   = "https://repo-maas.api.forge.epita.fr/"
    tenantSlug      = null
    bootstrap_token = data.vault_kv_secret_v2.srvc-maas_bootstrap-token.data["MAAS_BOOTSTRAP_TOKENS"]
    payload_type    = "LEGACY_WORKFLOW"

    nb_cpus   = 0 // no limit
    memory_mb = data.openstack_compute_flavor_v2.cri-maas-m2-large.ram - 1024

    registries = jsonencode([
      for url, token in data.vault_kv_secret_v2.srvc-maas_registry.data : {
        url   = url
        token = token
      }
    ])

    labels = jsonencode({})
  }

  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
  ]

  network {
    uuid = module.net_maas.network_id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.srvc-maas-worker.id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

# this is utterly disgusting but this was an emergency
# TODO: remove when not needed anymore
resource "openstack_compute_instance_v2" "srvc-maas-worker-old" {
  count = 15

  name = "srvc-maas-worker-${count.index + local.srvc_maas_worker_nb_instances + 1}"

  image_id  = data.openstack_images_image_v2.srvc_maas-worker.id
  flavor_id = data.openstack_compute_flavor_v2.cri-maas-m2-large.id

  user_data = local.user_data_ssh_keys

  metadata = {
    name = "srvc-maas-worker-${count.index + local.srvc_maas_worker_nb_instances + 1}.${lower(data.openstack_identity_project_v3.CRI.name)}.openstack.epita.fr"

    gitlab_token = data.vault_kv_secret_v2.srvc-maas_gitlab-token.data["ANSIBLE_GITLAB_TOKEN"]
    version      = "1.4.15-SNAPSHOT"
    snapshot     = "1.4.15-20250227.190453-1"

    scheduler_url   = "https://repo-maas.api.forge.epita.fr/"
    tenantSlug      = null
    bootstrap_token = data.vault_kv_secret_v2.srvc-maas_bootstrap-token.data["MAAS_BOOTSTRAP_TOKENS"]
    payload_type    = "LEGACY_WORKFLOW"

    nb_cpus   = 0 // no limit
    memory_mb = data.openstack_compute_flavor_v2.cri-maas-m2-large.ram - 1024

    registries = jsonencode([
      for url, token in data.vault_kv_secret_v2.srvc-maas_registry.data : {
        url   = url
        token = token
      }
    ])

    labels = jsonencode({})
  }

  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
  ]

  network {
    uuid = module.net_maas.network_id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.srvc-maas-worker.id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}
