# Networks

module "CRI_moodle-net" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "moodle-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.105.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.105.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}

resource "openstack_blockstorage_volume_v3" "moodle_data" {
  name = "moodle_data"
  size = 300
}

# MOODLE 2023 4.2
resource "openstack_blockstorage_volume_v3" "moodle-2023_data" {
  name                 = "moodle-2023_data"
  size                 = 400
  snapshot_id          = "0e934171-4b70-46b4-8647-e81707d58af2" # was deleted because volume is flattened
  enable_online_resize = true
}

resource "openstack_compute_instance_v2" "moodle-2023" {
  name      = "moodle-2023"
  flavor_id = data.openstack_compute_flavor_v2.m2-xlarge.id
  key_pair  = openstack_compute_keypair_v2.dummy.name
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-http-https.security_group_name,
  ]

  network {
    uuid = module.CRI_moodle-net.network_id
  }

  lifecycle {
    ignore_changes = [
      key_pair,
      user_data,
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "moodle-2023_data" {
  instance_id = openstack_compute_instance_v2.moodle-2023.id
  volume_id   = openstack_blockstorage_volume_v3.moodle-2023_data.id
}
