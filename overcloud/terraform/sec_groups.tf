resource "openstack_networking_secgroup_v2" "CRI_ingress-all" {
  name                 = "All Ingress traffic"
  description          = <<EOT
    Allow all Ingress traffic. DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-all-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-all.id
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-all-ipv6" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-all.id
  direction         = "ingress"
  ethertype         = "IPv6"
  remote_ip_prefix  = "::/0"
}

resource "openstack_networking_secgroup_v2" "CRI_egress-all" {
  name                 = "All Egress traffic"
  description          = <<EOT
    Allow all Egress traffic. This security group is meant to be used with
    other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-all-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_egress-all.id
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-all-ipv6" {
  security_group_id = openstack_networking_secgroup_v2.CRI_egress-all.id
  direction         = "egress"
  ethertype         = "IPv6"
  remote_ip_prefix  = "::/0"
}

resource "openstack_networking_secgroup_v2" "CRI_egress-external" {
  name                 = "All external Egress traffic"
  description          = <<EOT
    Allow all external Egress traffic. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-external-ipv4" {
  for_each = toset(local.external_ip_ranges)

  security_group_id = openstack_networking_secgroup_v2.CRI_egress-external.id
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = each.value
}

resource "openstack_networking_secgroup_v2" "CRI_egress-pie" {
  name                 = "All PIE Egress traffic"
  description          = <<EOT
    Allow all Egress traffic to PIE. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-pie-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_egress-pie.id
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "10.0.0.0/8"
}

resource "openstack_networking_secgroup_v2" "CRI_egress-common-svc_router" {
  name                 = "common-svc router Egress traffic"
  description          = <<EOT
    Allow Egress traffic to common-svc router. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-common-svc_router-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_egress-common-svc_router.id
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "192.168.103.254/32"
}

resource "openstack_networking_secgroup_v2" "CRI_egress-broadcast" {
  name                 = "Broadcast Egress traffic"
  description          = <<EOT
    Allow Egress traffic to broadcast. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_egress-broadcast-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_egress-common-svc_router.id
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "255.0.0.0/8"
}

resource "openstack_networking_secgroup_v2" "CRI_ingress-icmp" {
  name                 = "All ICMP traffic"
  description          = <<EOT
    Allow all Egress traffic. This security group is meant to be used with
    other ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-icmp-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-icmp.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-icmp-ipv6" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-icmp.id
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "ipv6-icmp"
  remote_ip_prefix  = "::/0"
}

resource "openstack_networking_secgroup_v2" "CRI_ingress-ssh" {
  name                 = "SSH Ingress only"
  description          = <<EOT
    Only allow SSH Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-ssh-ipv4" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-ssh.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "CRI_ingress-ssh-ipv6" {
  security_group_id = openstack_networking_secgroup_v2.CRI_ingress-ssh.id
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "::/0"
}

module "sg_ingress-http-https" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "HTTP and HTTPS Ingress only"
  description          = <<EOT
    Only allow HTTP and HTTPS Ingress. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "tcp"
      port_range_min = 80
      port_range_max = 80
    },
    {
      protocol       = "tcp"
      port_range_min = 443
      port_range_max = 443
    }
  ]
}

module "sg_ingress-dns" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "DNS Ingress only"
  description          = <<EOT
    Only allow DNS Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 53
      port_range_max = 53
    },
    {
      protocol       = "tcp"
      port_range_min = 53
      port_range_max = 53
    }
  ]
}

module "sg_ingress-nfs" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "NFS Ingress only"
  description          = <<EOT
    Only allow NFS Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 111
      port_range_max = 111
    },
    {
      protocol       = "tcp"
      port_range_min = 111
      port_range_max = 111
    },
    {
      protocol       = "udp"
      port_range_min = 2049
      port_range_max = 2049
    },
    {
      protocol       = "tcp"
      port_range_min = 2049
      port_range_max = 2049
    },
  ]
}

module "sg_ingress-node-exporter" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "Node exporter Ingress only"
  description          = <<EOT
    Only allow node exporter Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "tcp"
      port_range_min = 9100
      port_range_max = 9100
    },
  ]
}

module "sg_ingress-dhcp" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "DHCP Ingress only"
  description          = <<EOT
    Only allow DHCP Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 67
      port_range_max = 67
    }
  ]
}

module "sg_ingress-tftp" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "TFTP Ingress only"
  description          = <<EOT
    Only allow TFTP Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 69
      port_range_max = 69
    }
  ]
}

module "sg_ingress-wg" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "Wireguard Ingress only"
  description          = <<EOT
    Only allow Wireguard Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 51820
      port_range_max = 51820
    }
  ]
}

module "sg_ingress-smtp-mail-legacy" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "SMTP Ingress from Mail legacy only"
  description          = <<EOT
    Only allow SMTP Ingress from Mail Legacy. This security group is meant to
    be used with other ones.
  EOT
  delete_default_rules = true

  ingress_rules_ipv4 = [
    {
      remote_ip_prefix = "192.168.204.78/32"
      protocol         = "tcp"
      port_range_min   = 25
      port_range_max   = 25
    }
  ]
}

locals {
  external_ip_ranges = [
    "0.0.0.0/5",
    "8.0.0.0/7",
    "11.0.0.0/8",
    "12.0.0.0/6",
    "16.0.0.0/4",
    "32.0.0.0/3",
    "64.0.0.0/2",
    "128.0.0.0/3",
    "160.0.0.0/5",
    "168.0.0.0/6",
    "172.0.0.0/12",
    "172.32.0.0/11",
    "172.64.0.0/10",
    "172.128.0.0/9",
    "173.0.0.0/8",
    "174.0.0.0/7",
    "176.0.0.0/4",
    "192.0.0.0/9",
    "192.128.0.0/11",
    "192.160.0.0/13",
    "192.169.0.0/16",
    "192.170.0.0/15",
    "192.172.0.0/14",
    "192.176.0.0/12",
    "192.192.0.0/10",
    "193.0.0.0/8",
    "194.0.0.0/7",
    "196.0.0.0/6",
    "200.0.0.0/5",
    "208.0.0.0/4",
  ]
}
