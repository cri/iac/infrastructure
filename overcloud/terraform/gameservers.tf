module "CRI_gameservers-net" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "gameservers-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.104.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.104.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}


# Warsow

locals {
  gameservers_warsow_info_port = 27950
  gameservers_warsow_main_port = 44400
  gameservers_warsow_http_port = 44480
}

module "sg_ingress-warsow" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "Warsow ingress rules"
  description          = <<EOT
    Only allow port used by Warsow. This security group is meant to be used
    with other ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = local.gameservers_warsow_info_port
      port_range_max = local.gameservers_warsow_info_port
      }, {
      protocol       = "udp"
      port_range_min = local.gameservers_warsow_main_port
      port_range_max = local.gameservers_warsow_main_port
      }, {
      protocol       = "tcp"
      port_range_min = local.gameservers_warsow_http_port
      port_range_max = local.gameservers_warsow_http_port
    },
  ]
}

resource "openstack_compute_instance_v2" "CRI_gameservers_warsow" {
  count     = 0
  name      = "warsow-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m2-medium.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-warsow.security_group_name,
  ]

  network {
    uuid = module.CRI_gameservers-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_blockstorage_volume_v3" "gameservers_warsow_data" {
  count = length(openstack_compute_instance_v2.CRI_gameservers_warsow)
  name  = "warsow_data-${count.index}"
  size  = 50
}
resource "openstack_compute_volume_attach_v2" "gameservers_warsow_data" {
  count       = length(openstack_compute_instance_v2.CRI_gameservers_warsow)
  instance_id = openstack_compute_instance_v2.CRI_gameservers_warsow[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.gameservers_warsow_data[count.index].id
}

resource "openstack_dns_recordset_v2" "warsow_iaas_cri_epita_fr" {
  zone_id = openstack_dns_zone_v2.iaas_cri_epita_fr.id
  name    = "warsow.iaas.cri.epita.fr."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas_cri_epita_fr.name]
}

resource "openstack_lb_listener_v2" "gameservers_warsow_info" {
  name            = "Warsow Info"
  loadbalancer_id = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol        = "UDP"
  protocol_port   = local.gameservers_warsow_info_port
}
resource "openstack_lb_pool_v2" "gameservers_warsow_info" {
  listener_id = openstack_lb_listener_v2.gameservers_warsow_info.id
  protocol    = "UDP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "gameservers_warsow_info" {
  pool_id = openstack_lb_pool_v2.gameservers_warsow_info.id

  dynamic "member" {
    for_each = openstack_compute_instance_v2.CRI_gameservers_warsow
    content {
      address       = member.value.network[0].fixed_ip_v4
      protocol_port = local.gameservers_warsow_info_port
    }
  }
}
resource "openstack_lb_listener_v2" "gameservers_warsow_main" {
  name            = "Warsow Game"
  loadbalancer_id = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol        = "UDP"
  protocol_port   = local.gameservers_warsow_main_port
}
resource "openstack_lb_pool_v2" "gameservers_warsow_main" {
  listener_id = openstack_lb_listener_v2.gameservers_warsow_main.id
  protocol    = "UDP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "gameservers_warsow_main" {
  pool_id = openstack_lb_pool_v2.gameservers_warsow_main.id

  dynamic "member" {
    for_each = openstack_compute_instance_v2.CRI_gameservers_warsow
    content {
      address       = member.value.network[0].fixed_ip_v4
      protocol_port = local.gameservers_warsow_main_port
    }
  }
}
resource "openstack_lb_listener_v2" "gameservers_warsow_http" {
  name            = "Warsow HTTP"
  loadbalancer_id = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol        = "TCP"
  protocol_port   = local.gameservers_warsow_http_port
}
resource "openstack_lb_pool_v2" "gameservers_warsow_http" {
  listener_id = openstack_lb_listener_v2.gameservers_warsow_http.id
  protocol    = "TCP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "gameservers_warsow_http" {
  pool_id = openstack_lb_pool_v2.gameservers_warsow_http.id

  dynamic "member" {
    for_each = openstack_compute_instance_v2.CRI_gameservers_warsow
    content {
      address       = member.value.network[0].fixed_ip_v4
      protocol_port = local.gameservers_warsow_http_port
    }
  }
}
