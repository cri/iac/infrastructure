data "openstack_images_image_v2" "ubuntu-20-04-20210201" {
  name = "Ubuntu 20.04 release 20210201"
}

data "openstack_images_image_v2" "ubuntu-20-04-20220815" {
  name = "Ubuntu 20.04 release 20220815"
}

data "openstack_images_image_v2" "ubuntu-22-04-20221120" {
  name = "Ubuntu 22.04 release 20221120"
}

data "openstack_images_image_v2" "ubuntu-24-04-20240505" {
  name = "Ubuntu 24.04 LTS release 20240505"
}

data "openstack_images_image_v2" "ubuntu-24-04-20241210" {
  name = "Ubuntu 24.04 LTS release 20241210"
}

data "openstack_images_image_v2" "cirros-0-5-1" {
  name = "CirrOS 0.5.1"
}

data "openstack_images_image_v2" "fedora-coreos-33-20210217-3-0" {
  name = "Fedora CoreOS 33 Build 20210217.3.0"
}

data "openstack_images_image_v2" "centos-8-3-2011-20201204-2" {
  name = "CentOS 8.3.2011 20201204.2"
}

data "openstack_images_image_v2" "windows-10-20h2" {
  name = "Windows 10 20H2"
}
