module "net_k8s-prod-1" {
  source = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name   = "k8s-prod-1-net"

  subnets = [
    {
      cidr            = "192.168.121.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.121.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.210"]
    },
  ]
}

// PIE access
resource "openstack_networking_port_v2" "pie-router-0_k8s-prod-1" {
  network_id            = module.net_k8s-prod-1.network_id
  port_security_enabled = false
  fixed_ip {
    subnet_id  = module.net_k8s-prod-1.subnets[0].id
    ip_address = "192.168.121.252"
  }
}
resource "openstack_networking_router_interface_v2" "pie-router-0_k8s-prod-1" {
  router_id = data.openstack_networking_router_v2.pie[0].id
  port_id   = openstack_networking_port_v2.pie-router-0_k8s-prod-1.id
}
resource "openstack_networking_subnet_route_v2" "pie-router-0_k8s-prod-1" {
  for_each         = toset(local.pie-subnets)
  subnet_id        = module.net_k8s-prod-1.subnets[0].id
  destination_cidr = each.value
  next_hop         = openstack_networking_port_v2.pie-router-0_k8s-prod-1.fixed_ip[0].ip_address
}

resource "openstack_dns_recordset_v2" "api_prod-1_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "${module.k8s_prod-1.cluster_fqdn_api}."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas_cri_epita_fr.name]
}
resource "openstack_identity_application_credential_v3" "k8s-prod-1" {
  provider = openstack.rancher

  name  = "k8s-prod-1 OpenStack Cloud Controller Manager"
  roles = ["member", "load-balancer_member"]
}

module "k8s_prod-1" {
  source = "./k8s"

  cluster_name       = "prod-1"
  cluster_fqdn_api   = "api.prod-1.k8s.cri.epita.fr"
  kubernetes_version = "v1.24.17-rancher1-1"

  masters_affinity = "anti-affinity"
  nodes_affinity   = "anti-affinity"

  subnet_id = module.net_k8s-prod-1.subnets[0].id

  loadbalancer_id   = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  loadbalancer_port = 6443

  openstack_application_credential_id     = openstack_identity_application_credential_v3.k8s-prod-1.id
  openstack_application_credential_secret = openstack_identity_application_credential_v3.k8s-prod-1.secret

  argocd_rancher_cluster_id = data.rancher2_cluster.rancher_argocd.id
  argocd_cluster_extra_labels = {
    tenant = "CRI"
    type   = "openstack-rancher"
  }
}

module "k8s_prod-1-master-nodes" {
  count  = 3
  source = "./k8s-master-node"

  cluster_name              = "prod-1"
  node_id                   = count.index
  node_registration_command = module.k8s_prod-1.node_registration_command

  network_id = module.net_k8s-prod-1.network_id
  subnet_id  = module.net_k8s-prod-1.subnets[0].id

  secgroup_id          = module.k8s_prod-1.secgroup_id
  secgroup_master_id   = module.k8s_prod-1.secgroup_master_id
  secgroup_name        = module.k8s_prod-1.secgroup_name
  secgroup_master_name = module.k8s_prod-1.secgroup_master_name

  cluster_cidr             = module.k8s_prod-1.cluster_cidr
  service_cluster_ip_range = module.k8s_prod-1.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-24-04-20241210.id
  flavor_id = data.openstack_compute_flavor_v2.m2-large-ssd.id

  server_group_id    = module.k8s_prod-1.master_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys

  loadbalancer_pool_id = module.k8s_prod-1.loadbalancer_pool_id
}

module "k8s_prod-1-old-worker-nodes" {
  for_each = toset(concat(["0"], [for i in concat([0], range(3, 7), [8], range(10, 13)) : tostring(i)]))
  source   = "./k8s-worker-node"

  cluster_name              = "prod-1"
  node_id                   = tonumber(each.key)
  node_registration_command = module.k8s_prod-1.node_registration_command

  network_id = module.net_k8s-prod-1.network_id
  subnet_id  = module.net_k8s-prod-1.subnets[0].id

  secgroup_id   = module.k8s_prod-1.secgroup_id
  secgroup_name = module.k8s_prod-1.secgroup_name

  cluster_cidr             = module.k8s_prod-1.cluster_cidr
  service_cluster_ip_range = module.k8s_prod-1.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m1-xlarge.id


  server_group_id    = module.k8s_prod-1.worker_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys
}

module "k8s_prod-1-worker-newnodes-ssd" {
  for_each = toset(["1", "2", "7"])
  source   = "./k8s-worker-node"

  cluster_name              = "prod-1"
  node_id                   = tonumber(each.key)
  node_registration_command = module.k8s_prod-1.node_registration_command
  node_labels = {
    "forge-disktype" = "localssd"
  }

  network_id = module.net_k8s-prod-1.network_id
  subnet_id  = module.net_k8s-prod-1.subnets[0].id

  secgroup_id   = module.k8s_prod-1.secgroup_id
  secgroup_name = module.k8s_prod-1.secgroup_name

  cluster_cidr             = module.k8s_prod-1.cluster_cidr
  service_cluster_ip_range = module.k8s_prod-1.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.m1-2xlarge-ssd.id


  server_group_id    = module.k8s_prod-1.worker_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys
}

module "k8s_prod-1-worker-nodes-ssd" {
  for_each = toset(["15", "16"])
  source   = "./k8s-worker-node"

  cluster_name              = "prod-1"
  node_id                   = tonumber(each.key)
  node_registration_command = module.k8s_prod-1.node_registration_command
  node_labels = {
    "forge-disktype" = "localssd"
  }

  network_id = module.net_k8s-prod-1.network_id
  subnet_id  = module.net_k8s-prod-1.subnets[0].id

  secgroup_id   = module.k8s_prod-1.secgroup_id
  secgroup_name = module.k8s_prod-1.secgroup_name

  cluster_cidr             = module.k8s_prod-1.cluster_cidr
  service_cluster_ip_range = module.k8s_prod-1.service_cluster_ip_range

  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.huge-ssd.id


  server_group_id    = module.k8s_prod-1.worker_server_group_id
  user_data_ssh_keys = local.user_data_ssh_keys
}


resource "openstack_networking_floatingip_v2" "prod-1-svc" {
  # This is some nasty IP squatting so we can reuse those for this or other
  # clusters.
  for_each = toset([
    "91.243.117.180",
    "91.243.117.181",
    "91.243.117.182",
    "91.243.117.183",
    "91.243.117.184",
    "91.243.117.185",
    "91.243.117.186",
    "91.243.117.187",
    "91.243.117.188",
    "91.243.117.189",
  ])
  pool    = data.openstack_networking_network_v2.ext-net.name
  address = each.value
}
resource "openstack_dns_recordset_v2" "ingress_prod-1_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "ingress.prod-1.k8s.cri.epita.fr."
  type    = "A"
  records = ["91.243.117.180"]
}
resource "openstack_dns_recordset_v2" "ingress-pie_prod-1_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "ingress-pie.prod-1.k8s.cri.epita.fr."
  type    = "A"
  records = ["10.201.5.80"]
}
resource "openstack_dns_recordset_v2" "wild_prod-1_k8s_cri_epita_fr" {
  zone_id = module.dns_k8s_cri_epita_fr.zone_id
  name    = "*.prod-1.k8s.cri.epita.fr."
  type    = "CNAME"
  records = ["ingress.prod-1.k8s.cri.epita.fr."]
}

resource "openstack_networking_floatingip_v2" "prod-1-svc-new-pie" {
  for_each = toset([
    "10.201.5.45",
    "10.201.5.50",
    "10.201.5.53",
    "10.201.5.54",
    "10.201.5.69",
    "10.201.5.80",
    "10.201.5.89",
    "10.201.5.122",
    "10.201.5.123",
  ])
  pool    = data.openstack_networking_network_v2.pie-net.name
  address = each.value
}

resource "rancher2_cluster_role_template_binding" "labsi-roots-projects-view" {
  name               = "labsi-roots-projects-view"
  cluster_id         = module.k8s_prod-1.cluster_id
  role_template_id   = "projects-view"
  group_principal_id = "openldap_group://cn=labo-labsi-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}


resource "rancher2_project" "prod-1-forge-dev" {
  name       = "forge-dev"
  cluster_id = module.k8s_prod-1.cluster_id
}

resource "rancher2_project" "prod-1-forge" {
  name       = "forge"
  cluster_id = module.k8s_prod-1.cluster_id
}

resource "rancher2_project_role_template_binding" "labsi-roots-workload" {
  name               = "labsi-roots-workload"
  project_id         = rancher2_project.prod-1-forge-dev.id
  role_template_id   = "workloads-manage"
  group_principal_id = "openldap_group://cn=labo-labsi-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_project_role_template_binding" "labsi-roots-forge-read" {
  name               = "labsi-roots-forge-read"
  project_id         = rancher2_project.prod-1-forge.id
  role_template_id   = "workloads-view"
  group_principal_id = "openldap_group://cn=labo-labsi-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_role_template" "port-forward" {
  name        = "create-port-forward"
  context     = "project"
  description = "Role to allow creation of port-foward to pods"
  rules {
    api_groups = ["*"]
    resources  = ["pods/portforward"]
    verbs      = ["*"]
  }
}

resource "rancher2_project_role_template_binding" "labsi-roots-forge-portforward" {
  name               = "labsi-roots-forge-portforward"
  project_id         = rancher2_project.prod-1-forge.id
  role_template_id   = rancher2_role_template.port-forward.id
  group_principal_id = "openldap_group://cn=labo-labsi-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_namespace" "forge-dev" {
  for_each = toset([
    "forge-dev-default", "git-server", "forge-intranet", "postgres-forge-dev",
    "kafka-forge-dev", "forge-dev-pgadmin", "forge-special-projects"
  ])

  name       = each.key
  project_id = rancher2_project.prod-1-forge-dev.id
}


# METALAB

resource "rancher2_project" "prod-1-metalab" {
  name       = "metalab"
  cluster_id = module.k8s_prod-1.cluster_id
}

resource "rancher2_cluster_role_template_binding" "metalab-roots-projects-view" {
  name               = "metalab-roots-projects-view"
  cluster_id         = module.k8s_prod-1.cluster_id
  role_template_id   = "projects-view"
  group_principal_id = "openldap_group://cn=labo-metalab-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

resource "rancher2_project_role_template_binding" "metalab-project-metalab-roots" {
  for_each = toset(["workloads-view", "workloads-manage", "secrets-view"])

  name               = "metalab-roots-${each.key}"
  project_id         = rancher2_project.prod-1-metalab.id
  role_template_id   = each.key
  group_principal_id = "openldap_group://cn=labo-metalab-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}

# postgres access
resource "rancher2_project_role_template_binding" "metalab-roots-forge-read" {
  name               = "metalab-roots-forge-read"
  project_id         = rancher2_project.prod-1-forge.id
  role_template_id   = "workloads-view"
  group_principal_id = "openldap_group://cn=labo-metalab-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}
resource "rancher2_project_role_template_binding" "metalab-roots-forge-portforward" {
  name               = "metalab-roots-forge-portforward"
  project_id         = rancher2_project.prod-1-forge.id
  role_template_id   = rancher2_role_template.port-forward.id
  group_principal_id = "openldap_group://cn=labo-metalab-roots,ou=groups,dc=cri,dc=epita,dc=fr"
}
