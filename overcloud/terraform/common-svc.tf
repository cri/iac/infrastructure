module "common-svc-net" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "common-svc-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.103.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.103.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}

// PIE access
resource "openstack_networking_port_v2" "pie-router-0_common-svc" {
  network_id            = module.common-svc-net.network_id
  port_security_enabled = false
  fixed_ip {
    subnet_id  = module.common-svc-net.subnets[0].id
    ip_address = "192.168.103.252"
  }
}
resource "openstack_networking_router_interface_v2" "pie-router-0_common-svc" {
  router_id = data.openstack_networking_router_v2.pie[0].id
  port_id   = openstack_networking_port_v2.pie-router-0_common-svc.id
}
resource "openstack_networking_subnet_route_v2" "pie-router-0_common-svc" {
  for_each         = toset(local.pie-subnets)
  subnet_id        = module.common-svc-net.subnets[0].id
  destination_cidr = each.value
  next_hop         = openstack_networking_port_v2.pie-router-0_common-svc.fixed_ip[0].ip_address
}

resource "openstack_compute_instance_v2" "clone-store" {
  count     = 1
  name      = "clone-store-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.clone-store.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-nfs.security_group_name,
  ]

  network {
    uuid = module.common-svc-net.network_id
  }

  power_state = "shutoff"

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}
resource "openstack_blockstorage_volume_v3" "clone-store" {
  count = length(openstack_compute_instance_v2.clone-store)
  name  = "clone-store-${count.index}"
  size  = 500
}
resource "openstack_compute_volume_attach_v2" "clone-store" {
  count       = length(openstack_compute_instance_v2.clone-store)
  instance_id = openstack_compute_instance_v2.clone-store[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.clone-store[count.index].id
}
resource "openstack_networking_floatingip_v2" "clone-store_pie" {
  pool    = data.openstack_networking_network_v2.pie-net.name
  address = "10.201.5.111"
}
resource "openstack_compute_floatingip_associate_v2" "clone-store_pie" {
  floating_ip = openstack_networking_floatingip_v2.clone-store_pie.address
  instance_id = openstack_compute_instance_v2.clone-store[0].id
}

resource "openstack_compute_instance_v2" "ssh_gate" {
  name      = "ssh-gate"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m1-small.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-common-svc_router.name,
    openstack_networking_secgroup_v2.CRI_egress-broadcast.name,
    openstack_networking_secgroup_v2.CRI_egress-external.name,
    openstack_networking_secgroup_v2.CRI_egress-pie.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
  ]

  network {
    uuid = module.common-svc-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}
resource "openstack_dns_recordset_v2" "ssh_cri_epita_fr" {
  zone_id = module.dns_cri_epita_fr.zone_id
  name    = "ssh.cri.epita.fr."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas_cri_epita_fr.name]
}
resource "openstack_lb_listener_v2" "CRI_admin-svc-ssh" {
  loadbalancer_id        = openstack_lb_loadbalancer_v2.CRI_admin-svc.id
  protocol               = "TCP"
  protocol_port          = 22
  timeout_client_data    = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
  timeout_member_connect = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
  timeout_member_data    = 1000 * 60 * 60 * 4 # 2 hours, 4 minutes
}
resource "openstack_lb_pool_v2" "CRI_admin-svc-ssh" {
  listener_id = openstack_lb_listener_v2.CRI_admin-svc-ssh.id
  protocol    = "TCP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "CRI_admin-svc-ssh" {
  pool_id = openstack_lb_pool_v2.CRI_admin-svc-ssh.id

  member {
    address       = openstack_compute_instance_v2.ssh_gate.network[0].fixed_ip_v4
    protocol_port = 22
  }
}


resource "openstack_compute_instance_v2" "pieboot" {
  count     = 1
  name      = "pieboot-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.m2-small.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-dhcp.security_group_name,
    module.sg_ingress-tftp.security_group_name,
  ]

  network {
    uuid = module.common-svc-net.network_id
  }

  network {
    uuid        = data.openstack_networking_network_v2.pie-net.id
    fixed_ip_v4 = "10.201.5.67"
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_compute_instance_v2" "rt" {
  count     = 1
  name      = "rt-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-22-04-20221120.id
  flavor_id = data.openstack_compute_flavor_v2.m2-medium.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-http-https.security_group_name,
    module.sg_ingress-smtp-mail-legacy.security_group_name,
  ]

  network {
    uuid = module.common-svc-net.network_id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

