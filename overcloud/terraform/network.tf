locals {
  legacy-pie-subnets = [
    "10.2.0.0/16",
    "10.3.0.0/16",
    "10.41.0.0/16",
    "10.57.0.0/16",
    "10.223.0.0/16",
    "10.224.0.0/16",
    "10.233.0.0/16",
    "10.224.4.16/28",
    "10.224.4.208/32",
  ]
  pie-subnets = [
    "10.201.0.0/16",
    "10.202.0.0/16",
  ]
}

data "openstack_networking_network_v2" "ext-net" {
  name = "ext-net"
}

data "openstack_networking_router_v2" "external-router" {
  count     = 2
  name      = "external-router-${count.index}"
  tenant_id = data.openstack_identity_project_v3.CRI.id
}

data "openstack_networking_network_v2" "legacy-pie-net" {
  name = "legacy-pie-net"
}

data "openstack_networking_router_v2" "legacy-pie" {
  count     = 1
  name      = "legacy-pie-${count.index}"
  tenant_id = data.openstack_identity_project_v3.CRI.id
}

data "openstack_networking_network_v2" "pie-net" {
  name = "pie-net"
}

data "openstack_networking_router_v2" "pie" {
  count     = 1
  name      = "pie-${count.index}"
  tenant_id = data.openstack_identity_project_v3.CRI.id
}

resource "openstack_networking_network_v2" "cri-routers" {
  name           = "cri-routers-net"
  admin_state_up = true

  segments {
    physical_network = "physnet5"
    network_type     = "flat"
  }
}
resource "openstack_networking_subnet_v2" "cri-routers" {
  name            = "cri-routers-subnet"
  network_id      = openstack_networking_network_v2.cri-routers.id
  cidr            = "192.168.254.0/24"
  ip_version      = 4
  no_gateway      = true
  enable_dhcp     = false
  dns_nameservers = ["192.168.254.1"]
}
resource "openstack_networking_port_v2" "external-router_cri-routers" {
  network_id            = openstack_networking_network_v2.cri-routers.id
  port_security_enabled = false
  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.cri-routers.id
    ip_address = "192.168.254.10"
  }
}
resource "openstack_networking_router_interface_v2" "external-router_cri-routers" {
  router_id = data.openstack_networking_router_v2.external-router[0].id
  port_id   = openstack_networking_port_v2.external-router_cri-routers.id
}
resource "openstack_networking_router_route_v2" "external-router_cri-routers" {
  router_id        = data.openstack_networking_router_v2.external-router[0].id
  destination_cidr = "192.168.128.0/17"
  next_hop         = "192.168.254.240"
}
