terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.48.0"
    }
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.24.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.8.2"
    }
  }

  backend "s3" {
    region = "default"
    bucket = "terraform"
    key    = "tfstate.tf"
    endpoints = {
      s3 = "https://s3.cri.epita.fr"
    }
    force_path_style            = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_s3_checksum            = true
  }
}

provider "openstack" {
  # this uses the clouds.yaml file
  cloud       = "openstack"
  use_octavia = true
}
provider "openstack" {
  alias     = "rancher"
  cloud     = ""
  auth_url  = "https://openstack.cri.epita.fr:5000"
  region    = "RegionOne"
  tenant_id = data.openstack_identity_project_v3.CRI.id
  user_name = "rancher"
  password  = data.vault_generic_secret.k8s-undercloud_rancher_admin.data.password
}

data "vault_generic_secret" "k8s-undercloud_rancher_admin" {
  path = "k8s-undercloud/rancher/admin"
}
provider "rancher2" {
  api_url    = "https://rancher.undercloud.cri.epita.fr"
  access_key = data.vault_generic_secret.k8s-undercloud_rancher_admin.data.accesskey
  secret_key = data.vault_generic_secret.k8s-undercloud_rancher_admin.data.secretkey
}

provider "vault" {}

data "openstack_identity_project_v3" "CRI" {
  name = "CRI"
}
