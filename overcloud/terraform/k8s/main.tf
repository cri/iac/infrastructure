module "secgroup_k8s" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "${var.cluster_name} - Kubernetes"
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "tcp"
      port_range_min = 22
      port_range_max = 22
    },
    {
      remote_group_id = "@self"
    },
    # Necessary for nodePorts and OpenStack managed load balancers
    {
      protocol       = "tcp"
      port_range_min = 30000
      port_range_max = 32768
    },
    {
      protocol       = "udp"
      port_range_min = 30000
      port_range_max = 32768
    },
  ]

  egress_rules_ipv4 = [{ remote_ip_prefix = "0.0.0.0/0" }]
  egress_rules_ipv6 = [{ remote_ip_prefix = "::/0" }]
}

module "secgroup_k8s-master" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "${var.cluster_name} - Kubernetes Master"
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "tcp"
      port_range_min = 6443
      port_range_max = 6443
    },
  ]
}

resource "openstack_compute_servergroup_v2" "k8s-master" {
  name     = "${var.cluster_name}-k8s-master"
  policies = [var.masters_affinity]
}

resource "openstack_compute_servergroup_v2" "k8s-node" {
  name     = "${var.cluster_name}-k8s-node"
  policies = [var.nodes_affinity]
}
