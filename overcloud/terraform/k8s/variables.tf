variable "cluster_name" {
  description = "Name of the cluster"
  type        = string
}
variable "cluster_fqdn_api" {
  description = "FQDN address where the cluster API can be reached"
  type        = string
  default     = ""
}

variable "masters_affinity" {
  type    = string
  default = "soft-anti-affinity"
}
variable "nodes_affinity" {
  type    = string
  default = "soft-anti-affinity"
}

variable "kubernetes_version" {
  type    = string
  default = ""
}

variable "subnet_id" {
  description = "UUID of the subnet to use"
}

variable "cluster_cidr" {
  type    = string
  default = "172.20.0.0/16"
}
variable "service_cluster_ip_range" {
  type    = string
  default = "172.21.0.0/16"
}
variable "cluster_dns_server" {
  type    = string
  default = "172.21.0.10"
}


variable "loadbalancer_id" {
  type    = string
  default = ""
}
variable "loadbalancer_port" {
  type    = number
  default = 0
}

variable "openstack_auth_url" {
  type    = string
  default = "https://openstack.cri.epita.fr:5000"
}
variable "openstack_application_credential_id" {
  type    = string
  default = ""
}
variable "openstack_application_credential_secret" {
  type    = string
  default = ""
}

variable "argocd_rancher_cluster_id" {
  type        = string
  default     = ""
  description = "ID of the cluster where the ArgoCD cluster secret will be created."
}

variable "argocd_cluster_extra_labels" {
  type    = map(string)
  default = {}
}
