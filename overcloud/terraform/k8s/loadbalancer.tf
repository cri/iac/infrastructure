resource "openstack_lb_listener_v2" "this" {
  count = var.loadbalancer_id != "" ? 1 : 0

  loadbalancer_id = var.loadbalancer_id
  protocol        = "HTTPS"
  protocol_port   = var.loadbalancer_port
}
resource "openstack_lb_pool_v2" "this" {
  count = var.loadbalancer_id != "" ? 1 : 0

  listener_id = openstack_lb_listener_v2.this[0].id
  protocol    = "HTTPS"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
