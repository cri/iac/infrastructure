output "cluster_fqdn_api" {
  value = var.cluster_fqdn_api
}
output "cluster_id" {
  value = rancher2_cluster.this.id
}
output "node_registration_command" {
  value = rancher2_cluster.this.cluster_registration_token.0.node_command
}
output "secgroup_id" {
  value = module.secgroup_k8s.security_group_id
}
output "secgroup_master_id" {
  value = module.secgroup_k8s-master.security_group_id
}
output "secgroup_name" {
  value = module.secgroup_k8s.security_group_name
}
output "secgroup_master_name" {
  value = module.secgroup_k8s-master.security_group_name
}
output "loadbalancer_pool_id" {
  value = openstack_lb_pool_v2.this[0].id
}
output "cluster_cidr" {
  value = var.cluster_cidr
}
output "service_cluster_ip_range" {
  value = var.service_cluster_ip_range
}

output "master_server_group_id" {
  value = openstack_compute_servergroup_v2.k8s-master.id
}
output "worker_server_group_id" {
  value = openstack_compute_servergroup_v2.k8s-node.id
}
