# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:VSnd9ZIPyfKHOObuQCaKfnjIHRtR7qTw19Rz8tJxm+k=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.8.2"
  constraints = "3.8.2"
  hashes = [
    "h1:2ve7G+YXMIUiNSH+J7daqU2Jg6WOvaOfsUfllXFwOOQ=",
    "h1:5wenWB9fcfhGd/qB1G3TfXdPCiRpYJdcAughrV6qrB4=",
    "h1:86BIloLhu//b81kvK8YeK8nzeIXPBT0TGGNR/xLfYkw=",
    "h1:CAYmBNuqkUuBUI/9g/DxoZUA+FvlfeN91fYB48/li2k=",
    "h1:EzqgPMt20qzRlnX2DFTzyQuVeYCwZWB3Vfr9q5Pra0s=",
    "h1:FPcuQQlgeX47Zb6TFWak3pLNi9Hm842i5cH8iHlSgB0=",
    "h1:IG6KwVl/0XAhyObx0wbHV3zgrj1FL4nFp5RuI0dNGSs=",
    "h1:O92oyLi7wgyec/lbODY3oPUwahhns0FOBTaSMrJ9KRE=",
    "h1:Z38mV+mWZPqckGtwCQS6P6Rdp1gfSVYnQfoC5trWF/I=",
    "h1:dg/MF44OMJjwRL13YhwM3nFuZJPn0R4MsR3SjiReGNQ=",
    "h1:v49zBYXmqCit5CZ1tIExBka0lKTMD1paMX0FdmpTv1o=",
    "zh:3dd0f4f12f5a479941422bc413ea147a76253c9d1bdb8dd2d098146c80f90aa5",
    "zh:4132382680ec77dda4713fd4701cbc7dcc08ef4742fb997961c3332c30b0ae12",
    "zh:56eb6b44bdbaf2f3f37a1df35c01405af5cf6eea988f3e6441e4d70391067918",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7e5f78948af1118a870d1caeabaff6dd72ad3f17c08c7ce986eab0aab9ba5694",
    "zh:8648ec617934ee880ce453011e9d8e2070a6db1b34ce11c007511f9399624d98",
    "zh:b2ded1dc6fd8c63dadd160a3360fb717f6808b5ad058be2af162be170aaefc3a",
    "zh:b88850e96c489dc8b5c66682bd166d6bb4a02cd6e943ba1d411cab911efd9487",
    "zh:c22c108bd60fd1af3b6fbea65018b069f731c95b1fb3900a052cef2b7fe2341f",
    "zh:c834ee80617c08f670826c8e566fcb01b30986ce996fbfc6fc3e9d838ced4d5f",
    "zh:cd4e6ecf2925915ad83cbdb962f8af65a70aebf5811fafe3f16cba69c1b39a6b",
    "zh:f6d39c4d3861ff682969a1fe4b960b7a27eda49c5a9747f20e24c56f3817a4cd",
  ]
}

provider "registry.terraform.io/rancher/rancher2" {
  version     = "1.24.1"
  constraints = "1.24.1"
  hashes = [
    "h1:/oX4BWSv8n8CNXQefB73HnORQwsbFgCs7IxTkmCjgaQ=",
    "h1:0gtEKjy61mTOIEg6BC62eW8j/qP0JN7oL3UFVlLnHLA=",
    "h1:0mSCrqB2apfycm/9t9acFkXswM94osB1qUH5H3jFZqc=",
    "h1:2QFsbb8O1vPkhnMyh9Oz9/uBMTmsm3SYYgs7vuIXEYA=",
    "h1:4sRp5vLS4wP6axxj5ukGG3ok+K7xBj006aivwnB/hKY=",
    "h1:5Ia6R2i6blH3XkIjjixsmfyhKqTVdss27M82F90rqXY=",
    "h1:OfKUpb1hChdO+CluyNKjmrQimFJAJAA+s96Pp6vK6FQ=",
    "h1:Y/Tc0fkPcFKsk9T4UmUIrOxNR9V52R7N6wSM4BC3lds=",
    "h1:gOB2xbbzgOjP+b/TIdcF4CbCIpFbeKT8ZTWw2atlTjk=",
    "h1:jr62htInrewk+BoBENEUfYjx/3sJ+AHpjx9OIItIk9U=",
    "h1:oRSc+qTqd937PoqmCx8ENgHQf7U6wGis1Nkab0VOX/s=",
    "h1:sF9enCnztLnjp9Wcvk1f78OMTLp2RDMb+5QqAWaZGqE=",
    "zh:019607a142db3cb24c40837dc8a65e93a86b56bf4f8f8ebedf6433a8108b8a31",
    "zh:0bad17fb2bdc730102a056b3ca63fb9d890d2f85b47316620fc4f750fdbef078",
    "zh:46e73b838be94c24a46dba08461a7b955b03d32f050b7ba819e8f2bb5af1fe5f",
    "zh:574217a569ae4de201d43c95e09cdf5e22ea259c45b38a3b9d6247eac1ad3abf",
    "zh:591488b3bd1016ef9ce51b03fe08c4a2954d16be15157a4c075e833dc1b6e248",
    "zh:6657904ec02e17518bc712147fa618d84a1a9b3b3a275c3e112072f2dec12a16",
    "zh:6caf485186f466a485c58b36216f8cc39e3d4f08945b8ea9835158cfb0a73398",
    "zh:6e1cacae4ca25e2ad442a1ca12180a8226314c46c4d1c23871ff54e484392048",
    "zh:7b5dc8f2268dab0ad8ee38513462835b1269f5848fdd96d8e8f20a4afeb5509b",
    "zh:9ed3c38771c6a7232c378553fbb2b2d66dece982f8898fc91e2d6c0ddac00a88",
    "zh:bb06e06be0de781b134f565986be04e8581cdd68c58af520c3304c84b29c2f35",
    "zh:fb7f8d24b8b1681e50d3ee4baf7080ff1bbf8c30891ef27afcb5d95fb610d9fe",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.48.0"
  constraints = "1.48.0"
  hashes = [
    "h1:0Oy4KDG/+l4tKeB+kYglsrFnghsAVJr60YTAbLkhfWc=",
    "h1:1OTZZtFI/HIdp052eDSmjZ29d/4YP0emMkH2VzQs/P0=",
    "h1:7dEcQtVStegPyOOXeiRDa/rNNt9NuEsnYdHtNWa9wIE=",
    "h1:GAqVDfjuKoz4m4GaVh6DT+5UpFrW1VEG1hsw77grnKA=",
    "h1:HNt4MXQAJTi3HRBEcnOekmOHPqCD5G8ay0H7wEhAzuY=",
    "h1:MBgfCkSbT7UOLFNFv2hAsKij3xIYzKn3KE9O5pk2FSU=",
    "h1:Q1+17/v0+xpNDUvEqVOX9UqYTwb1suAdH73ObnIVepc=",
    "h1:ZL8GxqBLb3qH4QwnuWBwSTlnrdD9Fn6BBN1fUqPCzms=",
    "h1:jwygqqFJu7kpCtXN+c1fe2X5TmVOK6fDBDXkepAzE+Y=",
    "h1:mTlCzugRpavDX3IG2zAs6ZainqpTUpNnQKbQuR523NA=",
    "h1:nyb1jDCT76ETHbju3j93NvunwnKa8ad3S3kREzozGys=",
    "h1:qjf/qyH9oKOMujQk59bNxV8yLRbUhmihxMRrKOeA8qI=",
    "h1:x7HYTUJE6dh7TU/UoBEW2M8O1TM2J9+4htgDWAg7rU8=",
    "h1:yXANFQGRIQN4uLs31bSaNBzPe8MfTiLVK52522A3PZo=",
    "zh:1fe237fa1153e05879fd26857416a1d029a3f108e32e83c4931dd874c777aa6a",
    "zh:2c4587b4c810d569aafd69e287ecc2ee910e6c16cfc784e49861e0a8066b8655",
    "zh:3f1a42fce3c925afeeaa96efae0bc9be95acfc80ba147a8123d03038d429df6b",
    "zh:430511b62dc2fdafa070e9bd88e5e1fc39b3d667151aa9bf8e21b2c2c5421281",
    "zh:4452279f6f23d3f2c5969deebf24ae2c38af8e02d52ee589b658c52b321835e5",
    "zh:5525d1ca817f28ec9f0f648ea38b94fd0741130eaed2260bbd734efd03aecfb8",
    "zh:675001e8cec8d0d4f006ce01b0608b7c5a378b4e56c6a27fbf5562f04371de70",
    "zh:6c0f4da6da81da562e16af6fbb36035c0797de2a0384d0ef7c9a8b4676f8eca9",
    "zh:79db708664ecbcf9d1a6d20e6a294716bff21a2641a8f58bfce60f3d11b944ef",
    "zh:7bfc5ee6765694779fbfc00954fe04795035e85dfefd916dc6601717116b7005",
    "zh:899a17c1547aa1bf732a55c903f3df25c8a0c107c16e0753677aecb8ed32130c",
    "zh:9e02fb5267dc415a763ef55a24f3890f7e63de8d61e05e220d90a5a4a4b891ed",
    "zh:a224e6e677e92cd31d0806a2d11c9bb17d032eaa0086e2aa8136ae0e9ce2fa83",
    "zh:b3905869f6fea27ffd144eb8221ea67aeca63e23c06af43a221e55634faef3e2",
  ]
}
