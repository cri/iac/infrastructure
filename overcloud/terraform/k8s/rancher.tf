resource "rancher2_cluster" "this" {
  name = var.cluster_name
  rke_config {
    kubernetes_version = var.kubernetes_version
    enable_cri_dockerd = true

    services {
      kube_api {
        service_cluster_ip_range = var.service_cluster_ip_range
      }
      kube_controller {
        cluster_cidr             = var.cluster_cidr
        service_cluster_ip_range = var.service_cluster_ip_range
        extra_binds = [
          "/lib/modules:/lib/modules:ro",
        ]
      }
      kubelet {
        cluster_dns_server = var.cluster_dns_server
      }
      etcd {
        backup_config {
          enabled = true
        }
      }
    }

    network {
      plugin = "calico"
    }

    dns {
      linear_autoscaler_params {
        cores_per_replica            = 0.34
        nodes_per_replica            = 4
        prevent_single_point_failure = true
        min                          = 2
        max                          = 4
      }
    }

    monitoring {
      provider = "metrics-server"
      replicas = 2
      update_strategy {
        rolling_update {
          max_unavailable = 1
        }
      }
    }

    authentication {
      sans = var.cluster_fqdn_api != "" ? [var.cluster_fqdn_api] : []
    }

    cloud_provider {
      name = "external"
    }

    ingress {
      provider = "none"
    }
  }

  cluster_auth_endpoint {
    enabled = var.cluster_fqdn_api != ""
    fqdn    = var.loadbalancer_port != 0 ? "${var.cluster_fqdn_api}:${var.loadbalancer_port}" : var.cluster_fqdn_api
  }
}


resource "rancher2_cluster_sync" "this" {
  cluster_id = rancher2_cluster.this.id
}

resource "rancher2_secret_v2" "this" {
  count      = var.openstack_application_credential_id != "" ? 1 : 0
  cluster_id = rancher2_cluster_sync.this.id
  name       = "cloud-config"
  namespace  = "kube-system"
  data = {
    "cloud.conf" = <<-EOC
      [Global]
      auth-url=${var.openstack_auth_url}
      application-credential-id=${var.openstack_application_credential_id}
      application-credential-secret=${var.openstack_application_credential_secret}

      [LoadBalancer]
      use-octavia=true
      subnet-id=${var.subnet_id}
      enable-ingress-hostname=true
    EOC
  }
}

resource "rancher2_token" "argocd" {
  count       = var.argocd_rancher_cluster_id != "" ? 1 : 0
  cluster_id  = rancher2_cluster_sync.this.id
  description = "${var.cluster_name} ArgoCD token"
}
resource "rancher2_secret_v2" "argocd" {
  count      = var.argocd_rancher_cluster_id != "" ? 1 : 0
  cluster_id = var.argocd_rancher_cluster_id
  name       = "${var.cluster_name}-secret"
  namespace  = "argocd"
  labels = merge({
    "argocd.argoproj.io/secret-type" = "cluster"
  }, var.argocd_cluster_extra_labels)
  data = {
    name   = var.cluster_name
    server = yamldecode(rancher2_cluster_sync.this.kube_config).clusters[0].cluster.server
    config = jsonencode({
      bearerToken = rancher2_token.argocd[0].token
    })
  }
}
