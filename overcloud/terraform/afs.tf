locals {
  afs_nb_db_servers = 3
  afs_nb_instances  = 8
}

module "net_afs" {
  source         = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "afs-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.107.0/24"
      router_id       = data.openstack_networking_router_v2.external-router[0].id
      gateway_ip      = "192.168.107.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.210"]
    },
  ]
}
// New PIE subnets access
resource "openstack_networking_subnet_route_v2" "legacy-pie-router-0_afs-new-subnets" {
  for_each         = toset(["10.14.0.0/16", "10.29.0.0/16"])
  subnet_id        = module.net_afs.subnets[0].id
  destination_cidr = each.value
  next_hop         = "192.168.107.254"
}

module "sg_ingress-afs" {
  source               = "git::https://gitlab.cri.epita.fr/forge/infra/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "AFS Ingress only"
  description          = <<EOT
    Only allow AFS Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [
    {
      protocol       = "udp"
      port_range_min = 7000
      port_range_max = 7010
    },
  ]
}

resource "openstack_compute_servergroup_v2" "afs" {
  name     = "afs"
  policies = ["soft-anti-affinity"]
}
resource "openstack_compute_instance_v2" "afs" {
  count     = local.afs_nb_instances
  name      = "afs-${count.index}"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m2-large.id
  user_data = local.user_data_ssh_keys
  security_groups = [
    openstack_networking_secgroup_v2.CRI_egress-all.name,
    openstack_networking_secgroup_v2.CRI_ingress-icmp.name,
    openstack_networking_secgroup_v2.CRI_ingress-ssh.name,
    module.sg_ingress-node-exporter.security_group_name,
    module.sg_ingress-afs.security_group_name,
  ]

  network {
    uuid        = data.openstack_networking_network_v2.legacy-pie-net.id
    fixed_ip_v4 = "10.224.21.${100 + count.index}"
  }

  network {
    uuid        = data.openstack_networking_network_v2.pie-net.id
    fixed_ip_v4 = "10.201.5.${100 + count.index}"
  }

  network {
    uuid = module.net_afs.network_id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.afs.id
  }

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }
}

resource "openstack_blockstorage_volume_v3" "afs-data" {
  count                = local.afs_nb_instances
  name                 = "afs-data-${count.index}"
  size                 = 1000
  enable_online_resize = true
}
resource "openstack_compute_volume_attach_v2" "afs-data" {
  count       = local.afs_nb_instances
  instance_id = openstack_compute_instance_v2.afs[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.afs-data[count.index].id
}

resource "openstack_dns_recordset_v2" "afs_pie_cri_epita_fr" {
  count   = local.afs_nb_instances
  zone_id = module.dns_pie_cri_epita_fr.zone_id
  name    = "afs-${count.index}.pie.cri.epita.fr."
  type    = "A"
  records = ["10.201.5.${100 + count.index}"]
}

resource "openstack_dns_recordset_v2" "srv-vlserver_cri_epita_fr" {
  zone_id = module.dns_cri_epita_fr.zone_id
  name    = "_afs3-vlserver._udp.cri.epita.fr."
  type    = "SRV"
  records = [for i in range(local.afs_nb_db_servers) : "10 10 7003 afs-${i}.pie.cri.epita.fr."]
}
resource "openstack_dns_recordset_v2" "srv-prserver_cri_epita_fr" {
  zone_id = module.dns_cri_epita_fr.zone_id
  name    = "_afs3-prserver._udp.cri.epita.fr."
  type    = "SRV"
  records = [for i in range(local.afs_nb_db_servers) : "10 10 7002 afs-${i}.pie.cri.epita.fr."]
}
