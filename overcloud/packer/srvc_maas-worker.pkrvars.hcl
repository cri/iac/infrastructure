name = "srvc-maas-worker"
version = "1.6.1"

source_image_name = "forge-base-v1.2.0"
networks_id = [
  "eae64742-49fd-4c1b-a10f-baf1d1c6e072" # admin-svc-net
]
security_groups = [
  "All Egress traffic",
  "All ICMP traffic",
  "SSH Ingress only"
]

ssh_bastion_host     = "bastion.iaas.cri.epita.fr."
ssh_bastion_port     = 2222
ssh_bastion_username = "root"
ansible_groups = [
  "srvc_maas"
]
