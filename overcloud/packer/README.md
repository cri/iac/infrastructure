# Packer

This folder contains Packer configuration for custom image building.

## Usage

This uses the credentials from the clouds.yaml file.

Init packer (download dependencies):

```sh
packer init .
```

Build images (specify the var file of the image you want to  build, for example
to build the forge-base image here):

```sh
packer build -var-file=base.pkrvars.hcl forge-template.pkr.hcl
```
