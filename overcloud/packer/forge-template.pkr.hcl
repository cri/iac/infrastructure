variable "name" {
  type = string

  description = <<-EOF
  Name of the image
  EOF
}

variable "version" {
  type = string

  description = <<-EOF
  Version of the final image.
  EOF

  default = "0.0.0" # Developement version

  validation {
    condition = can(regex(
      "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$",
      var.version
    ))
    error_message = <<-EOF
    The version value must be a semantic version, see https://semver.org/.
    EOF
  }
}

variable "source_image_name" {
  type = string

  description = <<-EOF
  Source image to use.
  EOF
}

variable "builder_flavor" {
  type    = string
  default = "m1.large"

  description = <<-EOF
  Builder VM openstack flavor.
  EOF
}

variable "networks_id" {
  type = set(string)

  description = <<-EOF
  List of networks that the builder must be connected.
  EOF

  validation {
    condition = !contains([
      for network_id in var.networks_id :
      can(regex(
        "^[a-zA-Z0-9_]{8}-[a-zA-Z0-9_]{4}-[a-zA-Z0-9_]{4}-[a-zA-Z0-9_]{4}-[a-zA-Z0-9_]{12}$",
        network_id
      ))
    ], false)

    error_message = <<-EOF
    `networks_id` must be a valid openstack ids.
    EOF
  }
}

variable "security_groups" {
  type = set(string)

  description = <<-EOF
  Names of the security groups to add into the host.
  EOF
}

variable "ssh_key_file" {
  type = string

  default = env("SSH_KEY_FILE")

  description = <<-EOF
  File path to the ssh key (use `SSH_KEY_FILE`). if not provided will use user
  agent.
  EOF
}

variable "ssh_bastion_host" {
  type = string

  description = <<-EOF
  SSH bastion host used to connect to builder instance
  EOF
}

variable "ssh_bastion_port" {
  type    = number
  default = 22

  description = <<-EOF
  SSH bastion port used to connect to builder instance
  EOF
}

variable "ssh_bastion_username" {
  type = string

  description = <<-EOF
  Bastion user use to connect into the builder host.
  EOF
}

packer {
  required_plugins {
    openstack = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/openstack"
    }
  }
  required_plugins {
    ansible = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/ansible"
    }
  }
}
variable "ansible_groups" {
  type    = list(string)
  default = []

  description = <<-EOF
  List of groups that the host must be added in the ansible hosts.
  EOF
}

locals {
  full_image_name = "${var.name}-v${var.version}"
}

source "openstack" "base" {
  image_name    = local.full_image_name
  instance_name = "packer-builder_${local.full_image_name}"

  source_image_name = var.source_image_name
  flavor            = var.builder_flavor
  networks          = var.networks_id
  security_groups   = var.security_groups


  ssh_username = "root"
  user_data    = <<-EOF
  #cloud-config

  disable_root: false
  users:
  - name: root
    shell: /bin/bash
  EOF

  ssh_bastion_host       = var.ssh_bastion_host
  ssh_bastion_port       = var.ssh_bastion_port
  ssh_bastion_username   = var.ssh_bastion_username
  ssh_bastion_agent_auth = var.ssh_key_file == ""
  ssh_private_key_file   = var.ssh_key_file

  ssh_timeout = "2m"
}

build {
  name = "ansible"

  sources = [
    "source.openstack.base"
  ]

  provisioner "ansible" {
    playbook_file       = "../playbooks/main.yml"
    galaxy_file         = "../../requirements.yml"
    inventory_directory = "../"
    groups              = var.ansible_groups
    user = "root"
    host_alias = var.name

    galaxy_force_install = true

    extra_arguments = [
      "--scp-extra-args", "'-O'"
    ]
  }
}
