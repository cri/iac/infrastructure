locals {
  meta_groups = [
    "cri-roots",
    "ing-assistants",
    "ing-assistants-acu",
    "ing-assistants-acu-respos-42sh",
    "ing-assistants-acu-respos-harmonisation",
    "ing-assistants-acu-respos-httpd",
    "ing-assistants-acu-respos-malloc",
    "ing-assistants-acu-respos-minimake",
    "ing-assistants-acu-respos-myfind",
    "ing-assistants-acu-respos-piscine",
    "ing-assistants-acu-respos-sql",
    "ing-assistants-deadlines",
    "ing-assistants-devs",
    "ing-assistants-director-42sh",
    "ing-assistants-director-asm",
    "ing-assistants-director-chess",
    "ing-assistants-director-cpp",
    "ing-assistants-director-harmonisation",
    "ing-assistants-director-harmonisation-mpc",
    "ing-assistants-director-harmonisation-prog",
    "ing-assistants-director-httpd",
    "ing-assistants-director-java",
    "ing-assistants-directors-js",
    "ing-assistants-director-malloc",
    "ing-assistants-director-minimake",
    "ing-assistants-director-myfind",
    "ing-assistants-director-piscine",
    "ing-assistants-director-prerentree",
    "ing-assistants-director-spider",
    "ing-assistants-director-sql",
    "ing-assistants-director-tiger",
    "ing-assistants-gitlab-manager",
    "ing-assistants-intendants",
    "ing-assistants-moulettes",
    "ing-assistants-reviews",
    "ing-assistants-roots",
    "ing-assistants-toolchain",
    "ing-assistants-yaka",
    "ing-assistants-yaka-respos-chess",
    "ing-assistants-yaka-respos-cpp",
    "ing-assistants-yaka-respos-java",
    "ing-assistants-yaka-respos-js",
    "ing-assistants-yaka-respos-ping",
    "ing-assistants-yaka-respos-prerentree",
    "ing-assistants-yaka-respos-recrutement",
    "ing-assistants-yaka-respos-spider",
    "ing-assistants-yaka-respos-tiger",
    "ing-assistants-yaka-stage",
    "labo-metalab-roots",
  ]
}

data "gitlab_group" "meta" {
  full_path = "meta"
}

resource "gitlab_group" "meta_group" {
  for_each                = toset(local.meta_groups)
  name                    = each.value
  path                    = each.value
  visibility_level        = "internal"
  request_access_enabled  = false
  share_with_group_lock   = true
  subgroup_creation_level = "owner"
  project_creation_level  = "noone"
  parent_id               = data.gitlab_group.meta.id
}

resource "gitlab_group_ldap_link" "meta_group" {
  for_each      = toset(local.meta_groups)
  group_id      = gitlab_group.meta_group[each.value].id
  cn            = each.value
  group_access  = "developer"
  ldap_provider = "ldapmain"
}
