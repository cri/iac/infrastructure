terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.20.0"
    }
  }

  backend "s3" {
    region = "default"
    bucket = "terraform"
    key    = "tfstate-gitlab.tf"
    endpoints = {
      s3 = "https://s3.cri.epita.fr"
    }
    force_path_style            = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_s3_checksum            = true
  }
}

provider "gitlab" {
  base_url = "https://gitlab.cri.epita.fr/api/v4/"
}
