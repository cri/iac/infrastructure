# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.20.0"
  constraints = "~> 3.20.0"
  hashes = [
    "h1:0uHMIG9XQVTu1nxBI4vM3YkhQuXMRQ1Bykz6dXAvcso=",
    "h1:925y5TcrbuQCBeakFpNgatykNFk/SEq8c0bUOde7SAs=",
    "h1:A4ZDxBPu4zxqTwQCplxnjJRKEvIpk8xfYFdy8nBYU8k=",
    "h1:CyiM5K48hjU0D/3n4eq0gdmSlfrfd5LtGxHGYYxe0Qc=",
    "h1:DCcBDmS0i0DWcWtzMCGEqgpaHmNkZz8IDLu0vWT9wzc=",
    "h1:IRICq1oKYStK4PRmGZM1knV/ibKRF4l1MVf2zV3svGY=",
    "h1:KCnjw82fy5ueEevrYmzBcPyRNcR5idRH+f3ahkj6CbU=",
    "h1:M4rhXX62EcEIhuwzBX5mosAZIA61ZsLTVTnVt/+hWvg=",
    "h1:PE3M1Pg3smBCmBw08uGOLCE66vuWgmUdInm+ZwzeI/c=",
    "h1:VJVvmCdrwox96aNLa1eHmjHaZb2rgDOd5kuv/zqM6Fo=",
    "h1:k+gqfpk1mtdla8SBtFt14TR99wgyrHyNXH7Oj69kok0=",
    "h1:mKQm/bhozqpnxfUh1QOK3rjNQT5DHi2p/YIIGwLKM9Y=",
    "h1:qwCy1tAYa09il83mWDobliG+95fYrRvAiDDeFLIzBkQ=",
    "h1:v88KggmUBIfQ1etwsvIEkOToeHHuaTrmx5qeg6/Vt3A=",
    "zh:0428e9904b2fabf2001bdf83ac68af8287c0c790278ca0a8ed0c895db103af9f",
    "zh:0dd6a2d5bf3d33047da5d2707e394be01305e1215d5751ecfb4d06c97c4ac247",
    "zh:10b5f8de90114222a0576fb8a783d0a7e03b4dbb2922801c18ea83fd9b6cf287",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:1f75b2eed2939c38c444563e22566ba68e4da380f30c851179f1f38e9da7d9f4",
    "zh:2acab992d2f34f09212049a1b524264494d8800b17ff571bbdf5fb71e911f188",
    "zh:3d22e4fcf7b06871df142c0a8e534fe97072ebea58fb0dac5b1f208f09a6e780",
    "zh:44b780ea0b406e6077dc3141290003fa23c6ece63f856cd1bbff7f4256fabd83",
    "zh:5ea69f1651f5221def3d0a8ca5155ecef1f9acc2c5b6f4d25a555cf6c4e453ea",
    "zh:68ee5db31c5d926019b05d48ed33b5fd7288c2d0f2a4d5bb2c529ba2149e9442",
    "zh:6b281f22784ccbec4804565309d76a66bbf82a6d601d8614d610227c8bb39422",
    "zh:6de6d0cf793403f93fe1a8d2009485da99d097661b5c4c6b4f09a4386e2dfece",
    "zh:a3a4d659e186f9e64bb1e40fe01f218975221cd480c0851a38d2e7c7f6d40ffa",
    "zh:b16736c9752c9004bec0dadc08df17cb5afc564acea9b4b8452cf1da4dba5373",
    "zh:d23758c22b8ce88a075a1f9639e936630b48929b1b426e53c8868dc87b2b5b36",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.48.0"
  constraints = "~> 1.48.0"
  hashes = [
    "h1:0Oy4KDG/+l4tKeB+kYglsrFnghsAVJr60YTAbLkhfWc=",
    "h1:1OTZZtFI/HIdp052eDSmjZ29d/4YP0emMkH2VzQs/P0=",
    "h1:7dEcQtVStegPyOOXeiRDa/rNNt9NuEsnYdHtNWa9wIE=",
    "h1:GAqVDfjuKoz4m4GaVh6DT+5UpFrW1VEG1hsw77grnKA=",
    "h1:HNt4MXQAJTi3HRBEcnOekmOHPqCD5G8ay0H7wEhAzuY=",
    "h1:MBgfCkSbT7UOLFNFv2hAsKij3xIYzKn3KE9O5pk2FSU=",
    "h1:Q1+17/v0+xpNDUvEqVOX9UqYTwb1suAdH73ObnIVepc=",
    "h1:ZL8GxqBLb3qH4QwnuWBwSTlnrdD9Fn6BBN1fUqPCzms=",
    "h1:jwygqqFJu7kpCtXN+c1fe2X5TmVOK6fDBDXkepAzE+Y=",
    "h1:mTlCzugRpavDX3IG2zAs6ZainqpTUpNnQKbQuR523NA=",
    "h1:nyb1jDCT76ETHbju3j93NvunwnKa8ad3S3kREzozGys=",
    "h1:qjf/qyH9oKOMujQk59bNxV8yLRbUhmihxMRrKOeA8qI=",
    "h1:x7HYTUJE6dh7TU/UoBEW2M8O1TM2J9+4htgDWAg7rU8=",
    "h1:yXANFQGRIQN4uLs31bSaNBzPe8MfTiLVK52522A3PZo=",
  ]
}
