# Overcloud

This folder contains deployment files for services installed in our OpenStack
private cloud. More specifically, this contains Terraform configuration and
Ansible variables and playbooks.

## Getting started

To get started, you need to get your `clouds.yaml` file from OpenStack. You can
find it on Horizon (OpenStack's dashboard). Fill the file with your credentials
or with an application credential you created. Place the file at the root of the
`overcloud` folder. Be very careful about not leaking/commiting this file. It
is part of the gitignore of the repo.

## Terraform

Before running Terraform commands, you need to get S3 credentials for the
backend.

Run the following to create EC2 credential with your OpenStack account:

```sh
openstack ec2 credentials create
```

Source the following script to load your credentials in your environment
variables. Running this populates the `AWS_ACCESS_KEY_ID` and
`AWS_SECRET_ACCESS_KEY` variables.

```sh
source ./get_terraform_credentials.sh
```

Be careful about not leaking these variables in your environment. When you are
done using Terraform, run the following:

```sh
unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
```

## Ansible

Running Ansible is done with the following:

```sh
ansible-playbook playbooks/main.yml
```

The following options are relevant:
- `-l GROUPS_or_HOST`: limit the playbook to the specified host or group of
    hosts
- `-t TAGS`: specify a list of tags to target specific parts of playbooks
- `-D`: show diff
- `-C`: run in check mode, no change is applied
