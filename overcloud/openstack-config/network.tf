resource "openstack_networking_network_v2" "ext-net" {
  name           = "ext-net"
  admin_state_up = true

  segments {
    physical_network = "physnet1"
    network_type     = "flat"
  }
}

resource "openstack_networking_rbac_policy_v2" "CRI_ext-net_external" {
  action        = "access_as_external"
  object_id     = openstack_networking_network_v2.ext-net.id
  object_type   = "network"
  target_tenant = data.openstack_identity_project_v3.CRI.id
}

resource "openstack_networking_subnet_v2" "ext-subnet" {
  name        = "ext-subnet"
  network_id  = openstack_networking_network_v2.ext-net.id
  cidr        = "91.243.117.128/26"
  ip_version  = 4
  gateway_ip  = "91.243.117.190"
  enable_dhcp = false
}

resource "openstack_networking_router_v2" "external-router" {
  count               = 2
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}

resource "openstack_networking_network_v2" "legacy-pie-net" {
  name           = "legacy-pie-net"
  admin_state_up = true

  segments {
    physical_network = "physnet4"
    network_type     = "flat"
  }
}

resource "openstack_networking_rbac_policy_v2" "CRI_legacy-pie-net_external" {
  action        = "access_as_external"
  object_id     = openstack_networking_network_v2.legacy-pie-net.id
  object_type   = "network"
  target_tenant = data.openstack_identity_project_v3.CRI.id
}

resource "openstack_networking_subnet_v2" "legacy-pie-subnet" {
  name            = "legacy-pie-subnet"
  network_id      = openstack_networking_network_v2.legacy-pie-net.id
  cidr            = "10.224.21.0/24"
  ip_version      = 4
  no_gateway      = true
  enable_dhcp     = true
  dns_nameservers = ["91.243.117.210"]
}
resource "openstack_networking_subnet_route_v2" "legacy-pie" {
  for_each = toset([
    "10.2.0.0/16",
    "10.3.0.0/16",
    "10.41.0.0/16",
    "10.57.0.0/16",
    "10.223.0.0/16",
    "10.224.0.0/16",
    "10.233.0.0/16",
    "10.224.4.16/28",
    "10.224.4.208/32",
  ])
  subnet_id        = openstack_networking_subnet_v2.legacy-pie-subnet.id
  destination_cidr = each.key
  next_hop         = "10.224.21.254"
}

resource "openstack_networking_router_v2" "legacy-pie-router" {
  count               = 1
  name                = "legacy-pie-${count.index}"
  admin_state_up      = true
  external_network_id = openstack_networking_network_v2.legacy-pie-net.id
  enable_snat         = true
}

resource "openstack_networking_network_v2" "pie-net" {
  name           = "pie-net"
  admin_state_up = true

  segments {
    physical_network = "physnet6"
    network_type     = "flat"
  }
}

resource "openstack_networking_rbac_policy_v2" "CRI_pie-net_external" {
  action        = "access_as_external"
  object_id     = openstack_networking_network_v2.pie-net.id
  object_type   = "network"
  target_tenant = data.openstack_identity_project_v3.CRI.id
}

resource "openstack_networking_subnet_v2" "pie-subnet" {
  name            = "pie-subnet"
  network_id      = openstack_networking_network_v2.pie-net.id
  cidr            = "10.201.5.0/24"
  ip_version      = 4
  no_gateway      = true
  enable_dhcp     = true
  dns_nameservers = ["91.243.117.210"]
  allocation_pool {
    start = "10.201.5.10"
    end   = "10.201.5.254"
  }

}
resource "openstack_networking_subnet_route_v2" "pie" {
  for_each = toset([
    "10.201.0.0/16",
    "10.202.0.0/16",
  ])
  subnet_id        = openstack_networking_subnet_v2.pie-subnet.id
  destination_cidr = each.key
  next_hop         = "10.201.5.1"
}

resource "openstack_networking_router_v2" "pie-router" {
  count               = 1
  name                = "pie-${count.index}"
  admin_state_up      = true
  external_network_id = openstack_networking_network_v2.pie-net.id
  enable_snat         = true
}
resource "openstack_networking_router_route_v2" "pie-router" {
  count            = length(openstack_networking_router_v2.pie-router)
  router_id        = openstack_networking_router_v2.pie-router[count.index].id
  destination_cidr = "10.0.0.0/8"
  next_hop         = "10.201.5.1"
}
