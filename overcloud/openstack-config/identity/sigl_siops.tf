# Project
resource "openstack_identity_project_v3" "SIGL" {
  name      = "SIGL"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_ing-sigl-siops" {
  name      = "ing-sigl-siops"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}
data "openstack_identity_group_v3" "CRI_ing-sigl-openstack-member" {
  name      = "ing-sigl-openstack-member"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "SIGL_ing-sigl-siops_member" {
  project_id = openstack_identity_project_v3.SIGL.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-siops.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_ing-sigl-siops_load-balancer_member" {
  project_id = openstack_identity_project_v3.SIGL.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-siops.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_ing-sigl-openstack-member_member" {
  project_id = openstack_identity_project_v3.SIGL.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-openstack-member.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_ing-sigl-openstack-member_load-balancer_member" {
  project_id = openstack_identity_project_v3.SIGL.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-openstack-member.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_forge-roots_admin" {
  project_id = openstack_identity_project_v3.SIGL.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "SIGL_quotaset_compute" {
  project_id = openstack_identity_project_v3.SIGL.id

  instances                   = 15
  cores                       = 144
  ram                         = 192 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "SIGL_quotaset_volume" {
  project_id = openstack_identity_project_v3.SIGL.id

  volumes              = 50
  snapshots            = 10
  gigabytes            = 3000
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "SIGL_quotaset_network" {
  project_id = openstack_identity_project_v3.SIGL.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 1
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "sigl_epita_fr" {
  project_id = openstack_identity_project_v3.SIGL.id
  name       = "sigl.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
resource "openstack_dns_zone_v2" "iaas_sigl_epita_fr" {
  project_id = openstack_identity_project_v3.SIGL.id
  name       = "iaas.sigl.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
resource "openstack_dns_zone_v2" "sigl_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.SIGL.id
  name       = "sigl.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "SIGL_external-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.SIGL.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "SIGL_floating_ips" {
  for_each  = toset(["91.243.117.174"])
  tenant_id = openstack_identity_project_v3.SIGL.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}
