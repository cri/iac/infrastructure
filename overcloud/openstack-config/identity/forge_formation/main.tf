terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.48.0"
    }
  }
}

# Project
resource "openstack_identity_project_v3" "project" {
  name      = "Forge_form_${var.user.shortname}"
  domain_id = var.domain_id
}

resource "openstack_identity_role_assignment_v3" "role_assignment" {
  for_each   = toset(var.student_roles)
  project_id = openstack_identity_project_v3.project.id
  user_id    = var.user.id
  role_id    = each.key
}

resource "openstack_identity_role_assignment_v3" "forge-staff_assignments" {
  for_each   = toset(var.admin_roles)
  project_id = openstack_identity_project_v3.project.id
  group_id   = var.admin_group_id
  role_id    = each.key
}

# Quotas
resource "openstack_compute_quotaset_v2" "quotaset_compute" {
  project_id = openstack_identity_project_v3.project.id

  instances                   = 1
  cores                       = 8
  ram                         = 8 * 1024
  metadata_items              = 128
  key_pairs                   = 4
  server_groups               = 5
  server_group_members        = 5
  injected_files              = 5
  injected_file_content_bytes = 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "quotaset_volume" {
  project_id = openstack_identity_project_v3.project.id

  volumes              = 2
  snapshots            = 10
  gigabytes            = 20
  per_volume_gigabytes = 10
}
resource "openstack_networking_quota_v2" "quotaset_network" {
  project_id = openstack_identity_project_v3.project.id

  network             = 5
  subnet              = 5
  port                = 500
  router              = 3
  floatingip          = 0
  security_group      = 100
  security_group_rule = 1000
}

resource "openstack_dns_zone_v2" "zone" {
  project_id = openstack_identity_project_v3.project.id
  name       = "${var.user.shortname}.form.forge.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

resource "openstack_networking_rbac_policy_v2" "network_rbac" {
  action        = "access_as_shared"
  object_id     = var.network_id
  object_type   = "network"
  target_tenant = openstack_identity_project_v3.project.id
}

resource "openstack_networking_port_v2" "vm_port" {
  name           = "${var.user.shortname}_vm_port"
  network_id     = var.network_id
  tenant_id      = openstack_identity_project_v3.project.id
  admin_state_up = true
}

resource "openstack_lb_listener_v2" "listener_ssh" {
  protocol               = "TCP"
  protocol_port          = 22000 + var.index
  loadbalancer_id        = var.loadbalancer_id
  tenant_id              = var.admin_project_id
  name                   = "forge-formation-${var.user.shortname}-ssh-listener"
  timeout_client_data    = 14400000
  timeout_member_connect = 14400000
  timeout_member_data    = 14400000
}

resource "openstack_lb_pool_v2" "pool_ssh" {
  protocol    = "TCP"
  lb_method   = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.listener_ssh.id
  tenant_id   = var.admin_project_id
  name        = "forge-formation-${var.user.shortname}-ssh-pool"
}

resource "openstack_lb_member_v2" "member_ssh" {
  pool_id       = openstack_lb_pool_v2.pool_ssh.id
  address       = openstack_networking_port_v2.vm_port.all_fixed_ips[0]
  protocol_port = 22
  tenant_id     = var.admin_project_id
  subnet_id     = var.subnet_id
}

resource "openstack_lb_listener_v2" "listener_http" {
  protocol        = "TCP"
  protocol_port   = 8000 + var.index
  loadbalancer_id = var.loadbalancer_id
  tenant_id       = var.admin_project_id
  name            = "forge-formation-${var.user.shortname}-http-listener"
}

resource "openstack_lb_pool_v2" "pool_http" {
  protocol    = "TCP"
  lb_method   = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.listener_http.id
  tenant_id   = var.admin_project_id
  name        = "forge-formation-${var.user.shortname}-http-pool"
}

resource "openstack_lb_member_v2" "member_http" {
  pool_id       = openstack_lb_pool_v2.pool_http.id
  address       = openstack_networking_port_v2.vm_port.all_fixed_ips[0]
  protocol_port = 80
  tenant_id     = var.admin_project_id
  subnet_id     = var.subnet_id
}

resource "openstack_lb_listener_v2" "listener_https" {
  protocol        = "TCP"
  protocol_port   = 8400 + var.index
  loadbalancer_id = var.loadbalancer_id
  tenant_id       = var.admin_project_id
  name            = "forge-formation-${var.user.shortname}-https-listener"
}

resource "openstack_lb_pool_v2" "pool_https" {
  protocol    = "TCP"
  lb_method   = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.listener_https.id
  tenant_id   = var.admin_project_id
  name        = "forge-formation-${var.user.shortname}-https-pool"
}

resource "openstack_lb_member_v2" "member_https" {
  pool_id       = openstack_lb_pool_v2.pool_https.id
  address       = openstack_networking_port_v2.vm_port.all_fixed_ips[0]
  protocol_port = 443
  tenant_id     = var.admin_project_id
  subnet_id     = var.subnet_id
}
