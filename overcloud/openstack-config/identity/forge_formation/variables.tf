variable "user" {
  type = object({
    shortname = string
    username  = string
    id        = string
  })
}

variable "domain_id" {
  type = string
}

variable "student_roles" {
  type = list(string)
}

variable "admin_roles" {
  type = list(string)
}

variable "admin_group_id" {
  type = string
}

variable "network_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "loadbalancer_id" {
  type = string
}

variable "index" {
  type = number
}

variable "admin_project_id" {
  type = string
}
