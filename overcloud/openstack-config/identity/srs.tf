# Project
resource "openstack_identity_project_v3" "SRS" {
  name      = "SRS"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_ing-srs-roots" {
  name      = "ing-srs-roots"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "SRS_forge-roots_admin" {
  project_id = openstack_identity_project_v3.SRS.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}
resource "openstack_identity_role_assignment_v3" "SRS_ing-srs-roots_member" {
  project_id = openstack_identity_project_v3.SRS.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-srs-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "SRS_ing-srs-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.SRS.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-srs-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "SRS_quotaset_compute" {
  project_id = openstack_identity_project_v3.SRS.id

  instances                   = 0
  cores                       = 0
  ram                         = 0
  metadata_items              = 0
  key_pairs                   = 0
  server_groups               = 0
  server_group_members        = 0
  injected_files              = 0
  injected_file_content_bytes = 0
  injected_file_path_bytes    = 0
}
resource "openstack_blockstorage_quotaset_v3" "SRS_quotaset_volume" {
  project_id = openstack_identity_project_v3.SRS.id

  volumes              = 0
  snapshots            = 0
  gigabytes            = 0
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "SRS_quotaset_network" {
  project_id = openstack_identity_project_v3.SRS.id

  network             = 0
  subnet              = 0
  port                = 0
  router              = 0
  floatingip          = 0
  security_group      = 0
  security_group_rule = 0
}

# DNS
resource "openstack_dns_zone_v2" "srs_epita_fr" {
  project_id = openstack_identity_project_v3.SRS.id
  name       = "srs.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
