# Project
resource "openstack_identity_project_v3" "ING_Assistants" {
  name      = "ING_Assistants"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_ing-assistants-toolchain" {
  name      = "ing-assistants-toolchain"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "ING_Assistants_ing-assistants-toolchain_member" {
  project_id = openstack_identity_project_v3.ING_Assistants.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-assistants-toolchain.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "ING_Assistants_ing-assistants-toolchain_load-balancer_member" {
  project_id = openstack_identity_project_v3.ING_Assistants.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-assistants-toolchain.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "ING_Assistants_forge-roots_admin" {
  project_id = openstack_identity_project_v3.ING_Assistants.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}
resource "openstack_identity_role_assignment_v3" "ING_Assistants_forge-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.ING_Assistants.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "ING_Assistants_quotaset_compute" {
  project_id = openstack_identity_project_v3.ING_Assistants.id

  instances                   = 10
  cores                       = 64
  ram                         = 1024 * 64
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "ING_Assistants_quotaset_volume" {
  project_id = openstack_identity_project_v3.ING_Assistants.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 300
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "ING_Assistants_quotaset_network" {
  project_id = openstack_identity_project_v3.ING_Assistants.id

  network             = 20
  subnet              = 20
  port                = 500
  router              = 10
  floatingip          = 5
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "assistants_ing_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.ING_Assistants.id
  name       = "assistants.ing.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "ING_Assistants_external-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.ING_Assistants.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "ING_Assistants_floating_ips" {
  for_each  = toset(["91.243.117.152", "91.243.117.157"])
  tenant_id = openstack_identity_project_v3.ING_Assistants.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}

# PIE (router and floating IPs)
resource "openstack_networking_router_v2" "ING_Assistants_pie-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.ING_Assistants.id
  name                = "pie-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.pie-net.id
  enable_snat         = true
}
resource "openstack_networking_router_route_v2" "ING_Assistants_pie-router" {
  count            = length(openstack_networking_router_v2.ING_Assistants_pie-router)
  router_id        = openstack_networking_router_v2.ING_Assistants_pie-router[count.index].id
  destination_cidr = "10.0.0.0/8"
  next_hop         = "10.201.5.1"
}
resource "openstack_networking_floatingip_v2" "ING_Assistants_floating_ips_pie" {
  for_each  = toset(["10.201.5.150", "10.201.5.151", "10.201.5.152"])
  tenant_id = openstack_identity_project_v3.ING_Assistants.id
  pool      = data.openstack_networking_network_v2.pie-net.name
  address   = each.value
}
