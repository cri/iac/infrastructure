# Project
resource "openstack_identity_project_v3" "LRDE_image" {
  name      = "LRDE_image"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_labo-lrde-image-openstack" {
  name      = "labo-lrde-image-openstack"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}
data "openstack_identity_group_v3" "CRI_labo-lrde-roots" {
  name      = "labo-lrde-roots"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lrde-image_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lrde-image-openstack.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lrde-image_load-balancer_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lrde-image-openstack.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lrde-roots_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lrde-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lrde-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lrde-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lre-roots_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_labo-lre-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LRDE_image_forge-roots_admin" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "LRDE_image_quotaset_compute" {
  project_id = openstack_identity_project_v3.LRDE_image.id

  instances                   = 5
  cores                       = 5 * 8
  ram                         = 5 * 16 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "LRDE_image_quotaset_volume" {
  project_id = openstack_identity_project_v3.LRDE_image.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 500
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "LRDE_image_quotaset_network" {
  project_id = openstack_identity_project_v3.LRDE_image.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 1
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "image_lrde_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.LRDE_image.id
  name       = "image.lrde.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "LRDE_image_external-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.LRDE_image.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "LRDE_image_floating_ips" {
  for_each  = toset(["91.243.117.163"])
  tenant_id = openstack_identity_project_v3.LRDE_image.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}
resource "openstack_networking_router_v2" "LRDE_image_legacy-pie-router" {
  count               = 1
  name                = "legacy-pie-${count.index}"
  admin_state_up      = true
  tenant_id           = openstack_identity_project_v3.LRDE_image.id
  external_network_id = data.openstack_networking_network_v2.legacy-pie-net.id
  enable_snat         = true
}
resource "openstack_networking_router_route_v2" "LRDE_image_legacy-pie-router" {
  count            = length(openstack_networking_router_v2.LRDE_image_legacy-pie-router)
  router_id        = openstack_networking_router_v2.LRDE_image_legacy-pie-router[count.index].id
  destination_cidr = "10.0.0.0/8"
  next_hop         = "10.224.21.254"
}
resource "openstack_networking_router_v2" "LRDE_image_pie-router" {
  count               = 1
  name                = "pie-${count.index}"
  admin_state_up      = true
  tenant_id           = openstack_identity_project_v3.LRDE_image.id
  external_network_id = data.openstack_networking_network_v2.pie-net.id
  enable_snat         = true
}
resource "openstack_networking_router_route_v2" "LRDE_image_pie-router" {
  count            = length(openstack_networking_router_v2.LRDE_image_pie-router)
  router_id        = openstack_networking_router_v2.LRDE_image_pie-router[count.index].id
  destination_cidr = "10.0.0.0/8"
  next_hop         = "10.201.5.1"
}

resource "openstack_compute_flavor_v2" "LRDE_image-g1-large" {
  name      = "LRDE_image.g1.large"
  ram       = 16384
  vcpus     = 8
  disk      = 50
  is_public = false

  extra_specs = {
    "pci_passthrough:alias" = "nvidia_geforce_gtx_1650:1"
  }
}
resource "openstack_compute_flavor_access_v2" "LRDE_image-g1-large" {
  tenant_id = openstack_identity_project_v3.LRDE_image.id
  flavor_id = openstack_compute_flavor_v2.LRDE_image-g1-large.id
}

resource "openstack_compute_flavor_v2" "LRDE_image-g1-large-avx" {
  name      = "LRDE_image.g1.large-avx"
  ram       = 16384
  vcpus     = 8
  disk      = 50
  is_public = false

  extra_specs = {
    "pci_passthrough:alias" = "nvidia_geforce_gtx_1650:1"
    "trait:HW_CPU_X86_AVX"  = "required"
    "trait:HW_CPU_X86_AVX2" = "required"
  }
}
resource "openstack_compute_flavor_access_v2" "LRDE_image-g1-large-avx" {
  tenant_id = openstack_identity_project_v3.LRDE_image.id
  flavor_id = openstack_compute_flavor_v2.LRDE_image-g1-large-avx.id
}
