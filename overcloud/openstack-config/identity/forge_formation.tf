locals {
  students = [
    {
      shortname = "nicolas"
      username  = "nicolas.froger"
      id        = "295b521d230a666f7e03cea7b15e68b8b4030571586b462ee7a5671e86321796"
    },
    {
      username  = "armand.blin"
      shortname = "armand"
      id        = "5a7b1789f03a129e8b670b490a0d3623c2315e03ff53461f1a647a797cd9a915"
    },
    {
      username  = "augustin.begue"
      shortname = "augustin"
      id        = "900a34d03be0d770bb621565001c6edba08fa1a0816eba40d4c61c6347e46dc9"
    },
    {
      username  = "emmeline.heitzler"
      shortname = "emmeline"
      id        = "6aec8124216f0da8cec89b3c37484be3d1090821411eaede4766b4043e283795"
    },
    {
      username  = "etienne.senigout"
      shortname = "etienne"
      id        = "f4d3b1571f3c722241fcb90c981cae68514981148d8ecf462ded5f484fec69a3"
    },
    {
      username  = "gregoire.lefaure"
      shortname = "gregoire"
      id        = "2779cad0dd1397637f2b41653d9ab854a9a42980ff75873dd7fad30e2c15af27"
    },
    {
      username  = "manasse.ratovo"
      shortname = "manasse"
      id        = "3a4863b4f20710e5331ab989cdf06d6df4e5fe1ea2d7b3926d95075eb3a2a228"
    },
    {
      username  = "mateo.lelong"
      shortname = "mateo"
      id        = "8d0c6ad71c628c5fe29c74fafea01285d1650a158e41e59346671d2a2e1073c3"
    },
    {
      username  = "robin.lamberger"
      shortname = "robinl"
      id        = "203b5c167c585e396ad55ff640520ec37d0fd8833cff56bcad54575c514ff8de"
    },
    {
      username  = "robin.varliette"
      shortname = "robinv"
      id        = "9e7b13c55e1244e3b19ee03d1827dca70abf66fb2e1c989fdb3c6fa4770cc96b"
    },
    {
      username  = "vianney.marticou"
      shortname = "vianney"
      id        = "d77daf96290fea18822c54f3279b00ebc0ea6ebad883b24bc7a22cb987a11fa2"
    },
  ]
}

resource "openstack_networking_network_v2" "Forge_formation-net" {
  name           = "forge-formation-net"
  admin_state_up = "true"
  tenant_id      = openstack_identity_project_v3.CRI_playground.id
}

resource "openstack_networking_subnet_v2" "Forge_formation-subnet" {
  network_id = openstack_networking_network_v2.Forge_formation-net.id
  cidr       = "192.168.42.0/24"
  tenant_id  = openstack_identity_project_v3.CRI_playground.id
  dns_nameservers = [
    "91.243.117.210",
  ]
}

resource "openstack_networking_router_interface_v2" "Forge_formation-router-if" {
  router_id = openstack_networking_router_v2.CRI_playground_external-router[0].id
  subnet_id = openstack_networking_subnet_v2.Forge_formation-subnet.id
}

resource "openstack_lb_loadbalancer_v2" "Forge_formation-lb" {
  vip_subnet_id = openstack_networking_subnet_v2.Forge_formation-subnet.id
  name          = "forge-formation-lb"
  tenant_id     = openstack_identity_project_v3.CRI_playground.id
}

resource "openstack_networking_floatingip_associate_v2" "Forge_formation_fip" {
  floating_ip = "91.243.117.170"
  port_id     = openstack_lb_loadbalancer_v2.Forge_formation-lb.vip_port_id
}

module "formation" {
  for_each = {
    for index, student in local.students :
    index => student
  }
  source = "./forge_formation"

  network_id = openstack_networking_network_v2.Forge_formation-net.id
  subnet_id  = openstack_networking_subnet_v2.Forge_formation-subnet.id
  index      = each.key
  user       = each.value
  domain_id  = data.openstack_identity_project_v3.CRI_domain.id
  student_roles = [
    data.openstack_identity_role_v3.member.id
  ]
  admin_group_id = data.openstack_identity_group_v3.CRI_forge-staff.id
  admin_roles = [
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
    data.openstack_identity_role_v3.admin.id,
  ]
  loadbalancer_id  = openstack_lb_loadbalancer_v2.Forge_formation-lb.id
  admin_project_id = openstack_identity_project_v3.CRI_playground.id
}
