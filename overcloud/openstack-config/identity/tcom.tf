# Project
resource "openstack_identity_project_v3" "TCOM" {
  name      = "TCOM"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_ing-tcom-roots" {
  name      = "ing-tcom-roots"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_ing-tcom-assistants" {
  name      = "ing-tcom-assistants"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "TCOM_ing-tcom-roots_member" {
  project_id = openstack_identity_project_v3.TCOM.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-tcom-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "TCOM_ing-tcom-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.TCOM.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-tcom-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "TCOM_ing-tcom-assistants_member" {
  project_id = openstack_identity_project_v3.TCOM.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-tcom-assistants.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "TCOM_ing-tcom-assistants_load-balancer_member" {
  project_id = openstack_identity_project_v3.TCOM.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-tcom-assistants.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "TCOM_forge-roots_admin" {
  project_id = openstack_identity_project_v3.TCOM.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "TCOM_quotaset_compute" {
  project_id = openstack_identity_project_v3.TCOM.id

  instances                   = 40
  cores                       = 64
  ram                         = 128 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "TCOM_quotaset_volume" {
  project_id = openstack_identity_project_v3.TCOM.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 500
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "TCOM_quotaset_network" {
  project_id = openstack_identity_project_v3.TCOM.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 2
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "tcom_epita_fr" {
  project_id = openstack_identity_project_v3.TCOM.id
  name       = "tcom.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
resource "openstack_dns_zone_v2" "iaas_tcom_epita_fr" {
  project_id = openstack_identity_project_v3.TCOM.id
  name       = "iaas.tcom.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
resource "openstack_dns_zone_v2" "tcom_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.TCOM.id
  name       = "tcom.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "TCOM_external-router" {
  count               = 2
  tenant_id           = openstack_identity_project_v3.TCOM.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "TCOM_floating_ips" {
  for_each  = toset(["91.243.117.132"])
  tenant_id = openstack_identity_project_v3.TCOM.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}
