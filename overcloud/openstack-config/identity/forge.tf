data "openstack_identity_group_v3" "CRI_forge-roots" {
  name      = "forge-roots"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_forge" {
  name      = "forge"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

data "openstack_identity_group_v3" "CRI_forge-staff" {
  name      = "forge-staff"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}
