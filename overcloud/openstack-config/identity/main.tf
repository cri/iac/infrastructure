terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.48.0"
    }
  }
}

data "openstack_identity_role_v3" "admin" {
  name = "admin"
}
data "openstack_identity_role_v3" "member" {
  name = "member"
}
data "openstack_identity_role_v3" "load-balancer_member" {
  name = "load-balancer_member"
}
data "openstack_networking_network_v2" "ext-net" {
  name = "ext-net"
}
data "openstack_networking_subnet_v2" "ext-subnet" {
  name = "ext-subnet"
}
data "openstack_networking_network_v2" "legacy-pie-net" {
  name = "legacy-pie-net"
}
data "openstack_networking_subnet_v2" "legacy-pie-subnet" {
  name = "legacy-pie-subnet"
}
data "openstack_networking_network_v2" "pie-net" {
  name = "pie-net"
}
data "openstack_networking_subnet_v2" "pie-subnet" {
  name = "pie-subnet"
}
