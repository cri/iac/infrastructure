# Project
resource "openstack_identity_project_v3" "SIGL_intranet" {
  name      = "SIGL_intranet"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups are defined sigl_siops.tf

# Groups assignments
resource "openstack_identity_role_assignment_v3" "SIGL_intranet_ing-sigl-siops_member" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-siops.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_intranet_ing-sigl-openstack-member_member" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id
  group_id   = data.openstack_identity_group_v3.CRI_ing-sigl-openstack-member.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "SIGL_intranet_forge-roots_admin" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "SIGL_intranet_quotaset_compute" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id

  instances                   = 0
  cores                       = 0
  ram                         = 0
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 0
  injected_file_content_bytes = 0
  injected_file_path_bytes    = 0
}
resource "openstack_blockstorage_quotaset_v3" "SIGL_intranet_quotaset_volume" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id

  volumes              = 0
  snapshots            = 0
  gigabytes            = 0
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "SIGL_intranet_quotaset_network" {
  project_id = openstack_identity_project_v3.SIGL_intranet.id

  network             = 0
  subnet              = 0
  port                = 0
  router              = 0
  floatingip          = 0
  security_group      = 0
  security_group_rule = 0
}
