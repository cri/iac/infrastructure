# Project
resource "openstack_identity_project_v3" "CRI_playground" {
  name      = "CRI_Playground"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "CRI_playground_forge-staff_assignments" {
  for_each = toset([
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
  ])
  project_id = openstack_identity_project_v3.CRI_playground.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-staff.id
  role_id    = each.key
}

resource "openstack_identity_role_assignment_v3" "CRI_playground_forge-roots_assignments" {
  for_each = toset([
    data.openstack_identity_role_v3.member.id,
    data.openstack_identity_role_v3.load-balancer_member.id,
    data.openstack_identity_role_v3.admin.id,
  ])
  project_id = openstack_identity_project_v3.CRI_playground.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = each.key
}

# Quotas
resource "openstack_compute_quotaset_v2" "CRI_playground_quotaset_compute" {
  project_id = openstack_identity_project_v3.CRI_playground.id

  instances                   = 20
  cores                       = 64
  ram                         = 64 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "CRI_playground_quotaset_volume" {
  project_id = openstack_identity_project_v3.CRI_playground.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 1000
  per_volume_gigabytes = 50
}
resource "openstack_networking_quota_v2" "CRI_playground_quotaset_network" {
  project_id = openstack_identity_project_v3.CRI_playground.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 5
  security_group      = 100
  security_group_rule = 1000
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "CRI_playground_external-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.CRI_playground.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "CRI_playground_floating_ips" {
  for_each  = toset(["91.243.117.136", "91.243.117.140", "91.243.117.153", "91.243.117.170"])
  tenant_id = openstack_identity_project_v3.CRI_playground.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}
