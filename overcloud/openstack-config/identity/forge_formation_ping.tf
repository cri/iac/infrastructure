# Project
resource "openstack_identity_project_v3" "Forge_form_ping" {
  name      = "Forge_form_ping"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_forge-ing1" {
  name      = "forge-ing1"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "Forge_form_ping_forge-roots_admin" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}
resource "openstack_identity_role_assignment_v3" "Forge_form_ping_forge-ing1_member" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-ing1.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "Forge_form_ping_forge-ing1_load-balancer_member" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-ing1.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "Forge_form_ping_forge-staff_member" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-staff.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "Forge_form_ping_forge-staff_load-balancer_member" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-staff.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "Forge_form_ping_quotaset_compute" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id

  instances                   = 1
  cores                       = 8
  ram                         = 16 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 2 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "Forge_form_ping_quotaset_volume" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 100
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "Forge_form_ping_quotaset_network" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 1
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "ping_form_forge_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.Forge_form_ping.id
  name       = "ping.form.forge.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network (router and floating IPs)
resource "openstack_networking_router_v2" "Forge_form_ping_external-router" {
  count               = 1
  tenant_id           = openstack_identity_project_v3.Forge_form_ping.id
  name                = "external-router-${count.index}"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext-net.id
  enable_snat         = true
}
resource "openstack_networking_floatingip_v2" "Forge_form_ping_floating_ips" {
  for_each  = toset(["91.243.117.173"])
  tenant_id = openstack_identity_project_v3.Forge_form_ping.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}
