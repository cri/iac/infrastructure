# Project
resource "openstack_identity_project_v3" "LSE" {
  name      = "LSE"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_labo-lse" {
  name      = "labo-lse"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "LSE_labo-lse_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lse.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_labo-lse_load-balancer_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lse.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_labo-seal-permanents_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-seal-permanents.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_labo-seal-permanents_load-balancer_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-seal-permanents.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_labo-lre-roots_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_labo-lre-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LSE_forge-roots_admin" {
  project_id = openstack_identity_project_v3.LSE.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "LSE_quotaset_compute" {
  project_id = openstack_identity_project_v3.LSE.id

  instances                   = 0
  cores                       = 0
  ram                         = 0
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "LSE_quotaset_volume" {
  project_id = openstack_identity_project_v3.LSE.id

  volumes              = 15
  snapshots            = 10
  gigabytes            = 500
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "LSE_quotaset_network" {
  project_id = openstack_identity_project_v3.LSE.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 2
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "lse_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.LSE.id
  name       = "lse.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}
