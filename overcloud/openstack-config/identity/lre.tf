# Project
resource "openstack_identity_project_v3" "LRE" {
  name      = "LRE"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups
data "openstack_identity_group_v3" "CRI_labo-lre-roots" {
  name      = "labo-lre-roots"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

# Groups assignments
resource "openstack_identity_role_assignment_v3" "LRE_labo-lre-roots_member" {
  project_id = openstack_identity_project_v3.LRE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.member.id
}
resource "openstack_identity_role_assignment_v3" "LRE_labo-lre-roots_load-balancer_member" {
  project_id = openstack_identity_project_v3.LRE.id
  group_id   = data.openstack_identity_group_v3.CRI_labo-lre-roots.id
  role_id    = data.openstack_identity_role_v3.load-balancer_member.id
}
resource "openstack_identity_role_assignment_v3" "LRE_forge-roots_admin" {
  project_id = openstack_identity_project_v3.LRE.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "LRE_quotaset_compute" {
  project_id = openstack_identity_project_v3.LRE.id

  instances                   = 5
  cores                       = 80
  ram                         = 300 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "LRE_quotaset_volume" {
  project_id = openstack_identity_project_v3.LRE.id

  volumes              = 15
  snapshots            = 10
  gigabytes            = 10 * 1024
  per_volume_gigabytes = 10 * 1024
}
resource "openstack_networking_quota_v2" "LRE_quotaset_network" {
  project_id = openstack_identity_project_v3.LRE.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 2
  security_group      = 100
  security_group_rule = 1000
}

# DNS
resource "openstack_dns_zone_v2" "lre_iaas_epita_fr" {
  project_id = openstack_identity_project_v3.LRE.id
  name       = "lre.iaas.epita.fr."
  email      = "hostmaster@cri.epita.fr"
  type       = "PRIMARY"
}

# Network
resource "openstack_networking_floatingip_v2" "LRE_floating_ips" {
  for_each  = toset(["91.243.117.158", "91.243.117.159"])
  tenant_id = openstack_identity_project_v3.LRE.id
  pool      = data.openstack_networking_network_v2.ext-net.name
  address   = each.value
}

# Flavors
resource "openstack_compute_flavor_v2" "LRE-m1-huge-gpu" {
  name      = "LRE.m1.huge-gpu"
  ram       = 96 * 1024
  vcpus     = 24
  disk      = 128
  is_public = false

  extra_specs = {
    "pci_passthrough:alias" = "nvidia_a40:1"
    "trait:HW_CPU_X86_AVX"  = "required"
    "trait:HW_CPU_X86_AVX2" = "required"
  }
}
resource "openstack_compute_flavor_access_v2" "LRE-m1-huge-gpu" {
  tenant_id = openstack_identity_project_v3.LRE.id
  flavor_id = openstack_compute_flavor_v2.LRE-m1-huge-gpu.id
}
