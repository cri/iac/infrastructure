# Project
resource "openstack_identity_project_v3" "Lab_SI" {
  name      = "Lab_SI"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
}

resource "openstack_identity_role_assignment_v3" "Lab_SI_forge-roots_admin" {
  project_id = openstack_identity_project_v3.Lab_SI.id
  group_id   = data.openstack_identity_group_v3.CRI_forge-roots.id
  role_id    = data.openstack_identity_role_v3.admin.id
}

# Quotas
resource "openstack_compute_quotaset_v2" "Lab_SI_quotaset_compute" {
  project_id = openstack_identity_project_v3.Lab_SI.id

  instances                   = 10
  cores                       = 22
  ram                         = 64 * 1024
  metadata_items              = 128
  key_pairs                   = 100
  server_groups               = 10
  server_group_members        = 10
  injected_files              = 5
  injected_file_content_bytes = 10 * 1024
  injected_file_path_bytes    = 255
}
resource "openstack_blockstorage_quotaset_v3" "Lab_SI_quotaset_volume" {
  project_id = openstack_identity_project_v3.Lab_SI.id

  volumes              = 10
  snapshots            = 10
  gigabytes            = 500
  per_volume_gigabytes = 100
}
resource "openstack_networking_quota_v2" "Lab_SI_quotaset_network" {
  project_id = openstack_identity_project_v3.Lab_SI.id

  network             = 100
  subnet              = 100
  port                = 500
  router              = 10
  floatingip          = 2
  security_group      = 100
  security_group_rule = 1000
}
