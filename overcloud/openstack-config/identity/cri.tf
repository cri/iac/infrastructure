data "openstack_identity_project_v3" "CRI_domain" {
  name      = "CRI"
  is_domain = true
}

data "openstack_identity_project_v3" "CRI" {
  name      = "CRI"
  domain_id = data.openstack_identity_project_v3.CRI_domain.id
  is_domain = false
}

resource "openstack_compute_flavor_v2" "CRI-gitlab-runner" {
  name      = "CRI.gitlab-runner"
  ram       = 32768
  vcpus     = 16
  disk      = 100
  is_public = false

  extra_specs = {
    "aggregate_instance_extra_specs:ssd"           = "true"
    "aggregate_instance_extra_specs:max-cpu-level" = "3"
    "trait:HW_CPU_X86_SVM"                         = "required"
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-gitlab-runner" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-gitlab-runner.id
}

resource "openstack_compute_flavor_v2" "CRI-clone-store" {
  name  = "CRI.clone-store"
  ram   = 2048
  vcpus = 2
  disk  = 20

  extra_specs = {
    "quota:disk_read_bytes_sec"  = 128000000
    "quota:disk_read_iops_sec"   = 200
    "quota:disk_write_bytes_sec" = 128000000
    "quota:disk_write_iops_sec"  = 200
    "quota:vif_inbound_average"  = 93750 # 750 Mbps
    "quota:vif_outbound_average" = 93750 # 750 Mbps
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-clone-store" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-clone-store.id
}

resource "openstack_compute_flavor_v2" "CRI-m1-huge-gpu" {
  name      = "CRI.m1.huge-gpu"
  ram       = 98304
  vcpus     = 24
  disk      = 128
  is_public = false

  extra_specs = {
    "pci_passthrough:alias" = "nvidia_a40:1"
    "trait:HW_CPU_X86_AVX"  = "required"
    "trait:HW_CPU_X86_AVX2" = "required"
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-m1-huge-gpu" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-m1-huge-gpu.id
}

resource "openstack_compute_flavor_v2" "CRI-m1-2xlarge-ssd" {
  name      = "CRI.m1.2xlarge-ssd"
  ram       = 32768
  vcpus     = 16
  disk      = 300
  is_public = false

  extra_specs = {
    "aggregate_instance_extra_specs:ssd" = "true"
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-m1-2xlarge-ssd" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-m1-2xlarge-ssd.id
}

resource "openstack_compute_flavor_v2" "CRI-huge-ssd" {
  name      = "CRI.huge-ssd"
  ram       = 36864
  vcpus     = 32
  disk      = 300
  is_public = false

  extra_specs = {
    "aggregate_instance_extra_specs:ssd" = "true"
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-huge-ssd" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-huge-ssd.id
}

resource "openstack_compute_flavor_v2" "CRI-maas-m2-large" {
  name      = "CRI.maas-m2.large"
  ram       = 8192
  vcpus     = 8
  disk      = 80
  is_public = false

  extra_specs = {
    "trait:HW_CPU_X86_AVX"       = "required"
    "trait:HW_CPU_X86_AVX2"      = "required"
    "quota:disk_read_bytes_sec"  = 256000000
    "quota:disk_read_iops_sec"   = 1000
    "quota:disk_write_bytes_sec" = 256000000
    "quota:disk_write_iops_sec"  = 1000
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-maas-m2-large" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-maas-m2-large.id
}

# TODO: remove me when all computes kernels are updated
resource "openstack_compute_flavor_v2" "CRI-maas-m2-large-nested-tmp" {
  name      = "CRI.maas-m2.large.nested-tmp"
  ram       = 8192
  vcpus     = 8
  disk      = 80
  is_public = false

  extra_specs = {
    "aggregate_instance_extra_specs:nested-works" = "true"
    "trait:HW_CPU_X86_AVX"                        = "required"
    "trait:HW_CPU_X86_AVX2"                       = "required"
    "quota:disk_read_bytes_sec"                   = 256000000
    "quota:disk_read_iops_sec"                    = 1000
    "quota:disk_write_bytes_sec"                  = 256000000
    "quota:disk_write_iops_sec"                   = 1000
  }
}
resource "openstack_compute_flavor_access_v2" "CRI-maas-m2-large-nested-tmp" {
  tenant_id = data.openstack_identity_project_v3.CRI.id
  flavor_id = openstack_compute_flavor_v2.CRI-maas-m2-large-nested-tmp.id
}
