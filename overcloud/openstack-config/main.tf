terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.48.0"
    }
  }

  backend "s3" {
    region = "default"
    bucket = "terraform"
    key    = "openstack-config.tfstate.tf"
    endpoints = {
      s3 = "https://s3.cri.epita.fr"
    }
    force_path_style            = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
    skip_s3_checksum            = true
  }
}

provider "openstack" {
  # this uses the clouds.yaml file
  cloud       = "openstack"
  use_octavia = true
}

module "identity" {
  source = "./identity"
}

data "openstack_identity_project_v3" "CRI" {
  name = "CRI"
}
