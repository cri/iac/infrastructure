resource "openstack_images_image_v2" "ubuntu-20-04-20210201" {
  name             = "Ubuntu 20.04 release 20210201"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/ubuntu-20-04-20210201.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 1
  min_ram_mb       = 512
  visibility       = "public"
  protected        = true
}

resource "openstack_images_image_v2" "ubuntu-20-04-20220815" {
  name             = "Ubuntu 20.04 release 20220815"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/ubuntu-20-04-20220815.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 1
  min_ram_mb       = 512
  visibility       = "public"
  protected        = true
}

resource "openstack_images_image_v2" "ubuntu-22-04-20221120" {
  name             = "Ubuntu 22.04 release 20221120"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/ubuntu-22-04-20221120.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 1
  min_ram_mb       = 512
  visibility       = "public"
  protected        = true
}

resource "openstack_images_image_v2" "cirros-0-5-1" {
  name             = "CirrOS 0.5.1"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/cirros-0.5.1.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 1
  min_ram_mb       = 512
  visibility       = "public"
  protected        = true
}

resource "openstack_images_image_v2" "fedora-coreos-33-20210217-3-0" {
  name             = "Fedora CoreOS 33 Build 20210217.3.0"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/fedora-coreos-33.20210217.3.0.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 10
  min_ram_mb       = 1024
  visibility       = "public"
  protected        = false

  properties = {
    os_distro = "fedora-coreos"
  }
}

resource "openstack_images_image_v2" "centos-8-3-2011-20201204-2" {
  name             = "CentOS 8.3.2011 20201204.2"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/centos-8.3.2011-20201204.2.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 10
  min_ram_mb       = 1024
  visibility       = "public"
  protected        = false
}

resource "openstack_images_image_v2" "windows-10-20h2" {
  name             = "Windows 10 20H2"
  image_source_url = "https://s3.cri.epita.fr/cri-terraform.s3.cri.epita.fr/images/public/windows-10-20H2.raw"
  web_download     = true
  container_format = "bare"
  disk_format      = "raw"
  min_disk_gb      = 20
  min_ram_mb       = 2048
  visibility       = "public"
  protected        = false

  properties = {
    hw_cpu_sockets         = 2
    img_hide_hypervisor_id = true
  }
}
