#!/usr/bin/env bash
# shellcheck disable=SC2155

if [ ! -e clouds.yaml ]; then
    echo "ERROR: Missing clouds.yaml file"
    return 1
fi

OPENSTACK_USER="$(yq -r .clouds.openstack.auth.username clouds.yaml)"
OPENSTACK_DOMAIN="$(yq -r .clouds.openstack.auth.user_domain_name clouds.yaml)"

OS_CREDENTIALS_JSON="$(openstack ec2 credentials list \
    --user "$OPENSTACK_USER" \
    --user-domain "$OPENSTACK_DOMAIN" -f json | jq 'limit(1; .[] | select(."Project ID" == "e3b3356ef0384e839a6d650b658f7e90"))')"

export AWS_ACCESS_KEY_ID="$(echo "$OS_CREDENTIALS_JSON" | jq -r '.Access')"
export AWS_SECRET_ACCESS_KEY="$(echo "$OS_CREDENTIALS_JSON" | jq -r '.Secret')"

if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
    echo "ERROR: Did not find AWS credentials on your OpenStack account."
    echo "Make sure you created ec2 credentials on the CRI project with your account."
    echo "Use this command: openstack ec2 credentials create"
    return 1
fi

echo "Success!"
