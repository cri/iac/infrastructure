# Ansible Roles

This folder is kept empty as it is populated with the `ansible-galaxy` command
and the `requirements.yml` file at the root of the repository.

To populate or update, go at the root of the repo and run the following:

```sh
ansible-galaxy install -r requirements.yml -f
```
